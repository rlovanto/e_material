/*
SQLyog Professional v12.5.1 (64 bit)
MySQL - 8.0.25 : Database - inventory_sensync
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`Matrial_db` /*!40100 DEFAULT CHARACTER SET utf8mb4  */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `Matrial_db`;

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `id_brg` int NOT NULL AUTO_INCREMENT,
  `nama_brg` varchar(100) NOT NULL,
  `jenis` varchar(100) DEFAULT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  `stock` int DEFAULT NULL,
  `nomer_box` varchar(50) DEFAULT NULL,
  `kategori` varchar(25) DEFAULT NULL,
  `catatan` varchar(100) NOT NULL,
  `image` varchar(100) CHARACTER SET utf8mb4  DEFAULT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id_brg`,`nama_brg`) USING BTREE,
  KEY `id_jenis` (`jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=utf8mb4 ;

/*Data for the table `barang` */

insert  into `barang`(`id_brg`,`nama_brg`,`jenis`,`satuan`,`stock`,`nomer_box`,`kategori`,`catatan`,`image`,`user_create`,`create_date`,`user_update`,`update_date`) values 
(101,'Resistor 2K2','',NULL,4,'Rak #4','Asset','','','','0000-00-00','0','0000-00-00'),
(102,'Resistor 100 Ohm','',NULL,-1,'Rak #4','Habis pakai','','','','0000-00-00','0','0000-00-00'),
(103,'Multi Turn Trimpot 50K','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(104,'LED 3 mm 5 V Biru','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(105,'Resistor 4K7','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(106,'Resistor 10K','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(107,'LED 3 mm 5 V Hijau','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(108,'Terminal Block 2P','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(109,'Header Putih 4P','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(110,'Header Putih 3P','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(111,'Header Putih 5P','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(112,'Roset RJ45 1 Lubang','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(113,'Tulang Ikan 1x40 panjang 18 mm','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(114,'Selang Bakar 8 mm','',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(115,'Mur, Baut dan Ring (M4x25mm)','6',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(116,'Dop R3/4','6',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(117,'Dop R2','6',NULL,0,'Rak #3','Lancar','','','','0000-00-00','0','0000-00-00'),
(118,'Dop R1','6',NULL,0,'Rak #3','Lancar','','','','0000-00-00','0','0000-00-00'),
(120,'Baut M3x25 mm','6',NULL,0,'Rak #3','Lancar','','','','0000-00-00','0','0000-00-00'),
(121,'Baut M4x20 mm','6',NULL,0,'Rak #3','Lancar','','','','0000-00-00','0','0000-00-00'),
(122,' Mur M4','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(123,' Isi Glue gun kecil','6',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(124,'Sekrup M3x3mm','6',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(125,'Pipa R1\"','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(126,'Isi Glue Gun Besar','6',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(127,'Amplas','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(128,'Pylox','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(129,' Box 125x125x100 mm','6',NULL,0,'Rak #2','Lancar','','','','0000-00-00','0','0000-00-00'),
(130,' Box 150x200x100 mm','6',NULL,0,'Rak #1','Lancar','','','','0000-00-00','0','0000-00-00'),
(131,'Box 80x110x70','6',NULL,0,'Rak #2','Lancar','','','','0000-00-00','0','0000-00-00'),
(132,'Box 200x200x130','6',NULL,0,'Rak #1','Lancar','','','','0000-00-00','0','0000-00-00'),
(133,'ESP 32 Lolin','',NULL,-1,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(134,'Speed Controller','3',NULL,0,'Rak 4','Lancar','','','','0000-00-00','0','0000-00-00'),
(135,'Akrilik 2mm 45x45cm','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(136,'Aplikasi Windows 10','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(137,'Arduino Nano','2',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(138,'BME280','1',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(139,'CB Connector 3 pin','8',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(140,'CB Connector 4 pin','8',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(141,'Cyclone PM10','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(142,'Cyclone PM2.5','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(143,'Fan 12Vdc 8x8cm','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(144,'Film PCB Double Layer','6',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(145,'Film PCB Single Layer','6',NULL,0,'Rak #4','Lancar','','','','0000-00-00','0','0000-00-00'),
(146,'Filter Gas','6',NULL,-4,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(147,'Flow sensor SFM4100','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(148,'Header Putih 2P','4',NULL,-12,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(149,'Header RJ 11','4',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(150,'Heater Partikulat','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(151,'Inlet Sensor Partikulat','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(152,'Inlet Sensor Gas','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(153,'Isolasi Tahan Panas','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(154,'ISP (Internet)','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(155,'Jumper Male-to-Male','8',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(156,'Jumper Male-to-Female','8',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(157,'Kabel Ducting 25x25 mm','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(159,'Kabel LAN Outdoor','8',NULL,-14,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(160,'Kabel Micro HDMI','8',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(161,'Davis Vantage Pro2 (Solar Radiation, Rainfall, Anemometer, Solar Shield)','6',NULL,0,'','Lancar','','','','0000-00-00','0','0000-00-00'),
(163,'Power Bank','6',NULL,0,'Rak #5','Lancar','','','','0000-00-00','0','0000-00-00'),
(168,'eCHEM TpH 10m','Sensor','PCS',-1,'#Rak1','Asset','','','','2021-06-09','','0000-00-00'),
(169,'Suspended Solids 30 g/L 10m','Sensor','PCS',1,'#Rak1','Asset','','','','2021-06-09','','0000-00-00'),
(170,'LISA-01-254-VA-D-C / COD','Sensor','PCS',1,'#Rak1','Asset','','','','2021-06-09','','0000-00-00'),
(171,'W55 / Wiper','Sensor','PCS',1,'#Rak1','Asset','','','','2021-06-09','','0000-00-00'),
(172,'wiper blade 1mm path','Sensor','PCS',1,'#Rak1','Asset','','','','2021-06-09','','0000-00-00'),
(177,'Baud + Mur','Lain-Lain','PCS',0,'Rak 4','Habis pakai','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(178,'Avometer','Tools','PCS',2,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(179,'Bor Listrik','Tools','PCS',2,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(180,'Bor Tangan','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(181,'Box Komponen','Lain-Lain','PCS',2,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(182,'Digital Kaliper','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(183,'Gergaji Zigsaw','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(184,'Gerinda Duduk','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(185,'Headlamp','Tools','PCS',3,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(186,'Obeng Listrik (Cordless Screwdriver)','Tools','PCS',4,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(187,'Ragum','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(188,'Waterproff ABS Safety Box','Lain-Lain','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(189,'Termolaser','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(190,'Tang Potong','Tools','PCS',3,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(191,'Solder Soldering','Tools','PCS',4,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(192,'Dekko Solder ','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(193,'Obeng Buntek','Tools','PCS',3,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(194,'Rak Penyimpanan','Lain-Lain','PCS',5,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(195,'Air Cooler','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(196,'CCTV','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(197,'Computer Asus M241D','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(198,'Computer LG Flatron L17WSB','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(199,'Computer LG Flatron W1642S','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(200,'Dispenser','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(201,'Filling Cabinet 4 pintu','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(202,'Finger Print','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(203,'Hardisk External HDD','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(204,'Keyboard','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(205,'Kotak P3K','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(206,'Kulkas Changhong','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(207,'Kursi Kantor','ATK','PCS',17,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(208,'Laci Kecil Lion Star','ATK','PCS',3,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(209,'Laptop Dell Inspiron 340','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(210,'Laptop Lenovo Y-545','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(211,'Lemari Coklat Putih','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(212,'Meja Kerja','ATK','PCS',6,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(213,'Meja Perakitan','ATK','PCS',4,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(214,'Notebook Dell 11X','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(215,'Notebook Dell Inspiron 14','ATK','PCS',2,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(216,'Papan Tulis Besar','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(217,'Papan Tulis Sedang','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(218,'Printer HP DeskJet GT 5810','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(219,'Rak Cuci Piring','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(220,'Rak Komponen Hitam','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(221,'Rak Sepatu','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(222,'Router Linksys','Modul','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(223,'Router Tenda','Modul','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(224,'Running Text','Modul','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(225,'Tangga ukuran 3,5 m','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(226,'Tangga ukuran 5,5 m','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(227,'Tempat Sampah','ATK','PCS',3,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(228,'Tripod','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(229,'TV Dell','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(230,'TV Xiaomi','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(231,'Kapstock kayu','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(232,'Sikat WC','ATK','PCS',-13,'Rak 4','Habis pakai','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(233,'Keset','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(234,'Cermin','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(235,'Meja meeting','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(236,'Kursi kantor','ATK','PCS',8,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(237,'Kipas Angin','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(238,'Pompa Sabun','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(239,'Corner Shower Candy 3 Tiers','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(240,'Rak Sepatu','ATK','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(241,'Statif','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(242,'Klemburet Single','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(243,'Beaker 500 ml','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(244,'Labu ukur 500 ml','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(245,'Buret 50 ML Kran kaca','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(246,'Corong 5 cm','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(247,'Vial 500 ML MB','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(248,'Sepatu boot','Tools','PCS',2,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(249,'Obeng Magnetik','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(250,'Batang Pengaduk Larutan','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(251,'Gelas Kimia','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(252,'Troley Sparing','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(253,'Obeng','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(254,'Gerinda Potong','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(255,'Mata Solder ','Tools','PCS',1,'Rak 4','Habis pakai','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(256,'Lotfet 50gr','Tools','PCS',1,'Rak 4','Habis pakai','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(257,'Sedott Timah','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(258,'Matkan 500ml','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(259,'Botol Semprot 500 ml','Tools','PCS',2,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(260,'Pipet Ukur 1 ML','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(261,'Rak Tabung 12 Kayu','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(262,'Dudukan Bor Listrik','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(263,'Metric Tap Set','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(264,'Stabilizer','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(265,'Advan Router','Modul','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00'),
(266,'Cutting Mat A1','Tools','PCS',1,'Rak 4','Aset','','','Adhitya Chasani Wicaksono','2021-06-11','','0000-00-00');

/*Table structure for table `card` */

DROP TABLE IF EXISTS `card`;

CREATE TABLE `card` (
  `id` int NOT NULL AUTO_INCREMENT,
  `waktu` date DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `kartu` varchar(50) DEFAULT NULL,
  `nomor` varchar(20) NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `pic_cont` varchar(13) NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 ;

/*Data for the table `card` */

insert  into `card`(`id`,`waktu`,`type`,`kartu`,`nomor`,`tempat`,`pic_cont`,`user_create`,`create_date`,`user_update`,`update_date`) values 
(18,'2021-07-24','GSM','Telkomsel','082118432946','Base Mobile 1','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(19,'2021-06-27','GSM','Telkomsel','085157358060','Base Mobile 2','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(20,'2021-07-09','GSM','Telkomsel','082117245887','Base Mobile 3','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(21,'2021-07-17','GSM','Three','08978634526','LED Jabar','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(22,'2021-07-08','GSM','Telkomsel','085384593592','OKI','081363201575','Leni Yuniar','2021-06-15','','0000-00-00'),
(23,'2021-07-08','GSM','Telkomsel','085384593590','OI','085269105505','Leni Yuniar','2021-06-15','','0000-00-00'),
(24,'2021-06-20','GSM','Telkomsel','081214559983','Neosense','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(25,'2021-06-28','GSM','Telkomsel','085383727181','Serang','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(26,'2021-07-16','GSM','Telkomsel','085212692504','Kiarapayung (APF)','-','Leni Yuniar','2021-06-15','Leni Yuniar','2021-06-16'),
(27,'2021-06-30','GSM','Telkomsel','085314984632','Warung Bambu','-','Leni Yuniar','2021-06-15','','0000-00-00');

/*Table structure for table `images` */

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id_img` int NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `uploaded_on` datetime NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_img`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb3;

/*Data for the table `images` */

insert  into `images`(`id_img`,`file_name`,`uploaded_on`,`status`) values 
(13,'1578025552697544683202424764703.jpg','2020-01-03 11:26:11','1'),
(14,'Arduino Nano.jpeg','2020-01-03 13:55:25','1'),
(15,'Micro SD Card Adapter.jpeg','2020-01-03 13:59:32','1'),
(16,'USB to TTL.jpeg','2020-01-03 14:00:07','1'),
(17,'Wemos.jpeg','2020-01-03 14:00:54','1'),
(18,'Lolin NodeMCU.jpeg','2020-01-03 14:01:32','1'),
(19,'Mega Pro 2560.jpeg','2020-01-03 14:01:59','1'),
(20,'1.8 TFT.jpeg','2020-01-03 14:02:44','1'),
(21,'Touchscreen  TFT.jpeg','2020-01-03 14:03:21','1'),
(22,'RS485.jpeg','2020-01-03 14:03:41','1'),
(23,'Bluetooth HC-05.jpeg','2020-01-03 14:04:06','1'),
(24,'MQ-5.jpeg','2020-01-03 14:04:23','1'),
(25,'MQ-7.jpeg','2020-01-03 14:04:37','1'),
(26,'Multiplexer 16x1.jpeg','2020-01-03 14:05:14','1'),
(27,'RS232.jpeg','2020-01-03 14:05:36','1'),
(28,'Terminal Block.jpeg','2020-01-03 14:31:34','1'),
(29,'Cable Connector 1x1.jpeg','2020-01-03 14:33:52','1'),
(30,'Cable Connector 1x1 Kecil.jpeg','2020-01-03 14:35:35','1'),
(31,'Cable Connector 4x4.jpeg','2020-01-03 14:36:53','1'),
(32,'Cable Connector 6x6.jpeg','2020-01-03 14:37:42','1'),
(33,'Saklar Merah.jpeg','2020-01-03 14:38:32','1'),
(34,'Saklar Hijau.jpeg','2020-01-03 14:38:57','1'),
(35,'Saklar Orange.jpeg','2020-01-03 14:39:11','1'),
(36,'Saklar kotak.jpeg','2020-01-03 14:39:25','1'),
(37,'Mega Prototypr Shield v3.jpeg','2020-01-03 14:41:55','1'),
(38,'LCD 16x2.jpeg','2020-01-03 14:42:38','1'),
(39,'Arduino Uno.jpeg','2020-01-03 14:43:01','1'),
(40,'Wemos ATSAMd21G18.jpeg','2020-01-03 14:43:40','1'),
(41,'RJ45 Connector.jpeg','2020-01-03 14:44:21','1'),
(42,'Arduino Mega 2560.jpeg','2020-01-03 14:45:05','1'),
(43,'SHT 11.jpeg','2020-01-03 14:53:40','1'),
(44,'Sensor Arus ACS712.jpeg','2020-01-03 14:54:29','1'),
(45,'DHT 11.jpeg','2020-01-03 14:55:49','1'),
(46,'LCD 16x2 Besar.jpeg','2020-01-03 14:59:12','1'),
(47,'Regulator LM2596.jpeg','2020-01-03 15:52:32','1'),
(48,'Regulator XY-SJVA-4.jpeg','2020-01-03 15:53:59','1'),
(49,'Regulator XY-SJVA-4.jpeg','2020-01-03 15:54:21','1'),
(50,'Regulator XY-SJVA-4.jpeg','2020-01-03 15:57:28','1'),
(51,'Raspberry Pi Zero.jpeg','2020-01-03 15:58:55','1'),
(52,'Spec Sensor NO2.jpeg','2020-01-03 15:59:35','1'),
(53,'Spec Sensor SO2.jpeg','2020-01-03 16:00:29','1'),
(54,'USB to TTL (Spec).jpeg','2020-01-03 16:03:32','1'),
(55,'Spec ULPSM Evaluation Board.jpeg','2020-01-03 16:04:52','1'),
(56,'Spec Sensor CO.jpeg','2020-01-03 16:05:34','1'),
(57,'Spec Sensor Respiratory Irritants.jpeg','2020-01-03 16:05:52','1'),
(58,'Relay Hong Wei 555 Timer.jpeg','2020-01-03 16:36:00','1'),
(59,'Relay AC Voltage LM358.jpeg','2020-01-03 16:48:41','1'),
(60,'Relay Hong Wei Low Level Trigger.jpeg','2020-01-03 16:49:25','1'),
(61,'Relay Songle High Level Trigger.jpeg','2020-01-03 16:49:38','1'),
(62,'Relay 4 Modul.jpeg','2020-01-03 16:51:19','1'),
(63,'Relay 2 Modul.jpeg','2020-01-03 16:51:44','1'),
(64,'SIM900(1).jpeg','2020-01-03 16:54:21','1'),
(65,'SIM800L.jpeg','2020-01-03 16:55:41','1'),
(66,'SIM900(2).jpeg','2020-01-03 16:56:28','1'),
(67,'SIM900(3).jpeg','2020-01-03 16:57:01','1'),
(68,'Raspberry Pi 3 Model B+.jpeg','2020-01-03 16:57:56','1'),
(69,'Sensor PM APM.jpeg','2020-01-03 16:59:06','1'),
(70,'Sensor PM Winsen.jpeg','2020-01-03 16:59:24','1'),
(71,'Sensor PM Sensirion.jpeg','2020-01-03 16:59:37','1'),
(72,'15786469699607355998612974386540.jpg','2020-01-10 16:03:09','1'),
(74,'sparing.png','2020-01-10 16:37:33','1'),
(75,'IMG-20200110-WA0007.jpg','2020-01-10 16:42:41','1'),
(76,'720b4fc5ad719fd9d712e4d8c79e438a.jpg','2020-01-15 14:12:04','1'),
(77,'720b4fc5ad719fd9d712e4d8c79e438a.jpg','2020-01-15 14:14:41','1'),
(78,'2e6b2975381ec28c5297454f586ae682.jpg','2020-01-15 14:17:48','1'),
(79,'WhatsApp Image 2020-03-30 at 11.24.28.jpeg','2020-03-30 11:25:30','1'),
(80,'headerfemale1x40.jpg','2020-06-22 16:08:24','1'),
(81,'headerfemale1x40.jpg','2020-06-22 16:11:23','1'),
(82,'Kabel jumper female to female.jpg','2020-06-22 16:16:27','1'),
(83,'banana jack male female.jpg','2020-06-22 16:21:28','1'),
(84,'header rj45.jpg','2020-06-22 16:24:08','1'),
(85,'Selang bakar 3 mm.jpg','2020-06-22 16:26:18','1'),
(86,'Transistor NN 2n2222.jpg','2020-06-22 16:31:43','1'),
(87,'resistor 1k.jpg','2020-06-22 16:34:25','1'),
(88,'Resistor 2k2.jpg','2020-06-22 16:36:25','1'),
(89,'resistoe 100 ohm.jpg','2020-06-22 16:38:44','1'),
(90,'multi turn trimpot 50k.jpg','2020-06-22 16:40:59','1'),
(91,'LED 3 mm 5 V biru.jpg','2020-06-22 16:44:50','1'),
(92,'resistor 4k7.jpg','2020-06-22 16:46:47','1'),
(93,'resistor 10k.jpg','2020-06-22 16:48:38','1'),
(94,'LED 3 mm 5 V hijau.jpg','2020-06-22 16:52:06','1'),
(95,'Terminal block 2p.jpg','2020-06-22 16:55:45','1'),
(96,'header putih.jpg','2020-06-22 16:57:45','1'),
(97,'header putih.jpg','2020-06-22 16:59:17','1'),
(98,'header putih.jpg','2020-06-22 17:00:30','1'),
(99,'tulang ikan 1x40 18 mm.jpg','2020-06-22 17:05:18','1'),
(100,'Selang bakar 8 mm.jpg','2020-06-22 17:07:14','1'),
(101,'mur, baut dan ring.png','2020-06-23 14:32:53','1'),
(102,'dop pipa.jpg','2020-06-23 14:36:57','1'),
(103,'dop pipa.jpg','2020-06-23 14:39:17','1'),
(104,'kabel gland pg 16.jpg','2020-06-23 14:42:41','1'),
(105,'baut m3x25.jpg','2020-06-23 14:44:55','1'),
(106,'baut m4x20.jpg','2020-06-23 14:46:37','1'),
(107,'mur m4.jpg','2020-06-23 14:49:33','1'),
(108,'filler glue gun.jpg','2020-06-23 15:35:08','1'),
(109,'Sekrup M3x3mm.jpg','2020-06-23 15:38:24','1'),
(110,'Pipa.jpg','2020-06-23 15:47:34','1'),
(111,'filler glue gun.jpg','2020-06-23 16:08:10','1'),
(112,'Amplas.jpg','2020-06-23 16:10:05','1'),
(113,'pilox.jpg','2020-06-23 16:10:41','1'),
(114,'box.jpg','2020-06-23 16:15:38','1'),
(115,'box.jpg','2020-06-23 16:16:26','1'),
(116,'box.jpg','2020-06-23 16:17:15','1'),
(117,'esp32 lolin.jpg','2020-06-24 15:09:39','1'),
(118,'speed controller.jpg','2020-06-24 15:16:08','1'),
(119,'Akrilik.jpg','2020-06-24 15:41:01','1'),
(120,'arduino nano.jpg','2020-06-24 15:44:41','1'),
(121,'BME280.jpg','2020-06-24 15:46:29','1'),
(122,'CB connector 3 pin.png','2020-06-24 15:48:13','1'),
(123,'CB connector 4 pin.png','2020-06-24 15:49:24','1'),
(124,'cyclone pm10.jpg','2020-06-24 16:00:55','1'),
(125,'cooling fan.jpg','2020-06-24 16:07:02','1'),
(126,'film pcb.jpg','2020-06-24 16:11:42','1'),
(127,'filter gas.jpg','2020-06-24 16:14:01','1'),
(128,'flow sensor sfm4100.jpg','2020-06-24 16:18:22','1'),
(129,'header putih.jpg','2020-06-24 16:27:05','1'),
(130,'header rj11.jpg','2020-06-24 16:30:53','1'),
(131,'kabel microusd.png','2020-06-24 16:48:54','1'),
(132,'cable gland.jpg','2020-06-24 16:53:57','1'),
(133,'davis.jpg','2020-06-25 16:01:48','1'),
(134,'cable gland.jpg','2020-06-25 16:15:13','1'),
(135,'cable gland.jpg','2020-06-25 16:15:41','1'),
(136,'Untitled.png','2020-06-29 17:34:15','1'),
(137,'PB.jpeg','2020-06-29 17:39:02','1');

/*Table structure for table `isp` */

DROP TABLE IF EXISTS `isp`;

CREATE TABLE `isp` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_isp` varchar(100) NOT NULL,
  `nama_isp` varchar(100) NOT NULL,
  `nomor` varchar(100) NOT NULL,
  `site` varchar(100) NOT NULL,
  `bandwith` varchar(50) NOT NULL,
  `user_create` varchar(100) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(100) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 ;

/*Data for the table `isp` */

insert  into `isp`(`id`,`id_isp`,`nama_isp`,`nomor`,`site`,`bandwith`,`user_create`,`create_date`,`user_update`,`update_date`) values 
(1,'12324234','Indihomes','192989238','FO','','Admin Gudang','2021-06-11','Admin Gudang','2021-06-11'),
(4,'ID ISP','Provider ISP','Nomor','SITE','Bandwith','Admin Gudang','2021-06-15','','0000-00-00'),
(5,'123213','Telkom','198238','Yogyakarta','124mb','Admin Gudang','2021-06-15','','0000-00-00'),
(6,'','','','','','Leni Yuniar','2021-06-15','','0000-00-00'),
(7,'','','','','','Leni Yuniar','2021-06-15','','0000-00-00'),
(8,'ID ISP','Provider ISP','Nomor','SITE','Bandwith','Leni Yuniar','2021-06-15','','0000-00-00'),
(9,'122401235373','Indihome','-','Serang ','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(10,'172704271812','Indihome','-','Palu','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(11,'172825803820','Indihome','-','Mamuju','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(12,'172926203871','Indihome','-','Manokwari','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(13,'172324204152','Indihome','-','Ternate','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(14,'172001795046','Indihome','-','Ambon','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(15,'111721111923','Indihome','-','Pangkalpinang','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(16,'161314000125','Indihome','-','Samarinda','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(17,'172316234411','Indihome','-','Gorontalo','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(18,'111744102174','Indihome','-','Ogan Komering Ilir','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(19,'111711105691','Indihome','-','Ogan Ilir','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(20,'3100121966','Biznet','-','FO','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(21,'3100025162','Biznet','-','Base Karawang','-','Leni Yuniar','2021-06-15','','0000-00-00');

/*Table structure for table `jb` */

DROP TABLE IF EXISTS `jb`;

CREATE TABLE `jb` (
  `id_jenis` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_jenis`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ;

/*Data for the table `jb` */

insert  into `jb`(`id_jenis`,`nama`) values 
(1,'sensor'),
(2,'board'),
(3,'modul'),
(4,'komponen aktif'),
(5,'komponen pasif'),
(6,'lain-lain'),
(7,'board mcu'),
(8,'konektor');

/*Table structure for table `keluar` */

DROP TABLE IF EXISTS `keluar`;

CREATE TABLE `keluar` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kd_trans` varchar(100) NOT NULL,
  `nama_brg` varchar(100) NOT NULL,
  `tgl_keluar` date NOT NULL,
  `jumlah` int NOT NULL,
  `proyek` varchar(100) NOT NULL,
  `id_petugas` int NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 ;

/*Data for the table `keluar` */

insert  into `keluar`(`id`,`kd_trans`,`nama_brg`,`tgl_keluar`,`jumlah`,`proyek`,`id_petugas`,`user_create`,`create_date`,`user_update`,`update_date`) values 
(7,'001',' Box 150x200x100 mm','2021-06-05',12,'',0,'','0000-00-00','','0000-00-00'),
(8,'OUT-9921-0002',' Box 150x200x100 mm','2021-06-09',32,'',0,'','0000-00-00','','0000-00-00'),
(9,'OUT-9921-0003','XXX','2021-06-04',12,'',0,'','0000-00-00','','0000-00-00'),
(10,'OUT-9921-0004','Selang Bakar 8 mm','2021-06-05',12,'',0,'','0000-00-00','','0000-00-00'),
(11,'OUT-9921-0005','Resistor 2K2','2021-06-03',5,'SPARING',0,'','0000-00-00','Dani Hamdani','2021-06-07'),
(12,'OUT-9921-0006','seb','2021-06-04',3,'KLHK',0,'','0000-00-00','','0000-00-00'),
(13,'OUT-9921-0007','XXX','2021-06-07',4,'SPARING',0,'Admin Gudang','2021-06-07','','0000-00-00'),
(14,'OUT-9921-0008','Resistor 100 Ohm','2021-06-10',1,'Lain-Lain',11,'Beben Ubaedillah','2021-06-10','','0000-00-00');

/*Table structure for table `keluars` */

DROP TABLE IF EXISTS `keluars`;

CREATE TABLE `keluars` (
  `id_transaksi` varchar(12) DEFAULT NULL,
  `id_brg` varchar(100) DEFAULT NULL,
  `tanggal_keluar` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `id_petugas` int DEFAULT NULL,
  `jumlah_x` int DEFAULT NULL,
  `stock` int DEFAULT NULL,
  `proyek` varchar(100) DEFAULT NULL,
  `user_create` varchar(100) DEFAULT NULL,
  `create_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

/*Data for the table `keluars` */

insert  into `keluars`(`id_transaksi`,`id_brg`,`tanggal_keluar`,`status`,`id_petugas`,`jumlah_x`,`stock`,`proyek`,`user_create`,`create_date`) values 
('20210616001','171','2021-06-16','N',8,23,NULL,'SPARING','Admin Gudang','2021-06-16'),
('20210616002','232','2021-06-16','N',8,2,NULL,'All','Admin Gudang','2021-06-16'),
('20210616003','255','2021-06-16','N',1,2,NULL,'KLHK','Dani Hamdani','2021-06-16');

/*Table structure for table `kembali` */

DROP TABLE IF EXISTS `kembali`;

CREATE TABLE `kembali` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kd_trans` varchar(100) NOT NULL,
  `reference` varchar(50) DEFAULT NULL,
  `nama_brg` varchar(100) NOT NULL,
  `jumlah_k` int NOT NULL,
  `tgl_kembali` date NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 ;

/*Data for the table `kembali` */

insert  into `kembali`(`id`,`kd_trans`,`reference`,`nama_brg`,`jumlah_k`,`tgl_kembali`,`user_create`,`create_date`,`user_update`,`update_date`) values 
(13,'KBL-9921-0001','PNJ-9921-0001','Resistor 2K2',2,'2021-06-05','0','0000-00-00','0','0000-00-00'),
(14,'KBL-9921-0002','PNJ-9921-0001','Resistor 2K2',2,'2021-06-04','0','0000-00-00','0','0000-00-00'),
(15,'KBL-9921-0003','PNJ-9921-0001','Resistor 2K2',2,'2021-06-05','0','0000-00-00','0','0000-00-00'),
(17,'KBL-9921-0004','PNJ-9921-0001','Resistor 2K2',2,'2021-06-07','0','0000-00-00','Admin Gudang','2021-06-07'),
(18,'KBL-9921-0005','PNJ-9921-0001','Resistor 2K2',0,'2021-06-05','0','0000-00-00','0','0000-00-00'),
(19,'KBL-9921-0006','PNJ-9921-0002','XXX',0,'2021-06-09','Admin Gudang','2021-06-07','','0000-00-00'),
(22,'KBL-9921-0007','PNJ-9921-0002','XXX',10,'2021-06-09','Admin Gudang','2021-06-07','','0000-00-00');

/*Table structure for table `listrik` */

DROP TABLE IF EXISTS `listrik`;

CREATE TABLE `listrik` (
  `id_` int NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) NOT NULL,
  `id_pelanggan` varchar(100) NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `user_create` varchar(50) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `user_update` varchar(50) DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  PRIMARY KEY (`id_`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ;

/*Data for the table `listrik` */

insert  into `listrik`(`id_`,`jenis`,`id_pelanggan`,`tempat`,`user_create`,`create_date`,`user_update`,`update_date`) values 
(1,'Prabayar','1212321','Mantap','Admin Gudang','2021-06-11','Admin Gudang','2021-06-11'),
(2,'Pascabayar','72129231','xxxx','Admin Gudang','2021-06-15','Admin Gudang','2021-06-16'),
(3,'Pascabayar','535596330066','FO','Leni Yuniar','2021-06-15',NULL,NULL),
(4,'Token','535596898362','FO','Leni Yuniar','2021-06-15',NULL,NULL),
(5,'Pascabayar','534685702891','Anggadita','Leni Yuniar','2021-06-15',NULL,NULL),
(6,'Token','45061907171','Gintungkerta','Leni Yuniar','2021-06-15',NULL,NULL),
(7,'Token','45016942323','Base Karawang','Leni Yuniar','2021-06-15',NULL,NULL),
(8,'Pascabayar','533113011861','Base Cirebon','Leni Yuniar','2021-06-15',NULL,NULL);

/*Table structure for table `masuk` */

DROP TABLE IF EXISTS `masuk`;

CREATE TABLE `masuk` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kd_trans` varchar(100) NOT NULL,
  `nama_brg` varchar(100) NOT NULL,
  `merk` varchar(100) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `jumlah` int NOT NULL,
  `supplier` varchar(100) NOT NULL,
  `harga` int NOT NULL,
  `link` varchar(100) NOT NULL,
  `id_petugas` int NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 ;

/*Data for the table `masuk` */

insert  into `masuk`(`id`,`kd_trans`,`nama_brg`,`merk`,`tgl_masuk`,`jumlah`,`supplier`,`harga`,`link`,`id_petugas`,`user_create`,`create_date`,`user_update`,`update_date`) values 
(20,'IN-9921-0006','eCHEM TpH 10m','','2021-06-07',1,'TriOS',8820000,'https://www.trios.de/en/',0,'Adhitya Chasani Wicaksono','2021-06-09','','0000-00-00'),
(21,'IN-9921-0007','Suspended Solids 30 g/L 10m','','2021-06-07',1,'TriOS',32616000,'https://www.trios.de/en/',0,'Adhitya Chasani Wicaksono','2021-06-09','','0000-00-00'),
(22,'IN-9921-0008','LISA-01-254-VA-D-C / COD','','2021-06-07',1,'TriOS',108576000,'https://www.trios.de/en/',0,'Adhitya Chasani Wicaksono','2021-06-09','','0000-00-00'),
(23,'IN-9921-0009','W55 / Wiper','','2021-06-07',1,'TriOS',17640000,'https://www.trios.de/en/',0,'Adhitya Chasani Wicaksono','2021-06-09','','0000-00-00'),
(24,'IN-9921-0010','wiper blade 1mm path','','2021-06-07',1,'TriOS',2160000,'https://www.trios.de/en/',0,'Adhitya Chasani Wicaksono','2021-06-09','','0000-00-00');

/*Table structure for table `masuks` */

DROP TABLE IF EXISTS `masuks`;

CREATE TABLE `masuks` (
  `id_transaksi` varchar(12) DEFAULT NULL,
  `id_brg` varchar(100) DEFAULT NULL,
  `tanggal_masuk` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `id_petugas` int DEFAULT NULL,
  `jumlah_x` int DEFAULT NULL,
  `merk` varchar(100) DEFAULT NULL,
  `harga` int DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `user_create` varchar(100) DEFAULT NULL,
  `create_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

/*Data for the table `masuks` */

insert  into `masuks`(`id_transaksi`,`id_brg`,`tanggal_masuk`,`status`,`id_petugas`,`jumlah_x`,`merk`,`harga`,`link`,`user_create`,`create_date`) values 
('20210614037','159','2021-06-14','N',8,7,'KLHK',NULL,NULL,'Admin Gudang','2021-06-14'),
('20210614038','148','2021-06-14','N',8,6,'SPARING',NULL,NULL,'Admin Gudang','2021-06-14'),
('20210614038','146','2021-06-14','N',8,2,'SPARING',NULL,NULL,'Admin Gudang','2021-06-14'),
('20210614038','101','2021-06-14','N',8,2,'SPARING',NULL,NULL,'Admin Gudang','2021-06-14'),
('20210616039','143',NULL,'N',8,12,'Apa aja lah tes ini',NULL,NULL,'Admin Gudang','2021-06-16'),
('20210616040','163',NULL,'N',8,22,'Apa aja lah tes iniq',NULL,NULL,'Admin Gudang','2021-06-16'),
('20210616040','155',NULL,'N',8,12,'Apa aja lah tes ini',NULL,NULL,'Admin Gudang','2021-06-16'),
('20210616041','157',NULL,'N',8,2,'ssd',2000,NULL,'Admin Gudang','2021-06-16'),
('20210616042','157',NULL,'N',8,12,'Apa aja lah tes ini',2000,'https://www.youtube.com/watch?v=SUd7gDhZy7w','Admin Gudang','2021-06-16'),
('20210616043','136',NULL,'N',8,4,'Apa aja lah tes ini',2000,'https://www.youtube.com/watch?v=8-Z_M12hG4w','Admin Gudang','2021-06-16'),
('20210616043','115',NULL,'N',8,14,'Apa aja lah tes ini',2000,'https://www.youtube.com/watch?v=m8ElrTL8YZU','Admin Gudang','2021-06-16'),
('20210616044','133',NULL,'N',8,1,'Lolin',35000,'tokopedia.com','Admin Gudang','2021-06-16');

/*Table structure for table `pasca` */

DROP TABLE IF EXISTS `pasca`;

CREATE TABLE `pasca` (
  `id` int NOT NULL AUTO_INCREMENT,
  `provider` varchar(100) NOT NULL,
  `nomor` varchar(20) NOT NULL,
  `type` varchar(100) NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `pic_cont` varchar(20) NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 ;

/*Data for the table `pasca` */

insert  into `pasca`(`id`,`provider`,`nomor`,`type`,`tempat`,`pic_cont`,`user_create`,`create_date`,`user_update`,`update_date`) values 
(13,'AS','1272137','ada','bandung','128931','Admin Gudang','2021-06-15','','0000-00-00'),
(14,'Telkomsel','081119507105','GSM','Palu','081257375712','Leni Yuniar','2021-06-15','','0000-00-00'),
(15,'Telkomsel','081119507103','GSM','Mamuju','081354397174','Leni Yuniar','2021-06-15','','0000-00-00'),
(16,'Telkomsel','081119507102','GSM','Kendari','08114055364','Leni Yuniar','2021-06-15','','0000-00-00'),
(17,'Telkomsel','081119507104','GSM','Manokwari','082399215025','Leni Yuniar','2021-06-15','','0000-00-00'),
(18,'Telkomsel','081119507109','GSM','Ternate','082293080010','Leni Yuniar','2021-06-15','','0000-00-00'),
(19,'Telkomsel','081119507100','GSM','Ambon','085312587397','Leni Yuniar','2021-06-15','','0000-00-00'),
(20,'Telkomsel','081119507106','GSM','Pangkalpinang','085768318635','Leni Yuniar','2021-06-15','','0000-00-00'),
(21,'Telkomsel','081119507107','GSM','Samarinda','085932522824','Leni Yuniar','2021-06-15','','0000-00-00'),
(22,'Telkomsel','081119507101','GSM','Gorontalo','082258860308','Leni Yuniar','2021-06-15','','0000-00-00'),
(23,'Telkomsel','08112343192','GSM','Besland','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(24,'Telkomsel','08112003192','GSM','Indotaisei','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(25,'Telkomsel','08112193031','GSM','Indorama Purwakarta','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(26,'Telkomsel','08112437700','GSM','Gistex','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(27,'Telkomsel','08112487700','GSM','PMT ','-','Leni Yuniar','2021-06-15','','0000-00-00'),
(28,'Telkomsel','08112497700','GSM','Indorama Padalarang','-','Leni Yuniar','2021-06-15','','0000-00-00');

/*Table structure for table `petugas` */

DROP TABLE IF EXISTS `petugas`;

CREATE TABLE `petugas` (
  `id_petugas` int NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;

/*Data for the table `petugas` */

insert  into `petugas`(`id_petugas`,`username`,`full_name`,`password`,`status`) values 
(1,'dani','Dani Hamdani','$2y$05$lJRL04Haqo0nq8fefLtLu.dVl./M403Xo3/7f5cVmMIehEPvfVRRa','Admin'),
(8,'admin','Admin Gudang','$2y$05$0RfFGKdD.I9/9SRZd9../.kIQg7pwgDxhICT0t1aPZh29Ia2oRA3u','Super Admin'),
(10,'adhitsensync','Adhitya Chasani Wicaksono','$2y$05$ElUqN5/Vs990DcvIrUFQPOhuPbyfbmK4N9ZBlOFJQRVwNz/vz.IKS','Super Admin'),
(11,'bebensensync','Beben Ubaedillah','$2y$05$dETcp00bYvG8fJSm3VpWMem9AWg9yKbZNHATYepuGUgFKJhQJ2qJu','Super Admin'),
(12,'lenisensync','Leni Yuniar','$2y$05$CfTNK5f3M.JhVF5l8/XMv.rUS5V5UOs9GzdXU2oftrUV0qfzcl1Ny','Super Admin');

/*Table structure for table `pinjam` */

DROP TABLE IF EXISTS `pinjam`;

CREATE TABLE `pinjam` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kd_trans` varchar(100) NOT NULL,
  `nama_brg` varchar(100) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `jumlah` int NOT NULL,
  `proyek` varchar(100) NOT NULL,
  `id_petugas` int NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 ;

/*Data for the table `pinjam` */

insert  into `pinjam`(`id`,`kd_trans`,`nama_brg`,`tgl_pinjam`,`jumlah`,`proyek`,`id_petugas`,`user_create`,`create_date`,`user_update`,`update_date`) values 
(13,'PNJ-9921-0001','Resistor 2K2','2021-06-05',4,'All',0,'','0000-00-00','Admin Gudang','2021-06-07'),
(14,'PNJ-9921-0002','XXX','2021-06-02',0,'KLHK',0,'Admin Gudang','2021-06-07','','0000-00-00'),
(15,'PNJ-9921-0003','eCHEM TpH 10m','2021-06-15',2,'KLHK',1,'Dani Hamdani','2021-06-15','','0000-00-00');

/*Table structure for table `proyek` */

DROP TABLE IF EXISTS `proyek`;

CREATE TABLE `proyek` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_proyek` int NOT NULL,
  `nm_proyek` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

/*Data for the table `proyek` */

/*Table structure for table `tmp` */

DROP TABLE IF EXISTS `tmp`;

CREATE TABLE `tmp` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_brg` varchar(10) DEFAULT NULL,
  `nama_brg` varchar(100) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `jumlah_x` int DEFAULT NULL,
  `proyek` varchar(100) DEFAULT NULL,
  `stock` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb3;

/*Data for the table `tmp` */

/*Table structure for table `tmp_klr` */

DROP TABLE IF EXISTS `tmp_klr`;

CREATE TABLE `tmp_klr` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_brg` varchar(10) DEFAULT NULL,
  `nama_brg` varchar(100) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `jumlah_x` int NOT NULL,
  `stock` int NOT NULL,
  `proyek` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb3;

/*Data for the table `tmp_klr` */

/*Table structure for table `tmp_msk` */

DROP TABLE IF EXISTS `tmp_msk`;

CREATE TABLE `tmp_msk` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_brg` varchar(10) DEFAULT NULL,
  `nama_brg` varchar(100) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `jumlah_x` int DEFAULT NULL,
  `merk` varchar(100) DEFAULT NULL,
  `harga` int DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb3;

/*Data for the table `tmp_msk` */

/*Table structure for table `transaksi` */

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `id_transaksi` varchar(12) DEFAULT NULL,
  `id_brg` varchar(100) DEFAULT NULL,
  `tanggal_pinjam` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `id_petugas` int DEFAULT NULL,
  `jumlah_x` int DEFAULT NULL,
  `stock` int NOT NULL,
  `proyek` varchar(100) DEFAULT NULL,
  `user_create` varchar(100) DEFAULT NULL,
  `create_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

/*Data for the table `transaksi` */

insert  into `transaksi`(`id_transaksi`,`id_brg`,`tanggal_pinjam`,`status`,`id_petugas`,`jumlah_x`,`stock`,`proyek`,`user_create`,`create_date`) values 
('20210614037','159','2021-06-14','N',8,7,0,'KLHK','Admin Gudang','2021-06-14'),
('20210614038','148','2021-06-14','N',8,6,0,'SPARING','Admin Gudang','2021-06-14'),
('20210614038','146','2021-06-14','N',8,2,0,'SPARING','Admin Gudang','2021-06-14'),
('20210614038','101','2021-06-14','N',8,2,0,'SPARING','Admin Gudang','2021-06-14'),
('20210614037','159','2021-06-14','N',8,7,0,'KLHK','Admin Gudang','2021-06-14'),
('20210614038','148','2021-06-14','N',8,6,0,'SPARING','Admin Gudang','2021-06-14'),
('20210614038','146','2021-06-14','N',8,2,0,'SPARING','Admin Gudang','2021-06-14'),
('20210614038','101','2021-06-14','N',8,2,0,'SPARING','Admin Gudang','2021-06-14'),
('20210614037','159','2021-06-14','N',8,7,0,'KLHK','Admin Gudang','2021-06-14'),
('20210614038','148','2021-06-14','N',8,6,0,'SPARING','Admin Gudang','2021-06-14'),
('20210614038','146','2021-06-14','N',8,2,0,'SPARING','Admin Gudang','2021-06-14'),
('20210614038','101','2021-06-14','N',8,2,0,'SPARING','Admin Gudang','2021-06-14');

/* Trigger structure for table `keluar` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `TG_Barang_Keluar` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'maliak09'@'localhost' */ /*!50003 TRIGGER `TG_Barang_Keluar` AFTER INSERT ON `keluar` FOR EACH ROW BEGIN

 UPDATE barang SET stock=stock-NEW.jumlah



 WHERE nama_brg=NEW.nama_brg;



END */$$


DELIMITER ;

/* Trigger structure for table `keluars` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `TG_keluars` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'maliak09'@'localhost' */ /*!50003 TRIGGER `TG_keluars` AFTER INSERT ON `keluars` FOR EACH ROW BEGIN

 UPDATE barang SET stock=stock-NEW.jumlah_x



 WHERE id_brg=NEW.id_brg;



END */$$


DELIMITER ;

/* Trigger structure for table `kembali` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `TG_Barang_Kembali` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'maliak09'@'localhost' */ /*!50003 TRIGGER `TG_Barang_Kembali` AFTER INSERT ON `kembali` FOR EACH ROW BEGIN

 UPDATE barang SET stock=stock+NEW.jumlah_k



 WHERE nama_brg=NEW.nama_brg;



END */$$


DELIMITER ;

/* Trigger structure for table `kembali` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `TG_kurangiPinjam` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'maliak09'@'localhost' */ /*!50003 TRIGGER `TG_kurangiPinjam` AFTER INSERT ON `kembali` FOR EACH ROW BEGIN

 UPDATE pinjam SET jumlah=jumlah-NEW.jumlah_k



 WHERE nama_brg=NEW.nama_brg;



END */$$


DELIMITER ;

/* Trigger structure for table `masuk` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `TG_Barang_masuk` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'maliak09'@'localhost' */ /*!50003 TRIGGER `TG_Barang_masuk` AFTER INSERT ON `masuk` FOR EACH ROW BEGIN

 UPDATE barang SET stock=stock+NEW.jumlah



 WHERE nama_brg=NEW.nama_brg;



END */$$


DELIMITER ;

/* Trigger structure for table `masuks` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `masuksTG` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'maliak09'@'localhost' */ /*!50003 TRIGGER `masuksTG` AFTER INSERT ON `masuks` FOR EACH ROW BEGIN

 UPDATE barang SET stock=stock-NEW.jumlah_x



 WHERE id_brg=NEW.id_brg;



END */$$


DELIMITER ;

/* Trigger structure for table `pinjam` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `TG_Barang_pinjam` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'maliak09'@'localhost' */ /*!50003 TRIGGER `TG_Barang_pinjam` AFTER INSERT ON `pinjam` FOR EACH ROW BEGIN

 UPDATE barang SET stock=stock-NEW.jumlah



 WHERE nama_brg=NEW.nama_brg;



END */$$


DELIMITER ;

/* Trigger structure for table `transaksi` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `TG_Barang_pinjams` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'maliak09'@'localhost' */ /*!50003 TRIGGER `TG_Barang_pinjams` AFTER INSERT ON `transaksi` FOR EACH ROW BEGIN

 UPDATE barang SET stock=stock-NEW.jumlah_x



 WHERE id_brg=NEW.id_brg;



END */$$


DELIMITER ;

/*Table structure for table `barang_masuk` */

DROP TABLE IF EXISTS `barang_masuk`;

/*!50001 DROP VIEW IF EXISTS `barang_masuk` */;
/*!50001 DROP TABLE IF EXISTS `barang_masuk` */;

/*!50001 CREATE TABLE  `barang_masuk`(
 `id_transaksi` varchar(12) ,
 `tanggal_masuk` date ,
 `id_brg` varchar(100) ,
 `jumlah_x` int ,
 `merk` varchar(100) ,
 `harga` int ,
 `link` varchar(100) ,
 `user_create` varchar(100) ,
 `create_date` date ,
 `nama_brg` varchar(100) 
)*/;

/*Table structure for table `barangs` */

DROP TABLE IF EXISTS `barangs`;

/*!50001 DROP VIEW IF EXISTS `barangs` */;
/*!50001 DROP TABLE IF EXISTS `barangs` */;

/*!50001 CREATE TABLE  `barangs`(
 `id_brg` int ,
 `nama_brg` varchar(100) ,
 `jenis` varchar(100) ,
 `satuan` varchar(20) ,
 `stock` int ,
 `nomer_box` varchar(50) ,
 `kategori` varchar(25) 
)*/;

/*Table structure for table `barangx` */

DROP TABLE IF EXISTS `barangx`;

/*!50001 DROP VIEW IF EXISTS `barangx` */;
/*!50001 DROP TABLE IF EXISTS `barangx` */;

/*!50001 CREATE TABLE  `barangx`(
 `id_brg` int ,
 `nama_brg` varchar(100) ,
 `jenis` varchar(100) ,
 `satuan` varchar(20) ,
 `stock` int ,
 `nomer_box` varchar(50) ,
 `kategori` varchar(25) ,
 `user_create` varchar(50) ,
 `create_date` date ,
 `user_update` varchar(50) ,
 `update_date` date 
)*/;

/*Table structure for table `keluar_brg` */

DROP TABLE IF EXISTS `keluar_brg`;

/*!50001 DROP VIEW IF EXISTS `keluar_brg` */;
/*!50001 DROP TABLE IF EXISTS `keluar_brg` */;

/*!50001 CREATE TABLE  `keluar_brg`(
 `id_transaksi` varchar(12) ,
 `tanggal_keluar` date ,
 `id_brg` varchar(100) ,
 `jumlah_x` int ,
 `proyek` varchar(100) ,
 `user_create` varchar(100) ,
 `create_date` date ,
 `nama_brg` varchar(100) 
)*/;

/*Table structure for table `peminjaman` */

DROP TABLE IF EXISTS `peminjaman`;

/*!50001 DROP VIEW IF EXISTS `peminjaman` */;
/*!50001 DROP TABLE IF EXISTS `peminjaman` */;

/*!50001 CREATE TABLE  `peminjaman`(
 `id_transaksi` varchar(12) ,
 `tanggal_pinjam` date ,
 `id_brg` varchar(100) ,
 `jumlah_x` int ,
 `proyek` varchar(100) ,
 `user_create` varchar(100) ,
 `create_date` date ,
 `nama_brg` varchar(100) 
)*/;

/*View structure for view barang_masuk */

/*!50001 DROP TABLE IF EXISTS `barang_masuk` */;
/*!50001 DROP VIEW IF EXISTS `barang_masuk` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `barang_masuk` AS select `masuks`.`id_transaksi` AS `id_transaksi`,`masuks`.`tanggal_masuk` AS `tanggal_masuk`,`masuks`.`id_brg` AS `id_brg`,`masuks`.`jumlah_x` AS `jumlah_x`,`masuks`.`merk` AS `merk`,`masuks`.`harga` AS `harga`,`masuks`.`link` AS `link`,`masuks`.`user_create` AS `user_create`,`masuks`.`create_date` AS `create_date`,`barang`.`nama_brg` AS `nama_brg` from (`masuks` left join `barang` on((`masuks`.`id_brg` = `barang`.`id_brg`))) */;

/*View structure for view barangs */

/*!50001 DROP TABLE IF EXISTS `barangs` */;
/*!50001 DROP VIEW IF EXISTS `barangs` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `barangs` AS select `barang`.`id_brg` AS `id_brg`,`barang`.`nama_brg` AS `nama_brg`,`barang`.`jenis` AS `jenis`,`barang`.`satuan` AS `satuan`,`barang`.`stock` AS `stock`,`barang`.`nomer_box` AS `nomer_box`,`barang`.`kategori` AS `kategori` from `barang` where (`barang`.`kategori` = 'Asset') */;

/*View structure for view barangx */

/*!50001 DROP TABLE IF EXISTS `barangx` */;
/*!50001 DROP VIEW IF EXISTS `barangx` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `barangx` AS select `barang`.`id_brg` AS `id_brg`,`barang`.`nama_brg` AS `nama_brg`,`barang`.`jenis` AS `jenis`,`barang`.`satuan` AS `satuan`,`barang`.`stock` AS `stock`,`barang`.`nomer_box` AS `nomer_box`,`barang`.`kategori` AS `kategori`,`barang`.`user_create` AS `user_create`,`barang`.`create_date` AS `create_date`,`barang`.`user_update` AS `user_update`,`barang`.`update_date` AS `update_date` from `barang` where (`barang`.`kategori` = 'Habis Pakai') */;

/*View structure for view keluar_brg */

/*!50001 DROP TABLE IF EXISTS `keluar_brg` */;
/*!50001 DROP VIEW IF EXISTS `keluar_brg` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `keluar_brg` AS select `keluars`.`id_transaksi` AS `id_transaksi`,`keluars`.`tanggal_keluar` AS `tanggal_keluar`,`keluars`.`id_brg` AS `id_brg`,`keluars`.`jumlah_x` AS `jumlah_x`,`keluars`.`proyek` AS `proyek`,`keluars`.`user_create` AS `user_create`,`keluars`.`create_date` AS `create_date`,`barangx`.`nama_brg` AS `nama_brg` from (`keluars` left join `barangx` on((`keluars`.`id_brg` = `barangx`.`id_brg`))) */;

/*View structure for view peminjaman */

/*!50001 DROP TABLE IF EXISTS `peminjaman` */;
/*!50001 DROP VIEW IF EXISTS `peminjaman` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `peminjaman` AS select `transaksi`.`id_transaksi` AS `id_transaksi`,`transaksi`.`tanggal_pinjam` AS `tanggal_pinjam`,`transaksi`.`id_brg` AS `id_brg`,`transaksi`.`jumlah_x` AS `jumlah_x`,`transaksi`.`proyek` AS `proyek`,`transaksi`.`user_create` AS `user_create`,`transaksi`.`create_date` AS `create_date`,`barangs`.`nama_brg` AS `nama_brg` from (`transaksi` left join `barangs` on((`transaksi`.`id_brg` = `barangs`.`id_brg`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
