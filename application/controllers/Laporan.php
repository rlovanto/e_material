<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_laporan'));
    }
    

    public function barang()
    {
        $keyword = $this->input->get('cari', TRUE);
        $data['barang']      = $this->Mod_laporan->getAllBarang($keyword);
        $this->template->load('layoutbackend', 'laporan/barang_data', $data);
    }
    public function card()
    {
        $keyword = $this->input->get('cari', TRUE);
        $data['card']      = $this->Mod_laporan->getAllCard($keyword);
        $this->template->load('layoutbackend', 'laporan/card_data', $data);
    }
    public function pasca()
    {
        $keyword = $this->input->get('cari', TRUE);
        $data['pasca']      = $this->Mod_laporan->getAllPasca($keyword);
        $this->template->load('layoutbackend', 'laporan/pasca_data', $data);
    }
    public function masuk()
    {
        $tanggal1 = $this->input->get('tanggalMulai', TRUE);
        $tanggal2 = $this->input->get('tanggalAkhir', TRUE);
        $data['masuk']      = $this->Mod_laporan->getAllMasuk($tanggal1, $tanggal2);
        $this->template->load('layoutbackend', 'laporan/masuk_data', $data);
    }
    public function keluar()
    {
        $tanggal1 = $this->input->get('tanggalMulai', TRUE);
        $tanggal2 = $this->input->get('tanggalAkhir', TRUE);
        $data['keluar']      = $this->Mod_laporan->getAllKeluar($tanggal1, $tanggal2);
        $this->template->load('layoutbackend', 'laporan/keluar_data', $data);
    }
    public function pinjam()
    {
        $tanggal1 = $this->input->get('tanggalMulai', TRUE);
        $tanggal2 = $this->input->get('tanggalAkhir', TRUE);
        $data['pinjam']      = $this->Mod_laporan->getAllPinjam($tanggal1, $tanggal2);
        $this->template->load('layoutbackend', 'laporan/pinjam_data', $data);
    }
    public function kembali()
    {
        $tanggal1 = $this->input->get('tanggalMulai', TRUE);
        $tanggal2 = $this->input->get('tanggalAkhir', TRUE);
        $data['kembali']      = $this->Mod_laporan->getAllKembali($tanggal1, $tanggal2);
        $this->template->load('layoutbackend', 'laporan/kembali_data', $data);
    }
    public function users()
    {
        $keyword = $this->input->get('cari', TRUE);
        $data['users']      = $this->Mod_laporan->getAllUsers($keyword)->result();
        $this->template->load('layoutbackend', 'laporan/users_data', $data);
    }

    public function buku()
    {
        $keyword = $this->input->get('cari', TRUE);
        $data['buku']      = $this->Mod_laporan->getAllBuku($keyword);
        $this->template->load('layoutbackend', 'laporan/buku_data', $data);
    }

    public function majalah()
    {
        $tanggal1 = $this->input->get('tanggalMulai', TRUE);
        $tanggal2 = $this->input->get('tanggalAkhir', TRUE);
        $data['majalah']      = $this->Mod_laporan->getAllMajalah($tanggal1, $tanggal2);
        $this->template->load('layoutbackend', 'laporan/majalah_data', $data);
    }

    public function kliping()
    {
        $tanggal1 = $this->input->get('tanggalMulai', TRUE);
        $tanggal2 = $this->input->get('tanggalAkhir', TRUE);
        $data['kliping']      = $this->Mod_laporan->getAllKliping($tanggal1, $tanggal2);
        $this->template->load('layoutbackend', 'laporan/kliping_data', $data);
    }

    public function ebook()
    {
        $keyword = $this->input->get('cari', TRUE);
        $data['ebook']      = $this->Mod_laporan->getAllEbook($keyword);
        $this->template->load('layoutbackend', 'laporan/ebook_data', $data);
    }

    public function maps()
    {
        $keyword = $this->input->get('cari', TRUE);
        $data['maps']      = $this->Mod_laporan->getAllMaps($keyword);
        $this->template->load('layoutbackend', 'laporan/maps_data', $data);
    }

    public function flipbook()
    {
        $tanggal1 = $this->input->get('tanggalMulai', TRUE);
        $tanggal2 = $this->input->get('tanggalAkhir', TRUE);
        $data['flipbook']      = $this->Mod_laporan->getAllFlipbook($tanggal1, $tanggal2);
        $this->template->load('layoutbackend', 'laporan/flipbook_data', $data);
    }

    public function kegiatan()
    {
        $tanggal1 = $this->input->get('tanggalMulai', TRUE);
        $tanggal2 = $this->input->get('tanggalAkhir', TRUE);
        $data['kegiatan']      = $this->Mod_laporan->getAllKegiatan($tanggal1, $tanggal2);
        $this->template->load('layoutbackend', 'laporan/kegiatan_data', $data);
    }

    public function pengumuman()
    {
        $tanggal1 = $this->input->get('tanggalMulai', TRUE);
        $tanggal2 = $this->input->get('tanggalAkhir', TRUE);
        $data['pengumuman']      = $this->Mod_laporan->getAllPengumuman($tanggal1, $tanggal2);
        $this->template->load('layoutbackend', 'laporan/pengumuman_data', $data);
    }
    
    public function fasilitas()
    {
        $keyword = $this->input->get('cari', TRUE);
        $data['fasilitas']      = $this->Mod_laporan->getAllFasilitas($keyword);
        $this->template->load('layoutbackend', 'laporan/fasilitas_data', $data);
    }
    
    public function layanan()
    {
        $keyword = $this->input->get('cari', TRUE);
        $data['layanan']      = $this->Mod_laporan->getAllLayanan($keyword);
        $this->template->load('layoutbackend', 'laporan/layanan_data', $data);
    }

    public function news()
    {
        $tanggal1 = $this->input->get('tanggalMulai', TRUE);
        $tanggal2 = $this->input->get('tanggalAkhir', TRUE);
        $data['news']      = $this->Mod_laporan->getAllNews($tanggal1, $tanggal2);
        $this->template->load('layoutbackend', 'laporan/news_data', $data);
    }

    public function artikel()
    {
        $tanggal1 = $this->input->get('tanggalMulai', TRUE);
        $tanggal2 = $this->input->get('tanggalAkhir', TRUE);
        $data['artikel']      = $this->Mod_laporan->getAllArtikel($tanggal1, $tanggal2);
        $this->template->load('layoutbackend', 'laporan/artikel_data', $data);
    }

    public function peminjaman()
    {
        $data['title']="Laporan Peminjaman";
        $this->template->load('layoutbackend', 'laporan/peminjaman_data', $data);
    }

    public function cari_pinjaman()
    {
        // $tanggal1 = '2018-04-11';
        // $tanggal2 = '2018-04-17';
        $tanggal1 = $this->input->post('tanggal1');
        $tanggal2 = $this->input->post('tanggal2');
        $data['hasil_search'] = $this->Mod_laporan->searchPinjaman($tanggal1,$tanggal2);
        $this->load->view('laporan/peminjaman_search', $data);
    }

    public function detail_pinjam()
    {
        //$id_transaksi = $this->uri->segment(3);
        $id_transaksi = $this->input->post('id_transaksi');
        
        $data['title']        = "Detail Peminjaman";
        $data['kembali']       = $this->Mod_laporan->detailPinjaman($id_transaksi)->row_array(); 
        $data['detailpinjam'] = $this->Mod_laporan->detailPinjaman($id_transaksi)->result();
        $this->load->view('laporan/peminjaman_detail', $data);

        // $id_transaksi = $this->uri->segment(3);
        // $data['title']        = "Detail Peminjaman";
        // $data['kembali']       = $this->Mod_laporan->detailPinjaman($id_transaksi)->row_array(); 
        // $data['detailpinjam'] = $this->Mod_laporan->detailPinjaman($id_transaksi)->result();
        // $this->template->load('layoutbackend', 'laporan/peminjaman_detail', $data);
    }

    public function pengembalian()
    {
        $data['title']="Laporan Pengembalian";
        $this->template->load('layoutbackend', 'laporan/pengembalian_data', $data);
    }

    public function cari_pengembalian()
    {
        // $tanggal1 = '2018-04-19';
        // $tanggal2 = '2018-04-21';
        $tanggal1 = $this->input->post('tanggal1');
        $tanggal2 = $this->input->post('tanggal2');
        $data['hasil_search'] = $this->Mod_laporan->searchPengembalian($tanggal1,$tanggal2);
        $this->load->view('laporan/pengembalian_search', $data);
        
    }

    public function detail_pengembalian()
    {
        $id_transaksi = $this->input->post('id_transaksi');
        $data['title']               = "Detail Pengembalian";
        $data['pengembalian']        = $this->Mod_laporan->detailPengembalian($id_transaksi)->row_array(); 
        $data['detailjpengembalian'] = $this->Mod_laporan->detailPengembalian($id_transaksi)->result();
        $this->load->view('laporan/pengembalian_detail', $data);

        // $id_transaksi = $this->uri->segment(3);
        // $data['title']               = "Detail Pengembalian";
        // $data['pengembalian']        = $this->Mod_laporan->detailPengembalian($id_transaksi)->row_array(); 
        // $data['detailjpengembalian'] = $this->Mod_laporan->detailPengembalian($id_transaksi)->result();
        // $this->template->load('layoutbackend', 'laporan/pengembalian_detail', $data);
        // echo "<pre>";
        // print_r($data['detailjpengembalian']);
        // echo "</pre>";
    }

   

}

/* End of file Laporan.php */
