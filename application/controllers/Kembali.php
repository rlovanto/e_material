<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Kembali extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_kembali');
    }


    public function index()
    {
        $data['kembali']      = $this->Mod_kembali->getAll();
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'kembali/kembali_data', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'kembali/kembali_data', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'kembali/kembali_data', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend', 'kembali/kembali_data', $data);
        }
        
    }

    public function create()
    {
        $data['kd_trans'] = $this->Mod_kembali->buat_kode(); // variable $kodeunik merujuk ke file model_user.php pada function buat_kode. paham kan ya? harus paham dong

        $this->template->load('layoutbackend', 'kembali/kembali_create',$data);
    }

    public function insert()
    {
        if(isset($_POST['save'])) {
        
            //function validasi
            $this->_set_rules();

            //apabila kembali mengisi form
            if($this->form_validation->run()==true){
                $kd_trans = $this->input->post('kd_trans');
                $cek = $this->Mod_kembali->cekKembali($kd_trans);
                //cek nis yg sudah digunakan
                if($cek->num_rows() > 0){
                    $data['message'] = "<div class='alert alert-block alert-danger'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <p><strong><i class='icon-ok'></i>Kode kembali</strong> Sudah Digunakan...!</p></div>"; 
                    $this->template->load('layoutbackend', 'kembali/kembali_create', $data); 
                }
                //kalo blm digunakan lakukan insert data kedatabase
                else{
                    
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'tgl_kembali'  => $this->input->post('tgl_kembali'),
                        'jumlah_k'  => $this->input->post('jumlah_k'),
                        'reference'  => $this->input->post('reference'),
                        'user_create'  => $this->input->post('user_create'),
                        'create_date'  => $this->input->post('create_date') 
                                       );
                    $this->Mod_kembali->insertKembali("kembali", $save);
                    // echo "berhasil"; die();
                    redirect('kembali/index/create-success');
                }
            }
            //jika kembali mengkosongkan form input
            else{
                $this->template->load('layoutbackend', 'kembali/kembali_create');
            } 

        } //end $_POST['save']
    }

    public function edit()
    {
        $kd_trans = $this->uri->segment(3);
        
        $data['edit']    = $this->Mod_kembali->cekKembali($kd_trans)->row_array();
        // $data['anggota'] =  $this->Mod_anggota->getAll()->result_array();
        // print_r($data['edit']); die();
        $this->template->load('layoutbackend', 'kembali/kembali_edit', $data);
    }

    public function update()
    {
        if(isset($_POST['update'])) {
            // echo "proses update"; die();
            if(isset($_POST['update'])) {

                $this->_set_rules();
                //apabila user mengkosongkan form input
                if($this->form_validation->run()==true){
                    // echo "masuk"; die();
                    $kd_trans = $this->input->post('kd_trans');
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'tgl_kembali'  => $this->input->post('tgl_kembali'),
                        'jumlah_k'  => $this->input->post('jumlah_k'),
                        'reference'  => $this->input->post('reference'),
                        'user_update'  => $this->input->post('user_update'),
                        'update_date'  => $this->input->post('update_date')


                    );
                    $this->Mod_kembali->updateKembali($kd_trans, $save);
                    // echo "berhasil"; die();
                    redirect('kembali/index/update-success');      
                }
                //jika tidak mengkosongkan
                else{
                    $kd_trans = $this->input->post('kd_trans');
                    $data['edit']    = $this->Mod_kembali->cekKembali($kd_trans)->row_array();
                    $data['message'] = "";
                    $this->template->load('layoutbackend', 'kembali/kembali_edit', $data);
                }
            } //end empty $_FILES
        } // end $_POST['update']
    }

    public function delete()
    {
        $kd_trans = $this->input->post('kd_trans');
        $this->Mod_kembali->deleteKembali($kd_trans, 'kembali');
        // echo "berhasil"; die();
        redirect('kembali/index/delete-success');
    }

    //function global buat validasi input
    public function _set_rules()
    {
        $this->form_validation->set_rules('nama_brg','Kode Flibook','required|max_length[100]');
        $this->form_validation->set_rules('kd_trans','Keterangan','required|max_length[100]');
        $this->form_validation->set_rules('tgl_kembali','Sumber Kembali','required|max_length[50]');
        $this->form_validation->set_rules('jumlah_k','Link','required|max_length[200]'); 
        $this->form_validation->set_message('required', '{field} kosong, silahkan diisi');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>","</div>");
    }

}

/* End of file Buku.php */
