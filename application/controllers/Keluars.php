<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Keluars extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_keluars','Mod_barang'));
    }

    public function index()
    {
        $data['tglklr']  = date('Y-m-d');
        $data['autonumber'] = $this->Mod_keluars->AutoNumber();
        $this->template->load('layoutbackend', 'keluars/keluars_data', $data);
    }

    public function All()
    {
        $data['keluars']      = $this->Mod_keluars->getAllByUsers($this->session->userdata['id_petugas']);
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'keluars/keluars_tabel', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'keluars/keluars_tabel', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'keluars/keluars_tabel', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend','keluars/keluars_tabel', $data);
        }
        
    }
    public function Admin()
    {
        $data['keluars']      = $this->Mod_keluars->getAll();
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'keluars/keluars_tabel_a', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'keluars/keluars_tabel_a', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'keluars/keluars_tabel_a', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend','keluars/keluars_tabel_a', $data);
        }
        
    }

    public function tampil_tmp()
    {
        $data['tmp_klr']       = $this->Mod_keluars->getTmp()->result();
        $data['jumlahTmp'] = $this->Mod_keluars->jumlahTmp();
        $this->load->view('keluars/keluars_tampil_tmp',$data);
    }

	public function grafik()
	{
		$data['graph'] = $this->Mod_keluars->graph();
        $this->template->load('layoutbackend', 'keluars/keluars_chart', $data);

		// $this->load->view('keluars/keluars_chart', $data);
	}
    
    public function cari_buku()
    {
        $caribarang = $this->input->post('caribarang');
        $data['barang'] = $this->Mod_barang->BarangSearchKeluar($caribarang);
        $this->load->view('keluars/keluars_searchbook', $data);
    }

    public function cari_kode_buku()
    {
        //$id_brg = 7611;
        $id_brg = $this->input->post('id_brg');
        $hasil = $this->Mod_barang->cekBarang($id_brg);
        //jika ada buku dalam database
        if($hasil->num_rows() > 0) {
            $dbarang = $hasil->row_array();
            echo $dbarang['nama_brg']."|".$dbarang['satuan'];
        }
    }

    public function save_tmp()
    {
            $id_brg = $this->input->post('id_brg');
            // echo $id_brg; die();
            $cek = $this->Mod_keluars->cekTmp($id_brg);
            //cek apakah data masih kosong di tabel tmp_klr
            if($cek->num_rows() < 1) {
                $data = array(
                    'id_brg' => $this->input->post('id_brg'),
                    'nama_brg'     => $this->input->post('nama_brg'),
                    'satuan' => $this->input->post('satuan'),
                    'jumlah_x' => $this->input->post('jumlah_x'),
                    'stock' => $this->input->post('stock'),
                );
                $this->Mod_keluars->InsertTmp($data);
            }
    }

    public function hapus_tmp()
    {
        $id_brg = $this->input->post('id_brg');
        $this->Mod_keluars->deleteTmp($id_brg);
    }
    public function delete()
    {
        $id_transaksi = $this->input->post('id_transaksi');
        $this->Mod_keluars->deleteKeluar($id_transaksi, 'keluars');
        redirect('keluars/admin/delete-success');
    }
    public function simpan_transaksi()
    {
        $id_petugas = $this->session->userdata['id_petugas'];
        $user = $this->session->userdata['full_name'];
        //ambil data tmp_klr lakukan looping . setelah looping lakukan insert ke table transaksi
        $table_tmp = $this->Mod_keluars->getTmp()->result();
        foreach($table_tmp as $data){

            $id_brg = $data->id_brg; 
            $jumlah_x = $data->jumlah_x; 
            $stock = $data->stock; 
        
            $data = array(
                'id_transaksi'     => $this->input->post('id_transaksi'),
                'id_brg'        => $data->id_brg,
                'tanggal_keluar'   => $this->input->post('tgl_keluar'),
                'proyek'   => $this->input->post('proyek'),
                'status'           => "N",
                'id_petugas'       => $id_petugas,
                'jumlah_x'   =>$data->jumlah_x,
                'stock'   =>$data->stock,
                'user_create'  => $user,
                'create_date'  => date('Y-m-d')
            );           
            //insert data ke table transaksi
            $this->Mod_keluars->InsertTransaksi($data); 
            echo $data;

            //hapus table tmp_klr
            $this->Mod_keluars->deleteTmp($id_brg);
           
        }
    }
}

/* End of file keluars.php */
