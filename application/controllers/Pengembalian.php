<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembalian extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_pengembalian');
    }


    public function index()
    {
        $data['title'] = "Pengembalian Barang";
        $this->template->load('layoutbackend', 'pengembalian/pengembalian_data', $data);
    }

    public function cari_brg()
    {
        $id_brg = $this->input->post('id_brg');
        // $id_brg = 121210;
        $data['pencarian'] = $this->Mod_pengembalian->SearchBrg($id_brg);
        // print_r($data['pencarian']);
        $this->load->view('pengembalian/pengembalian_pencarian', $data);
        
         
    }     

    public function cari_transaksi()
    {
        $no_transaksi = $this->input->get_post('no_transaksi');
        // $no_transaksi = 20180411002;
        $hasil = $this->Mod_pengembalian->SearchTransaksi($no_transaksi);
        if($hasil->num_rows() > 0) {
            $dtrans = $hasil->row_array();
            echo $dtrans['id_brg']."|".$dtrans['tanggal_pinjam']."|".$dtrans['jumlah_x']."|".$dtrans['proyek'];
        }
    }

    public function tampil_brg()
    {
        
        $no_transaksi = $this->input->get('no_transaksi');
        $data['barang'] = $this->Mod_pengembalian->showBrg($no_transaksi)->result();
        $this->load->view('pengembalian/pengembalian_tampil_buku', $data);
        
    }

    public function simpan_transaksi()
    {
        $id_petugas = $this->session->userdata['id_petugas'];

        $simpan = array(
            'id_pengembalian'     => $this->input->post('id_pengembalian'),
            'tgl_pengembalian' => date('Y-m-d'),
            'id_transaksi'     => $this->input->post('no_transaksi'),
            'status'            => "Dikembalikan",
            'catatan'          => $this->input->post('catatan'),
            'jumlah_k'          => $this->input->post('jumlah_k'),
            'id_petugas'       => $id_petugas
        );
        $this->Mod_pengembalian->insertPengembalian($simpan);

        //update status peminjaman dari N menjadi Y
        $no_transaksi = $this->input->post('no_transaksi');
        $data = array(
            'status' => "Dikembalikan"
            // 'jumlah_x'=> $this->input->post('jumlah_k'),
        );

        $this->Mod_pengembalian->UpdateStatus($no_transaksi, $data);
    }

}

/* End of file Pengembalian.php */








