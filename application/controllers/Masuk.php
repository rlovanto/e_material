<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Masuk extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_masuk');
    }


    public function index()
    {
        $data['masuk']      = $this->Mod_masuk->getAll();
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'masuk/masuk_data', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'masuk/masuk_data', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'masuk/masuk_data', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend', 'masuk/masuk_data', $data);
        }
        
    }

    public function all()
    {
        $data['masuk']      = $this->Mod_masuk->getAllByUsers($this->session->userdata['id_petugas']);
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'masuk/masuk_data', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'masuk/masuk_data', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'masuk/masuk_data', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend', 'masuk/masuk_data', $data);
        }
        
    }

    public function create()
    {
        $data['kd_trans'] = $this->Mod_masuk->buat_kode(); // variable $kodeunik merujuk ke file model_user.php pada function buat_kode. paham kan ya? harus paham dong

        $this->template->load('layoutbackend', 'masuk/masuk_create',$data);
    }

    public function insert()
    {
        if(isset($_POST['save'])) {
        
            //function validasi
            $this->_set_rules();

            //apabila masuk mengisi form
            if($this->form_validation->run()==true){
                $kd_trans = $this->input->post('kd_trans');
                $cek = $this->Mod_masuk->cekMasuk($kd_trans);
                //cek nis yg sudah digunakan
                if($cek->num_rows() > 0){
                    $data['message'] = "<div class='alert alert-block alert-danger'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <p><strong><i class='icon-ok'></i>Kode masuk</strong> Sudah Digunakan...!</p></div>"; 
                    $this->template->load('layoutbackend', 'masuk/masuk_create', $data); 
                }
                //kalo blm digunakan lakukan insert data kedatabase
                else{
                    $id_petugas = $this->session->userdata['id_petugas'];
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'merk'  => $this->input->post('merk'),
                        'tgl_masuk'  => $this->input->post('tgl_masuk'),
                        'jumlah'  => $this->input->post('jumlah'),
                        'supplier'  => $this->input->post('supplier'),
                        'harga'  => $this->input->post('harga'),
                        'id_petugas' => $id_petugas,
                        'link'  => $this->input->post('link'),
                        'user_create'  => $this->input->post('user_create'),
                        'create_date'  => $this->input->post('create_date') 


                    );
                    $this->Mod_masuk->insertMasuk("masuk", $save);
                    // echo "berhasil"; die();
                    redirect('masuk/index/create-success');
                }
            }
            //jika masuk mengkosongkan form input
            else{
                $this->template->load('layoutbackend', 'masuk/masuk_create');
            } 

        } //end $_POST['save']
    }

    public function edit()
    {
        $kd_trans = $this->uri->segment(3);
        
        $data['edit']    = $this->Mod_masuk->cekMasuk($kd_trans)->row_array();
        // $data['anggota'] =  $this->Mod_anggota->getAll()->result_array();
        // print_r($data['edit']); die();
        $this->template->load('layoutbackend', 'masuk/masuk_edit', $data);
    }

    public function update()
    {
        if(isset($_POST['update'])) {
            // echo "proses update"; die();
            if(isset($_POST['update'])) {

                $this->_set_rules();
                //apabila user mengkosongkan form input
                if($this->form_validation->run()==true){
                    // echo "masuk"; die();
                    $kd_trans = $this->input->post('kd_trans');
                    $id_petugas = $this->session->userdata['id_petugas'];
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'merk'  => $this->input->post('merk'),
                        'tgl_masuk'  => $this->input->post('tgl_masuk'),
                        'jumlah'  => $this->input->post('jumlah'),
                        'supplier'  => $this->input->post('supplier'),
                        'harga'  => $this->input->post('harga'),
                        'id_petugas' => $id_petugas,
                        'link'  => $this->input->post('link'),
                        'user_update'  => $this->input->post('user_update'),
                        'update_date'  => $this->input->post('update_date')

                    );
                    $this->Mod_masuk->updateMasuk($kd_trans, $save);
                    // echo "berhasil"; die();
                    redirect('masuk/index/update-success');      
                }
                //jika tidak mengkosongkan
                else{
                    $kd_trans = $this->input->post('kd_trans');
                    $data['edit']    = $this->Mod_masuk->cekMasuk($kd_trans)->row_array();
                    $data['message'] = "";
                    $this->template->load('layoutbackend', 'masuk/masuk_edit', $data);
                }
            } //end empty $_FILES
        } // end $_POST['update']
    }

    public function delete()
    {
        $kd_trans = $this->input->post('kd_trans');
        $this->Mod_masuk->deleteMasuk($kd_trans, 'masuk');
        // echo "berhasil"; die();
        redirect('masuk/index/delete-success');
    }

    //function global buat validasi input
    public function _set_rules()
    {
        $this->form_validation->set_rules('nama_brg','Kode Flibook','required|max_length[100]');
        $this->form_validation->set_rules('kd_trans','Keterangan','required|max_length[100]');
        $this->form_validation->set_rules('tgl_masuk','Sumber Masuk','required|max_length[50]');
        $this->form_validation->set_rules('jumlah','Link','required|max_length[200]'); 
        $this->form_validation->set_rules('supplier','Link','required|max_length[200]'); 
        $this->form_validation->set_rules('harga','Link','required|max_length[200]'); 
        $this->form_validation->set_message('required', '{field} kosong, silahkan diisi');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>","</div>");
    }

}

/* End of file Buku.php */
