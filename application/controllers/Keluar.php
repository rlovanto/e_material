<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Keluar extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_keluar');
    }

//ini untuk Admin

    public function index()
    {
        $data['keluar']      = $this->Mod_keluar->getAll();
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'keluar/keluar_data', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'keluar/keluar_data', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'keluar/keluar_data', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend', 'keluar/keluar_data', $data);
        }
        
    }
//ini untuk user
    public function all()
    {
        $data['keluar']      = $this->Mod_keluar->getAllByUsers($this->session->userdata['id_petugas']);
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'keluar/keluar_data_user', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'keluar/keluar_data_user', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'keluar/keluar_data_user', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend', 'keluar/keluar_data_user', $data);
        }
        
    }
//ini untuk Admin
    public function create()
    {
        $data['kd_trans'] = $this->Mod_keluar->buat_kode(); // variable $kodeunik merujuk ke file model_user.php pada function buat_kode. paham kan ya? harus paham dong

        $this->template->load('layoutbackend', 'keluar/keluar_create',$data);
    }
//ini untuk user
    public function create_user()
    {
        $data['kd_trans'] = $this->Mod_keluar->buat_kode(); // variable $kodeunik merujuk ke file model_user.php pada function buat_kode. paham kan ya? harus paham dong

        $this->template->load('layoutbackend', 'keluar/keluar_create_user',$data);
    }
//ini untuk Admin
    public function insert()
    {
        if(isset($_POST['save'])) {
        
            //function validasi
            $this->_set_rules();

            //apabila keluar mengisi form
            if($this->form_validation->run()==true){
                $kd_trans = $this->input->post('kd_trans');
                $cek = $this->Mod_keluar->cekKeluar($kd_trans);
                //cek nis yg sudah digunakan
                if($cek->num_rows() > 0){
                    $data['message'] = "<div class='alert alert-block alert-danger'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <p><strong><i class='icon-ok'></i>Kode keluar</strong> Sudah Digunakan...!</p></div>"; 
                    $this->template->load('layoutbackend', 'keluar/keluar_create', $data); 
                }
                //kalo blm digunakan lakukan insert data kedatabase
                else{
                    $id_petugas = $this->session->userdata['id_petugas'];
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'tgl_keluar'  => $this->input->post('tgl_keluar'),
                        'jumlah'  => $this->input->post('jumlah'),
                        'proyek'  => $this->input->post('proyek'),
                        'id_petugas' => $id_petugas,
                        'user_create'  => $this->input->post('user_create'),
                        'create_date'  => $this->input->post('create_date')
                    );
                    $this->Mod_keluar->insertKeluar("keluar", $save);
                    redirect('keluar/index/create-success');
                }
            }
            //jika keluar mengkosongkan form input
            else{
                $this->template->load('layoutbackend', 'keluar/keluar_create');
            } 

        } //end $_POST['save']
    }
//ini untuk user
    public function insert_user()
    {
        if(isset($_POST['save'])) {
        
            //function validasi
            $this->_set_rules();

            //apabila keluar mengisi form
            if($this->form_validation->run()==true){
                $kd_trans = $this->input->post('kd_trans');
                $cek = $this->Mod_keluar->cekKeluar($kd_trans);
                //cek nis yg sudah digunakan
                if($cek->num_rows() > 0){
                    $data['message'] = "<div class='alert alert-block alert-danger'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <p><strong><i class='icon-ok'></i>Kode keluar</strong> Sudah Digunakan...!</p></div>"; 
                    $this->template->load('layoutbackend', 'keluar/keluar_create_user', $data); 
                }
                //kalo blm digunakan lakukan insert data kedatabase
                else{
                    $id_petugas = $this->session->userdata['id_petugas'];
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'tgl_keluar'  => $this->input->post('tgl_keluar'),
                        'jumlah'  => $this->input->post('jumlah'),
                        'proyek'  => $this->input->post('proyek'),
                        'id_petugas' => $id_petugas,
                        'user_create'  => $this->input->post('user_create'),
                        'create_date'  => $this->input->post('create_date')
                    );
                    $this->Mod_keluar->insertKeluar("keluar", $save);
                    redirect('keluar/all/create-success');
                }
            }
            //jika keluar mengkosongkan form input
            else{
                $this->template->load('layoutbackend', 'keluar/keluar_create_user');
            } 

        } //end $_POST['save']
    }
//ini untuk Admin
    public function edit()
    {
        $kd_trans = $this->uri->segment(3);
        
        $data['edit']    = $this->Mod_keluar->cekKeluar($kd_trans)->row_array();
        // $data['anggota'] =  $this->Mod_anggota->getAll()->result_array();
        // print_r($data['edit']); die();
        $this->template->load('layoutbackend', 'keluar/keluar_edit', $data);
    }
//ini untuk user
    public function edit_user()
    {
        $kd_trans = $this->uri->segment(3);
        
        $data['edit']    = $this->Mod_keluar->cekKeluar($kd_trans)->row_array();
        // $data['anggota'] =  $this->Mod_anggota->getAll()->result_array();
        // print_r($data['edit']); die();
        $this->template->load('layoutbackend', 'keluar/keluar_edit_user', $data);
    }
//ini untuk Admin
    public function update()
    {
        if(isset($_POST['update'])) {
            // echo "proses update"; die();
            if(isset($_POST['update'])) {

                $this->_set_rules();
                //apabila user mengkosongkan form input
                if($this->form_validation->run()==true){
                    $kd_trans = $this->input->post('kd_trans');
                    $id_petugas = $this->session->userdata['id_petugas'];
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'tgl_keluar'  => $this->input->post('tgl_keluar'),
                        'jumlah'  => $this->input->post('jumlah'),
                        'proyek'  => $this->input->post('proyek'),
                        'id_petugas' => $id_petugas,
                        'user_update'  => $this->input->post('user_update'),
                        'update_date'  => $this->input->post('update_date')

                    );
                    $this->Mod_keluar->updateKeluar($kd_trans, $save);
                    redirect('keluar/index/update-success');      
                }
                //jika tidak mengkosongkan
                else{
                    $kd_trans = $this->input->post('kd_trans');
                    $data['edit']    = $this->Mod_keluar->cekKeluar($kd_trans)->row_array();
                    $data['message'] = "";
                    $this->template->load('layoutbackend', 'keluar/keluar_edit', $data);
                }
            } 
        } 
    }
//ini untuk user
    public function update_user()
    {
        if(isset($_POST['update'])) {
            // echo "proses update"; die();
            if(isset($_POST['update'])) {

                $this->_set_rules();
                //apabila user mengkosongkan form input
                if($this->form_validation->run()==true){
                    $kd_trans = $this->input->post('kd_trans');
                    $id_petugas = $this->session->userdata['id_petugas'];
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'tgl_keluar'  => $this->input->post('tgl_keluar'),
                        'jumlah'  => $this->input->post('jumlah'),
                        'proyek'  => $this->input->post('proyek'),
                        'id_petugas' => $id_petugas,
                        'user_update'  => $this->input->post('user_update'),
                        'update_date'  => $this->input->post('update_date')

                    );
                    $this->Mod_keluar->updateKeluar($kd_trans, $save);
                    redirect('keluar/all/update-success');      
                }
                //jika tidak mengkosongkan
                else{
                    $kd_trans = $this->input->post('kd_trans');
                    $data['edit']    = $this->Mod_keluar->cekKeluar($kd_trans)->row_array();
                    $data['message'] = "";
                    $this->template->load('layoutbackend', 'keluar/keluar_edit_user', $data);
                }
            } 
        } 
    }

//ini untuk Admin
    public function delete()
    {
        $kd_trans = $this->input->post('kd_trans');
        $this->Mod_keluar->deleteKeluar($kd_trans, 'keluar');
        redirect('keluar/index/delete-success');
    }

//ini untuk user
    public function delete_user()
    {
        $kd_trans = $this->input->post('kd_trans');
        $this->Mod_keluar->deleteKeluar($kd_trans, 'keluar');
        redirect('keluar/all/delete-success');
    }

    //function global buat validasi input
    public function _set_rules()
    {
        $this->form_validation->set_rules('nama_brg','Kode Flibook','required|max_length[100]');
        $this->form_validation->set_rules('kd_trans','Keterangan','required|max_length[100]');
        $this->form_validation->set_rules('tgl_keluar','Sumber Keluar','required|max_length[50]');
        $this->form_validation->set_rules('jumlah','Link','required|max_length[200]'); 
        $this->form_validation->set_message('required', '{field} kosong, silahkan diisi');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>","</div>");
    }

}

/* End of file Buku.php */
