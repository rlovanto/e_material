<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_keluars','Mod_masuks','Mod_card','Mod_barang'));
    }

    function index()
    {
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Ganti Password</p></div>";
        }

        if($this->uri->segment(3)=="create-fail") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Gagal Ganti Password (Password tidak sesuai) </p></div>";
        }
        
        $data['countbarang'] = $this->Mod_barang->totalRows('barang');
        $data['countcard'] = $this->Mod_card->totalRows('card');
        $data['countmasuk'] = $this->Mod_masuks->totalRows('masuks');
        $data['countkeluar'] = $this->Mod_keluars->totalRows('keluars');


        $data['barang'] = $this->Mod_barang->getAll('barang');
        $data['card'] = $this->Mod_card->getAll('card');
        $data['masuks'] = $this->Mod_masuks->getAll('masuks');
        $data['keluars'] = $this->Mod_keluars->getAll('keluars');
        $this->template->load('layoutbackend', 'dashboard/dashboard_data', $data);
    }

}
/* End of file Controllername.php */
 