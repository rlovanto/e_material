<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends MY_Controller {
    private $filename = "import_data"; // Kita tentukan nama filenya

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_kategori');
    }

    public function form(){
		$data = array(); // Buat variabel $data sebagai array
		
		if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di Mod_kategori.php
			$upload = $this->Mod_kategori->upload_file($this->filename);
			
			if($upload['result'] == "success"){ // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH.'third_party/PHPExcel/PHPExcel.php';
				
				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
				
				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet; 
			}else{ // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}
        $this->template->load('layoutbackend', 'import/form_listrik', $data);

		// $this->load->view('form', $data);
	}
	
	public function import(){
		// Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		
		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();
		
		$numrow = 1;
		foreach($sheet as $row){
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'kd_kategori'=>$row['A'], // Insert data nis dari kolom A di excel
					'kategori'=>$row['B'], // Insert data nama dari kolom B di excel
                    'create_user' => $this->session->userdata('full_name'),
                    'create_at' => date('Y-m-d')
				));
			}
			
			$numrow++; // Tambah 1 setiap kali looping
		}

		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->Mod_kategori->insert_multiple($data);
		
		redirect("kategori"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

    public function all()
    {
        $title = $this->uri->segment(3);
        $data['kategori']    = $this->Mod_kategori->getAll('kategori');
        $this->load->view('kategori/detail_listrik', $data);
    }

    public function index()
    {
        $data['kategori']      = $this->Mod_kategori->getAll();
        
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'kategori/kategori_data', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'kategori/kategori_data', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'kategori/kategori_data', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend', 'kategori/kategori_data', $data);
        }
        
    }

    public function create()
    {
        $data['kd_kategori'] = $this->Mod_kategori->buat_kode();

        $this->template->load('layoutbackend', 'kategori/kategori_create',$data);
    }

    public function insert()
    {
        if(isset($_POST['save'])) {
        
            //function validasi
            $this->_set_rules();

            //apabila kategori mengisi form
            if($this->form_validation->run()==true){
                $kd_kategori = $this->input->post('kd_kategori');
                $cek = $this->Mod_kategori->cekKategori($kd_kategori);
                //cek nis yg sudah digunakan
                if($cek->num_rows() > 0){
                    $data['message'] = "<div class='alert alert-block alert-danger'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <p><strong><i class='icon-ok'></i>Kode kategori</strong> Sudah Digunakan...!</p></div>"; 
                    $this->template->load('layoutbackend', 'kategori/kategori_create', $data); 
                }
                //kalo blm digunakan lakukan insert data kedatabase
                else{
                    
                    $save  = array(
                        'kd_kategori'   => $this->input->post('kd_kategori'),
                        'kategori'  => $this->input->post('kategori'),
                        'create_user' => $this->session->userdata('full_name'),
                        'create_at' => date('Y-m-d')
                    );
                    $this->Mod_kategori->insertKategori("kategori", $save);
                    // echo "berhasil"; die();
                    redirect('kategori/index/create-success');
                }
            }
            //jika kategori mengkosongkan form input
            else{
                $this->template->load('layoutbackend', 'kategori/kategori_create');
            } 

        } //end $_POST['save']
    }

    public function edit()
    {
        $kd_kategori = $this->uri->segment(3);
        
        $data['edit']    = $this->Mod_kategori->cekKategori($kd_kategori)->row_array();
        $this->template->load('layoutbackend', 'kategori/kategori_edit', $data);
    }

    public function update()
    {
        if(isset($_POST['update'])) {
            // echo "proses update"; die();
            if(isset($_POST['update'])) {

                $this->_set_rules();
                //apabila user mengkosongkan form input
                if($this->form_validation->run()==true){
                    // echo "masuk"; die();
                    $kd_kategori = $this->input->post('kd_kategori');
                    $save  = array(
                        'kd_kategori'   => $this->input->post('kd_kategori'),
                        'kategori'  => $this->input->post('kategori'),
                        'update_user' => $this->session->userdata('full_name'),
                        'update_at' => date('Y-m-d')
                    );
                    $this->Mod_kategori->updateKategori($kd_kategori, $save);
                    // echo "berhasil"; die();
                    redirect('kategori/index/update-success');      
                }
                //jika ttypeak mengkosongkan
                else{
                    $kd_kategori = $this->input->post('kd_kategori');
                    $data['edit']    = $this->Mod_kategori->cekKategori($kd_kategori)->row_array();
                    $data['message'] = "";
                    $this->template->load('layoutbackend', 'kategori/kategori_edit', $data);
                }
            } //end empty $_FILES
        } // end $_POST['update']
    }

    public function delete()
    {
        $kd_kategori = $this->input->post('kd_kategori');
        $this->Mod_kategori->deleteKategori($kd_kategori, 'kategori');
        // echo "berhasil"; die();
        redirect('kategori/index/delete-success');
    }

    //function global buat validasi input
    public function _set_rules()
    {
        $this->form_validation->set_rules('kd_kategori','Kode Kategori','required|max_length[150]');
        $this->form_validation->set_rules('kategori','Kategori','required|max_length[255]');
        $this->form_validation->set_message('required', '{field} kosong, silahkan diisi');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>","</div>");
    }

}

/* End of file Buku.php */
