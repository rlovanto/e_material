<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Masuks extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_masuks','Mod_barang'));
    }
	public function grafik()
	{
		$data['graph'] = $this->Mod_masuks->graph();
        $this->template->load('layoutbackend', 'masuks/masuks_chart', $data);

		// $this->load->view('masuks/masuks_chart', $data);
	}
    public function index()
    {
        // $data['tglmsk']  = date('Y-m-d');
        $data['autonumber'] = $this->Mod_masuks->AutoNumbering();
        $this->template->load('layoutbackend', 'masuks/masuks_data', $data);
    }

    public function All()
    {
        $data['masuks']      = $this->Mod_masuks->getAll();
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'masuks/masuks_tabel', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'masuks/masuks_tabel', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'masuks/masuks_tabel', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend', 'masuks/masuks_tabel', $data);
        }
        
    }

    public function tampil_tmp()
    {
        $data['tmp_msk']       = $this->Mod_masuks->getTmp()->result();
        $data['jumlahTmp'] = $this->Mod_masuks->jumlahTmp();
        $this->load->view('masuks/masuks_tampil_tmp',$data);
    }

    public function cari_buku()
    {
        $caribarang = $this->input->post('caribarang');
        $data['barang'] = $this->Mod_barang->BarangSearch($caribarang);
        $this->load->view('masuks/masuks_searchbook', $data);
    }

    public function cari_kode_buku()
    {
        //$id_brg = 7611;
        $id_brg = $this->input->post('id_brg');
        $hasil = $this->Mod_barang->cekBarang($id_brg);
        //jika ada buku dalam database
        if($hasil->num_rows() > 0) {
            $dbarang = $hasil->row_array();
            echo $dbarang['nama_brg']."|".$dbarang['satuan'];
        }
    }

    public function save_tmp()
    {
            $id_brg = $this->input->post('id_brg');
            // echo $id_brg; die();
            $cek = $this->Mod_masuks->cekTmp($id_brg);
            //cek apakah data masih kosong di tabel tmp_msk
            if($cek->num_rows() < 1) {
                $data = array(
                    'id_brg' => $this->input->post('id_brg'),
                    'nama_brg'     => $this->input->post('nama_brg'),
                    'satuan' => $this->input->post('satuan'),
                    'jumlah_x' => $this->input->post('jumlah_x'),
                    'merk' => $this->input->post('merk'),
                    'harga' => $this->input->post('harga'),
                    'link' => $this->input->post('link'),
                );
                $this->Mod_masuks->InsertTmp($data);
            }
    }

    public function hapus_tmp()
    {
        $id_brg = $this->input->post('id_brg');
        $this->Mod_masuks->deleteTmp($id_brg);
    }
    public function delete()
    {
        $id_transaksi = $this->input->post('id_transaksi');
        $this->Mod_masuks->deletePinjam($id_transaksi, 'masuks');
        // echo "berhasil"; die();
        redirect('masuks/all/delete-success');
    }

    public function simpan_transaksi()
    {
        $id_petugas = $this->session->userdata['id_petugas'];
        $user = $this->session->userdata['full_name'];
        //ambil data tmp_msk lakukan looping . setelah looping lakukan insert ke table transaksi
        $table_tmp = $this->Mod_masuks->getTmp()->result();
        foreach($table_tmp as $data){

            $id_brg = $data->id_brg; 
            $jumlah_x = $data->jumlah_x; 
            $merk = $data->merk; 
            $harga = $data->harga; 
            $link = $data->link; 
            
            $data = array(
                'id_transaksi'     => $this->input->post('id_transaksi'),
                'id_brg'        => $data->id_brg,
                'tanggal_masuk'   => $this->input->post('tanggal_masuk'),
                // 'merk'   => $this->input->post('merk'),
                'status'           => "N",
                'id_petugas'       => $id_petugas,
                'jumlah_x'   =>$data->jumlah_x,
                'merk'   =>$data->merk,
                'harga'   =>$data->harga,
                'link'   =>$data->link,
                'user_create'  => $user,
                'create_date'  => date('Y-m-d')
            );           
            //insert data ke table transaksi
            $this->Mod_masuks->InsertTransaksi($data); 
            echo $data;

            //hapus table tmp_msk
            $this->Mod_masuks->deleteTmp($id_brg);
           
        }
    }
    public function edit()
    {
        $id_transaksi = $this->uri->segment(3);
        
        $data['edit']    = $this->Mod_masuks->cekTransaksi($id_transaksi)->row_array();
        // $data['anggota'] =  $this->Mod_anggota->getAll()->result_array();
        // print_r($data['edit']); die();
        $this->template->load('layoutbackend', 'masuks/masuks_edit', $data);
    }

    public function update()
    {
        if(isset($_POST['update'])) {
            // echo "proses update"; die();
            if(isset($_POST['update'])) {

                // $this->_set_rules();
                //apabila user mengkosongkan form input
                if($this->form_validation->run()==true){
                    // echo "masuk"; die();
                    $id_transaksi = $this->input->post('id_transaksi');
                    $save  = array(
                        'id_transaksi'   => $this->input->post('id_transaksi'),
                        'tanggal_masuk'  => $this->input->post('tanggal_masuk'),
                        'jumlah_x'  => $this->input->post('jumlah_x'),
                        'merk'  => $this->input->post('merk'),
                        'harga'  => $this->input->post('harga'),
                        'link'  => $this->input->post('link'),
                        'user_update'  => $this->input->post('user_update'),
                        'update_date'  => $this->input->post('update_date')
                    );
                    $this->Mod_masuks->updateTransaksi($id_transaksi, $save);
                    // echo "berhasil"; die();
                    redirect('masuks/all/update-success');      
                }
                //jika tidak mengkosongkan
                else{
                    $id_transaksi = $this->input->post('id_transaksi');
                    $data['edit']    = $this->Mod_masuks->cekTransaksi($id_transaksi)->row_array();
                    $data['message'] = "";
                    $this->template->load('layoutbackend', 'masuks/masuks_edit', $data);
                }
            } //end empty $_FILES
        } // end $_POST['update']
    }
}

/* End of file masuks.php */
