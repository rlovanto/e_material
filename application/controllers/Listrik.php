<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Listrik extends MY_Controller {
    private $filename = "import_data"; // Kita tentukan nama filenya

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_listrik');
    }

    public function form(){
		$data = array(); // Buat variabel $data sebagai array
		
		if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di Mod_listrik.php
			$upload = $this->Mod_listrik->upload_file($this->filename);
			
			if($upload['result'] == "success"){ // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH.'third_party/PHPExcel/PHPExcel.php';
				
				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
				
				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet; 
			}else{ // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}
        $this->template->load('layoutbackend', 'import/form_listrik', $data);

		// $this->load->view('form', $data);
	}
	
	public function import(){
		// Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		
		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();
		
		$numrow = 1;
		foreach($sheet as $row){
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'jenis'=>$row['A'], // Insert data nis dari kolom A di excel
					'id_pelanggan'=>$row['B'], // Insert data nama dari kolom B di excel
					'tempat'=>$row['C'], // Insert data jenis kelamin dari kolom C di excel
                    'user_create' => $this->session->userdata('full_name'),
                    'create_date' => date('Y-m-d')
				));
			}
			
			$numrow++; // Tambah 1 setiap kali looping
		}

		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->Mod_listrik->insert_multiple($data);
		
		redirect("listrik"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

    public function all()
    {
        $title = $this->uri->segment(3);
        $data['listrik']    = $this->Mod_listrik->getAll('listrik');
        // $data['anggota'] =  $this->Mod_anggota->getAll()->result_array();
        // print_r($data['edit']); die();
        $this->load->view('listrik/detail_listrik', $data);
    }

    public function index()
    {
        $data['listrik']      = $this->Mod_listrik->getAll();
        
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'listrik/listrik_data', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'listrik/listrik_data', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'listrik/listrik_data', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend', 'listrik/listrik_data', $data);
        }
        
    }

    public function create()
    {
        $this->template->load('layoutbackend', 'listrik/listrik_create');
    }

    public function insert()
    {
        if(isset($_POST['save'])) {
        
            //function validasi
            $this->_set_rules();

            //apabila listrik mengisi form
            if($this->form_validation->run()==true){
                $id = $this->input->post('id');
                $cek = $this->Mod_listrik->cekListrik($id);
                //cek nis yg sudah digunakan
                if($cek->num_rows() > 0){
                    $data['message'] = "<div class='alert alert-block alert-danger'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <p><strong><i class='icon-ok'></i>Kode listrik</strong> Sudah Digunakan...!</p></div>"; 
                    $this->template->load('layoutbackend', 'listrik/listrik_create', $data); 
                }
                //kalo blm digunakan lakukan insert data kedatabase
                else{
                    
                    $save  = array(
                        'jenis'   => $this->input->post('jenis'),
                        'id_pelanggan'  => $this->input->post('id_pelanggan'),
                        'tempat'  => $this->input->post('tempat'),
                        'user_create'  => $this->input->post('user_create'),
                        'create_date'  => $this->input->post('create_date')
                    );
                    $this->Mod_listrik->insertListrik("listrik", $save);
                    // echo "berhasil"; die();
                    redirect('listrik/index/create-success');
                }
            }
            //jika listrik mengkosongkan form input
            else{
                $this->template->load('layoutbackend', 'listrik/listrik_create');
            } 

        } //end $_POST['save']
    }

    public function edit()
    {
        $id_pelanggan = $this->uri->segment(3);
        
        $data['edit']    = $this->Mod_listrik->cekListrik($id_pelanggan)->row_array();
        $this->template->load('layoutbackend', 'listrik/listrik_edit', $data);
    }

    public function update()
    {
        if(isset($_POST['update'])) {
            // echo "proses update"; die();
            if(isset($_POST['update'])) {

                $this->_set_rules();
                //apabila user mengkosongkan form input
                if($this->form_validation->run()==true){
                    // echo "masuk"; die();
                    $id_pelanggan = $this->input->post('id_pelanggan');
                    $save  = array(
                        'jenis'   => $this->input->post('jenis'),
                        'id_pelanggan'  => $this->input->post('id_pelanggan'),
                        'tempat'  => $this->input->post('tempat'),
                        'user_update'  => $this->input->post('user_update'),
                        'update_date'  => $this->input->post('update_date')
                    );
                    $this->Mod_listrik->updateListrik($id_pelanggan, $save);
                    // echo "berhasil"; die();
                    redirect('listrik/index/update-success');      
                }
                //jika ttypeak mengkosongkan
                else{
                    $id_pelanggan = $this->input->post('id_pelanggan');
                    $data['edit']    = $this->Mod_listrik->cekListrik($id_pelanggan)->row_array();
                    $data['message'] = "";
                    $this->template->load('layoutbackend', 'listrik/listrik_edit', $data);
                }
            } //end empty $_FILES
        } // end $_POST['update']
    }

    public function delete()
    {
        $id_pelanggan = $this->input->post('id_pelanggan');
        $this->Mod_listrik->deleteListrik($id_pelanggan, 'listrik');
        // echo "berhasil"; die();
        redirect('listrik/index/delete-success');
    }

    //function global buat validasi input
    public function _set_rules()
    {
        $this->form_validation->set_rules('jenis','jenis','required|max_length[150]');
        $this->form_validation->set_rules('id_pelanggan','id_pelanggan','required|max_length[100]');
        $this->form_validation->set_rules('tempat','tempat Listrik','required|max_length[50]');
        $this->form_validation->set_message('required', '{field} kosong, silahkan diisi');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>","</div>");
    }

}

/* End of file Buku.php */
