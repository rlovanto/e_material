<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik extends MY_Controller {

    // Contructor.
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_statistik','Mod_masuks', 'Mod_keluars','Mod_peminjaman', 'Mod_pengembalian'));
    }

    // Menampilan halaman statistik.
    function index()
    {
        $data['countmasuk'] = $this->Mod_masuks->totalRows('barang_masuk');
        $data['countkeluar'] = $this->Mod_keluars->totalRows('keluar_brg');
        $data['countpinjam'] = $this->Mod_peminjaman->totalRowsi('peminjaman');
        $data['countkembali'] = $this->Mod_pengembalian->totalRows('pengembalian');
        $this->template->load('layoutbackend', 'statistik/statistik_dashboard', $data);
    }

    // Menampilkan statistika pengunjung.
    public function graph_masuk()
	{
		$data['graph'] = $this->Mod_statistik->graphic_barangmasuk();
        $this->template->load('layoutbackend', 'statistik/masuk_chart', $data);
	}
	// Menampilkan statistika anggota.
    public function graph_keluar()
	{
		$data['graph'] = $this->Mod_statistik->graphic_barangkeluar();
        $this->template->load('layoutbackend', 'statistik/keluar_chart', $data);
	}

    // Menampilkan statistika majalah.
    public function graph_pinjam()
	{
		$data['graph'] = $this->Mod_statistik->graphic_peminjaman();
        $this->template->load('layoutbackend', 'statistik/pinjam_chart', $data);
	}
    
    // Menampilkan statistika buku.
    public function graph_kembali()
	{
		$data['graph'] = $this->Mod_statistik->graphic_pengemblian();
        $this->template->load('layoutbackend', 'statistik/kembali_chart', $data);
	}

    
}