<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Pinjam extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_pinjam');
    }

//ini untuk Admin
    public function index()
    {
        $data['pinjam']      = $this->Mod_pinjam->getAll();
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'pinjam/pinjam_data', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'pinjam/pinjam_data', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'pinjam/pinjam_data', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend', 'pinjam/pinjam_data', $data);
        }
        
    }
//ini untuk user
    public function all()
    {
        $data['pinjam']      = $this->Mod_pinjam->getAllByUsers($this->session->userdata['id_petugas']);
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'pinjam/pinjam_data_user', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'pinjam/pinjam_data_user', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'pinjam/pinjam_data_user', $data);
        }
        else{
            $data['message'] = "";
            $this->template->load('layoutbackend', 'pinjam/pinjam_data_user', $data);
        }
        
    }


//ini untuk Admin

    public function create()
    {
        $data['kd_trans'] = $this->Mod_pinjam->buat_kode(); // variable $kodeunik merujuk ke file model_user.php pada function buat_kode. paham kan ya? harus paham dong

        $this->template->load('layoutbackend', 'pinjam/pinjam_create',$data);
    }

//ini untuk user

    public function create_user()
    {
        $data['kd_trans'] = $this->Mod_pinjam->buat_kode(); // variable $kodeunik merujuk ke file model_user.php pada function buat_kode. paham kan ya? harus paham dong

        $this->template->load('layoutbackend', 'pinjam/pinjam_create_user',$data);
    }

//ini untuk Admin
    public function insert()
    {
        if(isset($_POST['save'])) {
        
            //function validasi
            $this->_set_rules();

            //apabila pinjam mengisi form
            if($this->form_validation->run()==true){
                $kd_trans = $this->input->post('kd_trans');
                $cek = $this->Mod_pinjam->cekPinjam($kd_trans);
                //cek nis yg sudah digunakan
                if($cek->num_rows() > 0){
                    $data['message'] = "<div class='alert alert-block alert-danger'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <p><strong><i class='icon-ok'></i>Kode pinjam</strong> Sudah Digunakan...!</p></div>"; 
                    $this->template->load('layoutbackend', 'pinjam/pinjam_create', $data); 
                }
                //kalo blm digunakan lakukan insert data kedatabase
                else{
                    $id_petugas = $this->session->userdata['id_petugas'];
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'tgl_pinjam'  => $this->input->post('tgl_pinjam'),
                        'jumlah'  => $this->input->post('jumlah'),
                        'proyek'  => $this->input->post('proyek'),
                        'id_petugas' => $id_petugas,
                        'user_create'  => $this->input->post('user_create'),
                        'create_date'  => $this->input->post('create_date')
                    );
                    $this->Mod_pinjam->insertPinjam("pinjam", $save);
                    // echo "berhasil"; die();
                    redirect('pinjam/index/create-success');
                }
            }
            //jika pinjam mengkosongkan form input
            else{
                $this->template->load('layoutbackend', 'pinjam/pinjam_create');
            } 

        } //end $_POST['save']
    }

//ini untuk User
    public function insert_user()
    {
        if(isset($_POST['save'])) {
        
            //function validasi
            $this->_set_rules();

            //apabila pinjam mengisi form
            if($this->form_validation->run()==true){
                $kd_trans = $this->input->post('kd_trans');
                $cek = $this->Mod_pinjam->cekPinjam($kd_trans);
                //cek nis yg sudah digunakan
                if($cek->num_rows() > 0){
                    $data['message'] = "<div class='alert alert-block alert-danger'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <p><strong><i class='icon-ok'></i>Kode pinjam</strong> Sudah Digunakan...!</p></div>"; 
                    $this->template->load('layoutbackend', 'pinjam/pinjam_create_user', $data); 
                }
                //kalo blm digunakan lakukan insert data kedatabase
                else{
                    $id_petugas = $this->session->userdata['id_petugas'];
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'tgl_pinjam'  => $this->input->post('tgl_pinjam'),
                        'jumlah'  => $this->input->post('jumlah'),
                        'proyek'  => $this->input->post('proyek'),
                        'id_petugas' => $id_petugas,
                        'user_create'  => $this->input->post('user_create'),
                        'create_date'  => $this->input->post('create_date')
                    );
                    $this->Mod_pinjam->insertPinjam("pinjam", $save);
                    // echo "berhasil"; die();
                    redirect('pinjam/all/create-success');
                }
            }
            //jika pinjam mengkosongkan form input
            else{
                $this->template->load('layoutbackend', 'pinjam/pinjam_create_user');
            } 

        } 
    }
//ini untuk Admin
    public function edit()
    {
        $kd_trans = $this->uri->segment(3);
        
        $data['edit']    = $this->Mod_pinjam->cekPinjam($kd_trans)->row_array();
        $this->template->load('layoutbackend', 'pinjam/pinjam_edit', $data);
    }
//ini untuk User
    public function edit_user()
    {
        $kd_trans = $this->uri->segment(3);
        
        $data['edit']    = $this->Mod_pinjam->cekPinjam($kd_trans)->row_array();
        $this->template->load('layoutbackend', 'pinjam/pinjam_edit_user', $data);
    }

//ini untuk Admin
    public function update()
    {
        if(isset($_POST['update'])) {
            // echo "proses update"; die();
            if(isset($_POST['update'])) {

                $this->_set_rules();
                //apabila user mengkosongkan form input
                if($this->form_validation->run()==true){
                    // echo "masuk"; die();
                    $kd_trans = $this->input->post('kd_trans');
                    $id_petugas = $this->session->userdata['id_petugas'];
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'tgl_pinjam'  => $this->input->post('tgl_pinjam'),
                        'jumlah'  => $this->input->post('jumlah'),
                        'proyek'  => $this->input->post('proyek'),
                        'id_petugas' => $id_petugas,
                        'user_update'  => $this->input->post('user_update'),
                        'update_date'  => $this->input->post('update_date')

                    );
                    $this->Mod_pinjam->updatePinjam($kd_trans, $save);
                    // echo "berhasil"; die();
                    redirect('pinjam/index/update-success');      
                }
                //jika tidak mengkosongkan
                else{
                    $kd_trans = $this->input->post('kd_trans');
                    $data['edit']    = $this->Mod_pinjam->cekPinjam($kd_trans)->row_array();
                    $data['message'] = "";
                    $this->template->load('layoutbackend', 'pinjam/pinjam_edit', $data);
                }
            } //end empty $_FILES
        } // end $_POST['update']
    }

//ini untuk User
    public function update_user()
    {
        if(isset($_POST['update'])) {
            // echo "proses update"; die();
            if(isset($_POST['update'])) {

                $this->_set_rules();
                //apabila user mengkosongkan form input
                if($this->form_validation->run()==true){
                    // echo "masuk"; die();
                    $kd_trans = $this->input->post('kd_trans');
                    $id_petugas = $this->session->userdata['id_petugas'];
                    $save  = array(
                        'kd_trans'   => $this->input->post('kd_trans'),
                        'nama_brg'  => $this->input->post('nama_brg'),
                        'tgl_pinjam'  => $this->input->post('tgl_pinjam'),
                        'jumlah'  => $this->input->post('jumlah'),
                        'proyek'  => $this->input->post('proyek'),
                        'id_petugas' => $id_petugas,
                        'user_update'  => $this->input->post('user_update'),
                        'update_date'  => $this->input->post('update_date')

                    );
                    $this->Mod_pinjam->updatePinjam($kd_trans, $save);
                    // echo "berhasil"; die();
                    redirect('pinjam/all/update-success');      
                }
                //jika tidak mengkosongkan
                else{
                    $kd_trans = $this->input->post('kd_trans');
                    $data['edit']    = $this->Mod_pinjam->cekPinjam($kd_trans)->row_array();
                    $data['message'] = "";
                    $this->template->load('layoutbackend', 'pinjam/pinjam_edit_user', $data);
                }
            } //end empty $_FILES
        } // end $_POST['update']
    }
//ini untuk Admin
    public function delete()
    {
        $kd_trans = $this->input->post('kd_trans');
        $this->Mod_pinjam->deletePinjam($kd_trans, 'pinjam');
        // echo "berhasil"; die();
        redirect('pinjam/index/delete-success');
    }
//ini untuk User
    public function delete_user()
    {
        $kd_trans = $this->input->post('kd_trans');
        $this->Mod_pinjam->deletePinjam($kd_trans, 'pinjam');
        // echo "berhasil"; die();
        redirect('pinjam/all/delete-success');
    }

    //function global buat validasi input
    public function _set_rules()
    {
        $this->form_validation->set_rules('nama_brg','Kode Flibook','required|max_length[100]');
        $this->form_validation->set_rules('kd_trans','Keterangan','required|max_length[100]');
        $this->form_validation->set_rules('tgl_pinjam','Sumber Pinjam','required|max_length[50]');
        $this->form_validation->set_rules('jumlah','Link','required|max_length[200]'); 
        $this->form_validation->set_message('required', '{field} kosong, silahkan diisi');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>","</div>");
    }

}

/* End of file Buku.php */
