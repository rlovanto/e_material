<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends MY_Controller {
	
    private $filename = "import_data"; // Kita tentukan nama filenya


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_barang');
    }
	
	public function form(){
		$data = array(); // Buat variabel $data sebagai array
		
		if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di Mod_barang.php
			$upload = $this->Mod_barang->upload_file($this->filename);
			
			if($upload['result'] == "success"){ // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH.'third_party/PHPExcel/PHPExcel.php';
				
				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
				
				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet; 
			}else{ // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}
        $this->template->load('layoutbackend', 'import/form', $data);

		// $this->load->view('form', $data);
	}
    public function export(){
		// Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		// Panggil class PHPExcel nya
		$excel = new PHPExcel();

		// Settingan awal fil excel
		$excel->getProperties()->setCreator('Admin Gudang')
							   ->setLastModifiedBy('Admin Gudang')
							   ->setTitle("Data Barang")
							   ->setSubject("Barang")
							   ->setDescription("Laporan Semua Data Barang")
							   ->setKeywords("Data Barang");

		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA BARANG "); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
        $excel->setActiveSheetIndex(0)->setCellValue('B3', "NAMA BARANG"); // Set kolom B3 dengan tulisan "NIS"
        $excel->setActiveSheetIndex(0)->setCellValue('C3', "JENIS"); // Set kolom C3 dengan tulisan "NAMA"
        $excel->setActiveSheetIndex(0)->setCellValue('D3', "SATUAN"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
        $excel->setActiveSheetIndex(0)->setCellValue('E3', "STOCK"); // Set kolom E3 dengan tulisan "ALAMAT"
        $excel->setActiveSheetIndex(0)->setCellValue('F3', "NOMER BOX"); // Set kolom E3 dengan tulisan "ALAMAT"
        $excel->setActiveSheetIndex(0)->setCellValue('G3', "KATEGORI"); // Set kolom E3 dengan tulisan "ALAMAT"
        $excel->setActiveSheetIndex(0)->setCellValue('H3', "CATATAN"); // Set kolom E3 dengan tulisan "ALAMAT"

        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);

		// Panggil function view yang ada di Mod_barang untuk menampilkan semua data siswanya
		$barang = $this->Mod_barang->view();

		$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach($barang as $data){ // Lakukan looping pada variabel barang
			$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
			$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nama_brg);
			$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->jenis);
			$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->satuan);
			$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->stock);
			$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->nomer_box);
			$excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->kategori);
			$excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->catatan);
			
			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
			
			$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}

		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(30); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(30); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(30); // Set width kolom E
		
		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Laporan Data Barang");
		$excel->setActiveSheetIndex(0);

		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Data Barang.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');

		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}

	public function import(){
		// Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		
		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();
		
		$numrow = 1;
		foreach($sheet as $row){
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'nama_brg'=>$row['A'], // Insert data nis dari kolom A di excel
					'jenis'=>$row['B'], // Insert data nama dari kolom B di excel
					'satuan'=>$row['C'], // Insert data jenis kelamin dari kolom C di excel
					'stock'=>$row['D'], // Insert data alamat dari kolom D di excel
					'nomer_box'=>$row['E'], // Insert data alamat dari kolom D di excel
					'kategori'=>$row['F'], // Insert data alamat dari kolom D di exce
					'catatan'=>$row['G'], // Insert data alamat dari kolom D di exce
                    'user_create' => $this->session->userdata('full_name'),
                    'create_date' => date('Y-m-d')
				));
			}
			
			$numrow++; // Tambah 1 setiap kali looping
		}

		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->Mod_barang->insert_multiple($data);
		
		redirect("barang"); // Redirect ke halaman awal (ke controller barang fungsi index)
	}
    public function index()
    {
        $data['barang']      = $this->Mod_barang->getAll();
        
        if($this->uri->segment(3)=="create-success") {
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Disimpan...!</p></div>";    
            $this->template->load('layoutbackend', 'barang/barang_data', $data); 
        }
        else if($this->uri->segment(3)=="update-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Update...!</p></div>"; 
            $this->template->load('layoutbackend', 'barang/barang_data', $data);
        }
        else if($this->uri->segment(3)=="delete-success"){
            $data['message'] = "<div class='alert alert-block alert-success'>
            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <p><strong><i class='icon-ok'></i>Data</strong> Berhasil Dihapus...!</p></div>"; 
            $this->template->load('layoutbackend', 'barang/barang_data', $data);
        }
        else{
            $data['message'] = "";

            $this->template->load('layoutbackend', 'barang/barang_data', $data);
        }
        
    }

    public function create()
    {
        $this->template->load('layoutbackend', 'barang/barang_create');
    }

    public function insert()
    {
        if(isset($_POST['save'])) {
        
            //function validasi
            $this->_set_rules();

            //apabila barang mengisi form
            if($this->form_validation->run()==true){
                $nama_brg = $this->input->post('nama_brg');
                $cek = $this->Mod_barang->cekBarang($nama_brg);
                //cek nis yg sudah digunakan
                if($cek->num_rows() > 0){
                    $data['message'] = "<div class='alert alert-block alert-danger'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <p><strong><i class='icon-ok'></i>Kode barang</strong> Sudah Digunakan...!</p></div>"; 
                    $this->template->load('layoutbackend', 'barang/barang_create', $data); 
                }
                //kalo blm digunakan lakukan insert data kedatabase
                else{
                    
                    $save  = array(
                        'nama_brg'   => $this->input->post('nama_brg'),
                        'jenis'  => $this->input->post('jenis'),
                        'satuan'  => $this->input->post('satuan'),
                        // 'harga'  => $this->input->post('harga'),
                        'stock'  => $this->input->post('stock'),
                        'nomer_box'  => $this->input->post('nomer_box'),
                        'kategori'  => $this->input->post('kategori'),
                        'catatan'  => $this->input->post('catatan'),
                        'user_create'  => $this->input->post('user_create'),
                        'create_date'  => $this->input->post('create_date')
                    );
                    $this->Mod_barang->insertBarang("barang", $save);
                    // echo "berhasil"; die();
                    redirect('barang/index/create-success');
                }
            }
            //jika barang mengkosongkan form input
            else{
                $this->template->load('layoutbackend', 'barang/barang_create');
            } 

        } //end $_POST['save']
    }

    public function edit()
    {
        $nama_brg = $this->uri->segment(3);
        
        $data['edit']    = $this->Mod_barang->cekBarang($nama_brg)->row_array();
        // $data['anggota'] =  $this->Mod_anggota->getAll()->result_array();
        // print_r($data['edit']); die();
        $this->template->load('layoutbackend', 'barang/barang_edit', $data);
    }

    public function update()
    {
        if(isset($_POST['update'])) {
            // echo "proses update"; die();
            if(isset($_POST['update'])) {

                $this->_set_rules();
                //apabila user mengkosongkan form input
                if($this->form_validation->run()==true){
                    // echo "masuk"; die();
                    $nama_brg = $this->input->post('nama_brg');
                    $save  = array(
                        'nama_brg'   => $this->input->post('nama_brg'),
                        'jenis'  => $this->input->post('jenis'),
                        'satuan'  => $this->input->post('satuan'),
                        'stock'  => $this->input->post('stock'),
                        'nomer_box'  => $this->input->post('nomer_box'),
                        'kategori'  => $this->input->post('kategori'),
                        'catatan'  => $this->input->post('catatan'),
                        'user_update'  => $this->input->post('user_update'),
                        'update_date'  => $this->input->post('update_date')
                    );
                    $this->Mod_barang->updateBarang($nama_brg, $save);
                    // echo "berhasil"; die();
                    redirect('barang/index/update-success');      
                }
                //jika tidak mengkosongkan
                else{
                    $nama_brg = $this->input->post('nama_brg');
                    $data['edit']    = $this->Mod_barang->cekBarang($nama_brg)->row_array();
                    $data['message'] = "";
                    $this->template->load('layoutbackend', 'barang/barang_edit', $data);
                }
            } //end empty $_FILES
        } // end $_POST['update']
    }

    public function delete()
    {
        $nama_brg = $this->input->post('nama_brg');
        $this->Mod_barang->deleteBarang($nama_brg, 'barang');
        // echo "berhasil"; die();
        redirect('barang/index/delete-success');
    }

    //function global buat validasi input
    public function _set_rules()
    {
        $this->form_validation->set_rules('nama_brg','Kode Flibook','required|max_length[100]');
        $this->form_validation->set_rules('jenis','Keterangan','required|max_length[100]');
        $this->form_validation->set_rules('stock','Sumber Barang','required|max_length[50]');
        $this->form_validation->set_message('required', '{field} kosong, silahkan diisi');
        $this->form_validation->set_error_delimiters("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>","</div>");
    }

}

/* End of file Buku.php */
