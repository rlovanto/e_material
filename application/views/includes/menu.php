<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<a class="navbar-brand" href="<?php echo base_url('dashboard'); ?>">
  <img src="<?php echo base_url(); ?>assets/img/profile/logo.png" style="height: 50px; margin-top: -7px !important;">
</a>
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url('dashboard'); ?>"><strong>E-Material-V1</strong></a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>
                <?php echo  $this->session->userdata['full_name']; ?>
                <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="<?php echo base_url('users/gantiPassword/'.$this->session->userdata['id_petugas']) ?>"><i class="glyphicon glyphicon-cog"></i> Ganti Password</a>
                </li>

                <li class="divider"></li>
                <li><a href="<?php echo base_url('login/logout') ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </li>
                <?php 
                    if($this->session->userdata['status'] == "Super Admin"){
                ?>
                <li>
                    <a href="#"><i class="fa fa-desktop fa-fw"></i> Data Master<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo base_url('kategori'); ?>"><i class="glyphicon glyphicon-filter"></i> Kategori Barang</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('barang'); ?>"><i class="glyphicon glyphicon-briefcase"></i> Barang</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('users'); ?>"><i class="glyphicon glyphicon-shopping-cart"></i> Supplier</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('users'); ?>"><i class="glyphicon glyphicon-user"></i> Users</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('backup'); ?>"><i class="glyphicon glyphicon-save-file"></i> Bakcup DB</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <?php } ?>
                <?php 
                    if($this->session->userdata['status'] == "Admin"){
                ?>
                <li>
                    <a href="#"><i class="fa fa-desktop fa-fw"></i> Data Stock<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                         
                        <li>
                            <a href="<?php echo base_url('barang'); ?>"><i class="glyphicon glyphicon-briefcase"></i> Barang</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <?php } ?>
      
                <?php 
                    if($this->session->userdata['status'] == "Admin"){
                ?>
                <li>
                    <a href="#"><i class="glyphicon glyphicon-pencil"></i> Data Transaksi - <?= $this->session->userdata('status'); ?> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
    

                        <li>
                            <a href="<?php echo base_url('keluars'); ?>"><i class="glyphicon glyphicon-export"></i> Barang Keluar</a>
                        </li>

                        <li>
                            <a href="<?php echo base_url('keluars/all'); ?>"><i class="glyphicon glyphicon-export"></i> Lihat Barang Keluar</a>
                        </li>

                        <li>
                            <a href="<?php echo base_url('peminjaman'); ?>"><i class="glyphicon glyphicon-export"></i> Peminjaman Barang</a>
                        </li>

                        <li>
                            <a href="<?php echo base_url('peminjaman/all'); ?>"><i class="glyphicon glyphicon-export"></i> Lihat Peminjaman Barang</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>                        
                <?php } ?>

                <?php 
                    if($this->session->userdata['status'] == "Super Admin"){
                ?>
                <li>
                    <a href="#"><i class="glyphicon glyphicon-pencil"></i> Transaksi Barang <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">

                        <li>
                            <a href="<?php echo base_url('masuks'); ?>"><i class="glyphicon glyphicon-import"></i> Barang Masuk</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('masuks/all'); ?>"><i class="glyphicon glyphicon-import"></i> Lihat data Masuk</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('keluars'); ?>"><i class="glyphicon glyphicon-export"></i> Barang Keluar</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('keluars/admin'); ?>"><i class="glyphicon glyphicon-export"></i> Lihat data Keluar</a>
                        </li>

                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <?php } ?>

                <?php 
                    if($this->session->userdata['status'] == "Super Admin"){
                ?>
                <li>
                    <a href="#"><i class="glyphicon glyphicon-sort"></i> Grafik Statistik<span
                            class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo base_url('statistik'); ?>"><i
                                    class="glyphicon glyphicon-eye-open"></i>
                                Statistik</a>
                        </li>
                    </ul>
                </li>
                <?php } ?>
                <?php 
                    if($this->session->userdata['status'] == "Super Admin"){
                ?>
                <li>
                    <a href="#"><i class="fa fa-file-text  fa-fw"></i> Module Laporan<span
                            class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo base_url('laporan/barang'); ?>"><i class="glyphicon glyphicon-briefcase"></i> Data
                                Barang</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('laporan/card'); ?>"><i class="glyphicon glyphicon-phone-alt"></i> Data
                                Kartu Gsm</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('laporan/pasca'); ?>"><i class="glyphicon glyphicon-envelope"></i> Data
                                Kartu Pasca Bayar</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('laporan/masuk'); ?>"><i class="glyphicon glyphicon-import"></i> Data Barang Masuk</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('laporan/keluar'); ?>"><i
                                    class="glyphicon glyphicon-export"></i> Data Barang keluar</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('laporan/pinjam'); ?>"><i class="glyphicon glyphicon-export"></i> Peminjaman Barang</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('laporan/kembali'); ?>"><i class="glyphicon glyphicon-import"></i> Pengembalian Barang</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <?php } ?>

                <li>
                    <a href="<?php echo base_url('login/logout') ?>"><i class="fa fa-power-off fa-fw"></i> Logout</a>
                </li>

            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>

<script>
    function myFunction() {
        window.print();
    }
</script>