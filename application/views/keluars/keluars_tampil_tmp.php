<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>Id Barang</td>
            <td>Nama Barang</td>
            <td>Satuan</td>
            <td>Jumlah</td>
            <td>Stock</td>
            <td></td>
        </tr>
    </thead>
    <?php foreach($tmp_klr as $tmp_klr):?>
    <tr>
        <td><?php echo $tmp_klr->id_brg;?></td>
        <td><?php echo $tmp_klr->nama_brg;?></td>
        <td><?php echo $tmp_klr->satuan;?></td>
        <td><?php echo $tmp_klr->jumlah_x;?></td>
        <td><?php echo $tmp_klr->stock;?></td>
        <!-- <td><input type="text" name="jumlah_x" class="form-control" placeholder="Jumlah" value="<?php echo set_value('jumlah_x'); ?>"> -->
        </td>
        <td><a href="#" class="hapus" kode="<?php echo $tmp_klr->id_brg;?>"><i class="glyphicon glyphicon-trash"></i></a></td>
    </tr>
    <?php endforeach;?>
    <tr>
        <td colspan="2">Total Barang Keluar</td>
        <td colspan="2"><input type="text" id="jumlahTmp" readonly="readonly" value="<?php echo $jumlahTmp;?>" class="form-control"></td>
    </tr>
</table>