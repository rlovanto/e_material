<div class="row">
    <div class="col-lg-12"><br />

        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('keluars'); ?>">Barang Keluar</a></li>
            <li class="active">Data Barang Keluar</li>
        </ol>

        <?php
            
            if(!empty($message)) {
                echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <?php echo anchor('keluars', 'Add', array('class' => 'btn btn-primary btn-sm')); ?>
        <?php echo anchor('keluars/grafik', 'statistik', array('class' => 'btn btn-primary btn-sm')); ?>

        <br /><br />
        <div class="panel panel-default">

            <div class="panel-heading">
                Data Barang Keluar
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <td>No.</td>
                            <td>ID Transaksi</td>
                            <td>Tgl Keluar</td>
                            <td>Nama barang</td>
                            <td>Jumlah</td>
                            <td>Proyek</td>
                            <td>User Submit</td>
                            <td>Tanggal Submit</td>
                            <?php 
                                if($this->session->userdata['status'] == "Super Admin"){
                            ?>
                            <td>Aksi</td>
                            <?php } ?>

                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                                $no = 1;
                                foreach($keluars->result() as $row) {
                                ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row->id_transaksi;?></td>
                            <td><?php echo $row->tanggal_keluar;?></td>
                            <td><?php echo $row->nama_brg;?></td>
                            <td><?php echo $row->jumlah_x;?></td>
                            <td><?php echo $row->proyek;?></td>
                            <td><?php echo $row->user_create;?></td>
                            <td><?php echo $row->create_date;?></td>
                            <?php 
                                if($this->session->userdata['status'] == "Super Admin"){
                            ?>
                            <td class="text-center">
                            <a href="#" name="<?php echo $row->id_transaksi;?>" class="hapus btn btn-danger btn-xs"
                                    kode="<?php echo $row->id_transaksi;?>">Hapus</a>
                            </td>
                            <?php } ?>

                        </tr>
                        <?php $no++; } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- Modal Hapus-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="idhapus" id="idhapus">
                <p>Apakah anda yakin ingin menghapus keluars <strong class="text-konfirmasi"> </strong> ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-xs" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger btn-xs" id="konfirmasi">Hapus</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-plugins/dataTables.bootstrap.min.js">
</script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-responsive/dataTables.responsive.js">
</script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>

<script type="text/javascript">
    // function confirmDelete()
    // {
    //     return confirm("Apakah anda yakin ingin menghapus data anggota")
    // }

    $(function () {
        $(".hapus").click(function () {
            var kode = $(this).attr("kode");
            var name = $(this).attr("name");

            $(".text-konfirmasi").text(name)
            $("#idhapus").val(kode);
            $("#myModal").modal("show");
        });

        $("#konfirmasi").click(function () {
            var kode = $("#idhapus").val();

            $.ajax({
                url: "<?php echo site_url('keluars/delete');?>",
                type: "POST",
                data: "id_transaksi=" + kode,
                cache: false,
                success: function (html) {
                    location.href =
                        "<?php echo site_url('keluars/admin/delete-success');?>";
                }
            });
        });
    });
</script>