<div class="row">
    <div class="col-lg-12"><br />

        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('barang'); ?>">Barang</a></li>
            <li class="active">Edit Barang</li>
        </ol>

        <?php
            echo validation_errors();
            //buat message npm
            if(!empty($message)) {
            echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">

            <div class="panel-heading">
                Edit Barang
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <?php
                //validasi error upload
                if(!empty($error)) {
                    echo $error;
                }
            ?>
                <?php echo form_open_multipart('barang/update', array('class' => 'form-horizontal', 'id' => 'jvalidate')) ?>
                <div class="form-group">
                    <p class="col-sm-2 text-left">ID barang </p>
                    <div class="col-sm-10">
                        <input type="hidden" name="id_brg" class="form-control" placeholder="Nama barang"
                            value="<?php echo $edit['id_brg']; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Nama barang </p>

                    <div class="col-sm-10">
                        <input type="text" name="nama_brg" class="form-control" placeholder="Nama barang"
                            value="<?php echo $edit['nama_brg']; ?>" readonly="readonly">
                    </div>
                </div>
                
                <div class="form-group">
                    <p class="col-sm-2 text-left">Jenis Barang </p>
                    <div class="col-sm-10">
                        <?php 
                            $jenis = array('Sensor' => 'Sensor', 'Board' => 'Board', 'Modul' => 'Modul', 'Komponen Aktif' => 'Komponen Aktif', 'Komponen Pasif' => 'Komponen Pasif', 'Lain-lain' => 'Lain-lain', 'Board MCU' => 'Board MCU', 'Konektor' => 'Konektor', 'Tools' => 'Tools', 'ATK' => 'ATK'); 
                            echo form_dropdown('jenis', $jenis, $edit['jenis'],"class='form-control'");    
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <p class="col-sm-2 text-left">Satuan </p>
                    <div class="col-sm-10">
                        <?php 
                            $satuan = array('PCS' => 'PCS', 'DUS' => 'DUS', 'PACK' => 'PACK', 'LITER' => 'LITER', 'METER' => 'METER'); 
                            echo form_dropdown('satuan', $satuan, $edit['satuan'],"class='form-control'");    
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Stock </p>
                    <div class="col-sm-10">
                        <input type="text" name="stock" class="form-control" placeholder="Stock Barang"
                            value="<?php echo $edit['stock']; ?>" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Nomor Box </p>
                    <div class="col-sm-10">
                        <input type="text" name="nomer_box" class="form-control" placeholder="Nomor Box Barang"
                            value="<?php echo $edit['nomer_box']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Kategori Barang </p>
                    <div class="col-sm-10">
                        <?php 
                            $kategori = array('Habis Pakai' => 'Habis Pakai', 'Asset' => 'Asset'); 
                            echo form_dropdown('kategori', $kategori, $edit['kategori'],"class='form-control'");    
                        ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <p class="col-sm-2 text-left">Catatan </p>
                    <div class="col-sm-10">
                        <input type="text" name="catatan" class="form-control" placeholder="Catatan Barang"
                            value="<?php echo $edit['catatan']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <p class="col-sm-2 text-left"><b/>*Jika Jenis Barang "Lain-lain" Catatan Silahkan Diisi untuk penambahan Jenis barang baru Di Database ,Jika bukan beri Tanda "--"</p>
                </div>
                <!-- <div class="form-group">
                    <p class="col-sm-2 text-left">Images</p>

                    <div class="col-sm-10">
                        <?php if($edit['image'] != '') { ?>
                            <img src="<?php echo base_url('assets/img/barang/'.$edit['image']);?>" width="100px" height="100px">
                        <?php }else{ ?>
                            <img src="<?php echo base_url('assets/img/barang/book-default.jpg');?>" width="100px" height="100px">
                        <?php } ?> <br /><br />
                        <input type="file" name="userfile" class="form-control btn-file"  value="<?php echo set_value('userfile'); ?>">
                    </div>
                </div> -->

                <input type="hidden" class="form-control" id="user_update" name="user_update"  value="<?= $this->session->userdata('full_name'); ?>" required>             
                <input name="update_date" type="hidden" id="update_date" value=" <?php echo date('Y-m-d'); ?> "readonly>
                
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="btn-group pull-left">
                            <?php echo anchor('barang', 'Cancel', array('class' => 'btn btn-danger btn-sm')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-group pull-right">
                            <input type="submit" name="update" value="Update" class="btn btn-success btn-sm">
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/bootstrap-datepicker.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/tinymce/tinymce.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>



<script type="text/javascript">
    tinymce.init({
        selector: 'textarea'
    });

    $(document).ready(function () {
        $("#tanggal1").datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#tanggal2").datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#tanggal").datepicker({
            format: 'yyyy-mm-dd'
        });
    })
</script>