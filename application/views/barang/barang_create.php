<div class="row">
    <div class="col-lg-12"><br />
       
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('barang'); ?>">Barang</a></li>
            <li class="active">Create Barang</li>
        </ol>

        <?php
            echo validation_errors();
            //buat message npm
            if(!empty($message)) {
            echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">

            <div class="panel-heading">
                Create Barang
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <?php
                //validasi error upload
                if(!empty($error)) {
                    echo $error;
                }
            ?>
            <?php echo form_open_multipart('barang/insert', array('class' => 'form-horizontal', 'id' => 'jvalidate')) ?>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Nama Barang </p>

                    <div class="col-sm-10">
                        <input type="text" name="nama_brg" class="form-control" placeholder="Nama Barang" value="<?php echo set_value('nama_brg'); ?>">
                    </div>
                </div>
                
                <div class="form-group">
                    <p class="col-sm-2 text-left">Jenis Barang </p>
                    <div class="col-sm-10">
                        <select name="jenis" class="form-control">
                            <option value="">- Pilih Jenis Barang -</option>
                            <option value="Sensor" <?php echo set_select('jenis','Sensor'); ?>>Sensor</option>
                            <option value="Board" <?php echo set_select('jenis','Board'); ?>>Board</option>
                            <option value="Modul" <?php echo set_select('jenis','Modul'); ?>>Modul</option>
                            <option value="Komponen Aktif" <?php echo set_select('jenis','Komponen Aktif'); ?>>Komponen Aktif</option>
                            <option value="Komponen Pasif" <?php echo set_select('jenis','Komponen Pasif'); ?>>Komponen Pasif</option>
                            <option value="Lain-lain" <?php echo set_select('jenis','Lain-lain'); ?>>Lain - Lain </option>
                            <option value="Board MCU" <?php echo set_select('jenis','Board MCU'); ?>>Board MCU</option>
                            <option value="Konektor" <?php echo set_select('jenis','Konektor'); ?>>Konektor</option>
                            <option value="Tools" <?php echo set_select('jenis','Tools'); ?>>Tools</option>
                            <option value="ATK" <?php echo set_select('jenis','ATK'); ?>>ATK</option>

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Satuan </p>
                    <div class="col-sm-10">
                        <select name="satuan" class="form-control">
                            <option value="">- Pilih Satuan -</option>
                            <option value="PCS" <?php echo set_select('satuan','PCS'); ?>>PCS</option>
                            <option value="DUS" <?php echo set_select('satuan','DUS'); ?>>DUS</option>
                            <option value="PACK" <?php echo set_select('satuan','PACK'); ?>>PACK</option>
                            <option value="LITER" <?php echo set_select('satuan','LITER'); ?>>LITER</option>
                            <option value="METER" <?php echo set_select('satuan','METER'); ?>>METER</option>
                        </select>
                    </div>
                </div>

                <!-- <div class="form-group">
                    <p class="col-sm-2 text-left">Link Reference </p>

                    <div class="col-sm-10">
                        <input type="text" name="link" class="form-control" placeholder="Link Reference" value="<?php echo set_value('link'); ?>">
                    </div>
                </div> -->
                        <input type="hidden" name="stock" class="form-control" placeholder="Stock" value="0" readonly>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Nomor Box</p>
                    <div class="col-sm-10">
                        <input type="text" name="nomer_box" class="form-control" placeholder="Nomor box" value="<?php echo set_value('nomer_box'); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <p class="col-sm-2 text-left">Kategori </p>
                    <div class="col-sm-10">
                        <select name="kategori" class="form-control">
                            <option value="">- Pilih Kategori -</option>
                            <option value="Habis Pakai" <?php echo set_select('kategori','Habis Pakai'); ?>>Habis Pakai</option>
                            <option value="Asset" <?php echo set_select('kategori','Asset'); ?>>Asset</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left"><b/>Catatan</p>
                    <div class="col-sm-10">
                        <input type="text" name="catatan" class="form-control" placeholder="isi catatan" value="--">

                        <!-- <textarea rows="4" cols="50" name="comment" form="usrform" name="catatan" class="form-control" placeholder="Catatan" value="<?php echo set_value('catatan'); ?>">
                         Enter text here...</textarea> -->
                    </div>
                </div>
                <div class="form-group">
                    <p class="col-sm-2 text-left">*Jika Jenis Barang "Lain-lain" Catatan Silahkan Diisi untuk penambahan Jenis barang baru Di Database ,Jika bukan beri Tanda "--"</p>
                </div>
                <!-- <div class="form-group">
                    <p class="col-sm-2 text-left">Images</p>

                    <div class="col-sm-10">
                        <input type="file" name="userfile" class="form-control btn-file"  value="<?php echo set_value('userfile'); ?>">
                    </div>
                </div> -->

                <input type="hidden" class="form-control" id="user_create" name="user_create" value="<?= $this->session->userdata('nama_karyawan'); ?>" required>
                <input name="create_date" type="hidden" id="create_date" value=" <?php echo date('Y-m-d'); ?> " readonly>
            
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="btn-group pull-left">
                            <?php echo anchor('barang', 'Cancel', array('class' => 'btn btn-danger btn-sm')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-group pull-right">
                            <input type="submit" name="save" value="Save" class="btn btn-success btn-sm">
                        </div>
                    </div>
                </div>
            <?php echo form_close(); ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/bootstrap-datepicker.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/tinymce/tinymce.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>



<script type="text/javascript">

tinymce.init({selector:'textarea'});

$(document).ready(function() {
    $("#tanggal1").datepicker({
        format:'yyyy-mm-dd'
    });
    
    $("#tanggal2").datepicker({
        format:'yyyy-mm-dd'
    });
    
    $("#tanggal").datepicker({
        format:'yyyy-mm-dd'
    });
})



</script>