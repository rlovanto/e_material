<div class="row">
    <div class="col-lg-12"><br />
       
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('card'); ?>">Kartu Gsm</a></li>
            <li class="active">Data Kartu Gsm</li>
        </ol>

        <?php
            
            if(!empty($message)) {
                echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <?php 
            if($this->session->userdata['status'] == "Super Admin"){
        ?>
        <?php echo anchor('card/create', 'Add', array('class' => 'btn btn-primary btn-sm')); ?>
        <?php echo anchor('card/form', 'Import Data', array('class' => 'btn btn-primary btn-sm')); ?>
        <?php } ?>

        <br />
        <br />
        <div class="panel panel-default">

            <div class="panel-heading">
                Data Kartu Gsm
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <td>No.</td>
                                        <td>Masa Aktif</td>
                                        <td>Masa Tenggang</td>
                                        <td>Type Kartu</td>
                                        <td>kartu</td>
                                        <td>Nomor</td>
                                        <td>Tempat</td>
                                        <td>PIC Kontak</td>
                                        <?php 
                                            if($this->session->userdata['status'] == "Super Admin"){
                                        ?>
                                        <td>Aksi</td>
                                        <?php } ?>

                                    </tr>
                                </thead>
                                <tbody>

                                <?php 
                                $no = 1;
                                
                                foreach($card->result() as $row) {
                                    $masaaktif = $row->waktu;

                                    $sekarang = date("d-m-Y");

                                    $masaberlaku = strtotime($masaaktif) - strtotime($sekarang);

                                    // echo "Masa Aktif  sampai dengan: $masaaktif <br> Sekarang : $sekarang <p>";
                                    // echo "Masa aktif  kurang".$masaberlaku/(24*60*60)." hari lagi";
                                ?>

                                    <tr>
                                        <td><?php echo $no;?></td>
                                        <td><?php echo $row->waktu;?></td>
                                        <td><b/><?php echo "Masa aktif kurang ".$masaberlaku/(24*60*60)." hari lagi";?></td>
                                        <!-- <td></td> -->
                                        <td><?php echo $row->type;?></td>
                                        <td><?php echo $row->kartu;?></td>
                                        <td><?php echo $row->nomor;?></td>
                                        <td><?php echo $row->tempat;?></td>
                                        <td><?php echo $row->pic_cont;?></td>
                                        <?php 
                                            if($this->session->userdata['status'] == "Super Admin"){
                                        ?>
                                        <td class="text-center">
                                            <a href="<?php echo base_url('card/edit/'.$row->nomor) ?>"><input nomor="submit" class="btn btn-success btn-xs" name="edit" value="Edit"></a>
                                            <a href="#" name="<?php echo $row->nomor;?>" class="hapus btn btn-danger btn-xs" kode="<?php echo $row->nomor;?>">Hapus</a>
                                        </td>
                                         <?php } ?>

                                    </tr>
                                <?php $no++; } ?>    
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- Modal Hapus-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Konfirmasi</h4>
        </div>
        <div class="modal-body">
            <input type="hidden" name="idhapus" id="idhapus">
                <p>Apakah anda yakin ingin menghapus card <strong class="text-konfirmasi"> </strong> ?</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-success btn-xs" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger btn-xs" id="konfirmasi">Hapus</button>
        </div>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#dataTables-example').DataTable({
        responsive: true
    });
});
</script>

<script type="text/javascript">
    // function confirmDelete()
    // {
    //     return confirm("Apakah anda yakin ingin menghapus data anggota")
    // }

    $(function(){
        $(".hapus").click(function(){
            var kode=$(this).attr("kode");
            var name=$(this).attr("name");
           
            $(".text-konfirmasi").text(name)
            $("#idhapus").val(kode);
            $("#myModal").modal("show");
        });
        
        $("#konfirmasi").click(function(){
            var kode = $("#idhapus").val();
            
            $.ajax({
                url:"<?php echo site_url('card/delete');?>",
                type:"POST",
                data:"nomor="+kode,
                cache:false,
                success:function(html){
                    location.href="<?php echo site_url('card/index/delete-success');?>";
                }
            });
        });
    });
</script>

