<div class="row">
    <div class="col-lg-12"><br />
       
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('masuk/create'); ?>">Barang Masuk</a></li>
            <li class="active">Create Barang Masuk</li>
        </ol>

        <?php
            echo validation_errors();
            //buat message npm
            if(!empty($message)) {
            echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">

            <div class="panel-heading">
                Create Barang Masuk
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <?php
                //validasi error upload
                if(!empty($error)) {
                    echo $error;
                }
            ?>
            <?php echo form_open_multipart('masuk/insert', array('class' => 'form-horizontal', 'id' => 'jvalidate')) ?>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Id Transaksi</p>

                    <div class="col-sm-10">
                        <input type="text" name="kd_trans" class="form-control" placeholder="Id Transaksi" value="<?= $kd_trans; ?>" readonly>
                        <!-- <input type="text" name="kd_trans" class="form-control" placeholder="Id Transaksi" value="<?php echo set_value('kd_trans'); ?>"> -->
                    </div>
                </div>
                <div class="form-group">
                    <p class="col-sm-2 text-left">Nama Barang </p>

                    <div class="col-sm-10">
                            <select name="nama_brg" id="nama_brg" class="form-control" onchange='changeValue(this.value)' required>
                                <option value="">-Pilih-</option>
                                    <?php
                                    
                                    $con = mysqli_connect("localhost", "root", "", "inventory_sensync");
                                    $query=mysqli_query($con, "select * from barang order by nama_brg asc"); 
                                    $result = mysqli_query($con, "select * from barang");  
                                    $jsArray = "var prdName = new Array();\n";
                                    while ($row = mysqli_fetch_array($result)) {  
                                    echo '<option name="nama_brg"  value="' . $row['nama_brg'] . '">' . $row['nama_brg'] . '</option>';  
                                    $jsArray .= "prdName['" . $row['nama_brg'] . "'] = {jumlah:'" . addslashes($row['jumlah']) . "',jumlah:'".addslashes($row['jumlah'])."',harga:'".addslashes($row['harga'])."'};\n";
                                    }
                                    ?>
                            </select>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Merk/Brand</p>
                    <div class="col-sm-10">
                        <input type="text" name="merk" class="form-control" placeholder="Merk/Brand" value="<?php echo set_value('merk'); ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Tgl Masuk Barang </p>

                    <div class="col-sm-10">
                        <input type="date" name="tgl_masuk" class="form-control" placeholder="Tanggal Masuk Barang" value="<?php echo set_value('tgl_masuk'); ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Jumlah</p>
                    <div class="col-sm-10">
                        <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" value="<?php echo set_value('jumlah'); ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Supplier</p>
                    <div class="col-sm-10">
                        <input type="text" name="supplier" class="form-control" placeholder="Supplier" value="<?php echo set_value('supplier'); ?>">
                    </div>
                </div>
                
                <div class="form-group">
                    <p class="col-sm-2 text-left">Harga Beli</p>
                    <div class="col-sm-10">
                        <input type="text" name="harga" class="form-control" placeholder="Harga Beli" value="<?php echo set_value('harga'); ?>">
                    </div>
                </div>
                
                <div class="form-group">
                    <p class="col-sm-2 text-left">Link Reference </p>

                    <div class="col-sm-10">
                        <input type="text" name="link" class="form-control" placeholder="Link Reference" value="<?php echo set_value('link'); ?>">
                    </div>
                </div>
                <input type="hidden" class="form-control" id="user_create" name="user_create" value="<?= $this->session->userdata('full_name'); ?>" required>
                <input name="create_date" type="hidden" id="create_date" value=" <?php echo date('Y-m-d'); ?> " readonly>

                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="btn-group pull-left">
                            <?php echo anchor('masuk', 'Cancel', array('class' => 'btn btn-danger btn-sm')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-group pull-right">
                            <input type="submit" name="save" value="Save" class="btn btn-success btn-sm">
                        </div>
                    </div>
                </div>
            <?php echo form_close(); ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/bootstrap-datepicker.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/tinymce/tinymce.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>



<script type="text/javascript">

tinymce.init({selector:'textarea'});

$(document).ready(function() {
    $("#tanggal1").datepicker({
        format:'yyyy-mm-dd'
    });
    
    $("#tanggal2").datepicker({
        format:'yyyy-mm-dd'
    });
    
    $("#tanggal").datepicker({
        format:'yyyy-mm-dd'
    });
})



</script>
<script type="text/javascript"> 
<?php echo $jsArray; ?>
function changeValue(id){
    document.getElementById('jumlah').value = prdName[id].jumlah;
    document.getElementById('harga').value = prdName[id].harga;
    document.getElementById('jumlah').value = prdName[id].jumlah;
};
</script>
 

