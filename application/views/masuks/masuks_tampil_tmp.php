<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>Id Barang</td>
            <td>Nama Barang</td>
            <td>Satuan</td>
            <td>Jumlah</td>
            <td>Merk</td>
            <td>Harga</td>
            <td>Link</td>
            <td></td>
        </tr>
    </thead>
    <?php foreach($tmp_msk as $tmp_msk):?>
    <tr>
        <td><?php echo $tmp_msk->id_brg;?></td>
        <td><?php echo $tmp_msk->nama_brg;?></td>
        <td><?php echo $tmp_msk->satuan;?></td>
        <td><?php echo $tmp_msk->jumlah_x;?></td>
        <td><?php echo $tmp_msk->merk;?></td>
        <td><?php echo $tmp_msk->harga;?></td>
        <td><?php echo $tmp_msk->link;?></td>
        <td><a href="#" class="hapus" kode="<?php echo $tmp_msk->id_brg;?>"><i class="glyphicon glyphicon-trash"></i></a></td>
    </tr>
    <?php endforeach;?>
    <tr>
        <td colspan="2">Total Barang Masuk</td>
        <td colspan="2"><input type="text" id="jumlahTmp" readonly="readonly" value="<?php echo $jumlahTmp;?>" class="form-control"></td>
    </tr>
</table>