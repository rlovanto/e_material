<div class="row">
    <div class="col-lg-12"><br />
       
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('masuks'); ?>">Transaksi Barang Masuk</a></li>
            <li class="active">masuks</li>
        </ol>

        <?php
            
            if(!empty($message)) {
                echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- <div class="row"> -->
    <div class="col-lg-12">
    <br>

    <!-- <legend>Transaksi</legend> -->
        <div class="panel panel-default">
            <div class="panel-body">
                <form class="form-horizontal" action="<?php echo site_url('masuks/simpan_transaksi');?>" method="post">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-lg-4 ">No. Transaksi</label>
                            <div class="col-lg-7">
                                <input type="text" id="id_transaksi" name="id_transaksi" class="form-control" value="<?php echo $autonumber ?>" readonly="readonly">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-lg-4 ">Tgl Barang Masuk</label>
                            <div class="col-lg-7">
                                <!-- <input type="date" id="tanggal_masuk" name="tanggal_masuk" class="form-control" > -->
                                <input type="date" id="tanggal_masuk" name="tanggal_masuk" class="form-control" placeholder="tanggal_masuk" value="<?php echo set_value('tanggal_masuk'); ?>">

                            </div>
                        </div>
 
                        <input type="hidden" class="form-control" id="user_create" name="user_create" value="<?= $this->session->userdata('nama_karyawan'); ?>" required>
                        <input name="create_date" type="hidden" id="create_date" value=" <?php echo date('Y-m-d'); ?> " readonly>

                    </div>
                    
                    </div>
                </form>
            </div>
        </div>

        <!-- data buku -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Data Barang</strong>
            </div>
            
            <div class="panel-body">
                <div class="col-md-6">
                        <input type="hidden" class="form-control"  id="id_brg" >
                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" class="form-control"  id="nama_brg" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label >Satuan</label>
                        <input type="text" class="form-control"  id="satuan" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label >Jumlah</label>
                        <input type="text" class="form-control"  id="jumlah_x">
                    </div>
                    <div class="col-md-6">

                        <div class="form-group ">
                            <label class="sr-only">Barang</label>
                            <button id="tambah_buku" class="btn btn-primary"> Add Barang <i class="glyphicon glyphicon-plus"></i></button>
                        </div>
                       
                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="sr-only">Barang</label>
                            <button id="cari" class="btn btn-success"> Search Barang <i class="glyphicon glyphicon-search"></i></button>
                        </div>
                       
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Merk</label>
                        <input type="text" class="form-control"  id="merk">
                    </div>
                    <div class="form-group">
                        <label >Harga</label>
                        <input type="text" class="form-control"  id="harga">
                    </div>
                    <div class="form-group">
                        <label >Link</label>
                        <input type="text" class="form-control"  id="link">
                    </div>
                </div>
                <br /><br />

                <!-- buat tampil tabel tmp     -->
                <div id="tampil"></div>
            </div>
            
            
            
            <div class="panel-footer">
                <button id="simpan" class="btn btn-primary"><i class="glyphicon glyphicon-hdd"></i> Simpan</button>
                <?php echo anchor('masuks/all', 'Cancel', array('class' => 'btn btn-danger btn-sm ')); ?>

            </div>
        </div>
        <!-- end data buku -->

        
    </div>
    <!-- /.col-lg-12 -->

</div>
<!-- /.end row -->

 

<!-- Modal Cari Buku -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><strong>Search Barang</strong></h4>
        </div>
        <div class="modal-body"><br />
            <!--<label class="col-lg-4 control-label">Cari Nama Nasabah</label>-->
            <input type="text" name="caribarang" id="caribarang" class="form-control" placeholder="please search barang code">

            <div id="tampilbuku"></div>

        </div>

        <br /><br />
        <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary" id="konfirmasi">Hapus</button>-->
        </div>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- End Modal Cari Buku -->





<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {

    //alert('');

    $('#dataTables-example').DataTable({
        responsive: true
    });


    //load data tmp 
    function loadData()
    {
        $("#tampil").load("<?php echo site_url('masuks/tampil_tmp') ?>");
    }
    loadData();

    //function buat mengkosong form data buku setelah di tambah ke tmp
    function EmptyData()
    {
        $("#id_brg").val("");
        $("#nama_brg").val("");
        $("#satuan").val("");
        $("#jumlah_x").val("");
        $("#merk").val("");
        $("#harga").val("");
        $("#link").val("");
    }

    //ambil data anggota berdasarkan npm
    // $("#npm").click(function(){
    $("#npm").change(function(){    
        var npm = $("#npm").val();
        
        $.ajax({
            url:"<?php echo site_url('masuks/cari_anggota');?>",
            type:"POST",
            data:"npm="+npm,
            cache:false,
            success:function(html){
                $("#nama").val(html);
                // document.write(html)
            }
        })
        
    });

    //show modal search buku
    $("#cari").click(function(){
        $("#myModal2").modal("show");
        //return false;  biar gk langsung ilang
    })

    //search buku
    $("#caribarang").keyup(function(){
        var caribarang = $('#caribarang').val();

         $.ajax({
            url:"<?php echo site_url('masuks/cari_buku');?>",
            type:"POST",
            data:"caribarang="+caribarang,
            cache:false,
            success:function(hasil){
                $("#tampilbuku").html(hasil);
                
            }
        })
        
    })


    //tambah buku dari modal ke form
    
    // $(".tambah").live("click", function(){
    $('body').on('click', '.tambah', function(){
        
        var id_brg = $(this).attr("kode");
        var nama_brg     = $(this).attr("nama_brg");
        var satuan = $(this).attr("satuan");
        var jumlah_x = $(this).attr("jumlah_x");
        var merk = $(this).attr("merk");
        var harga = $(this).attr("harga");
        var link = $(this).attr("link");
        
        $("#id_brg").val(id_brg);
        $("#nama_brg").val(nama_brg);
        $("#satuan").val(satuan);
        $("#jumlah_x").val(jumlah_x);
        $("#merk").val(merk);
        $("#harga").val(harga);
        $("#link").val(link);

        $("#myModal2").modal("hide");
        //console.log(id_brg);
       
    });


    //event keypress cari kode
    $("#id_brg").keypress(function(){
        
        //13 adalah kode asci buat enter
        if(event.which == 13) {
            var id_brg = $("#id_brg").val();

            $.ajax({
                url:"<?php echo site_url('masuks/cari_kode_buku');?>",
                type:"POST",
                data:"id_brg="+id_brg,
                cache:false,
                success:function(hasil){
                //split digunakan untuk memecah string    
                   data = hasil.split("|");
                   if(data==0) {
                       alert("Barang " + id_brg + " Barang Not Found");
                       $("#nama_brg").val("");
                       $("#satuan").val("");
                       $("#jumlah_x").val("");
                       $("#merk").val("");
                       $("#harga").val("");
                       $("#link").val("");
                   }
                   else{
                       $("#nama_brg").val(data[0]);
                       $("#satuan").val(data[1]);
                       $("#jumlah_x").val(data[2]);
                       $("#merk").val(data[3]);
                       $("#harga").val(data[4]);
                       $("#link").val(data[5]);
                       $("#tambah_buku").focus();
                   }

                   //console.log(data);
                }
            })
            
        } 

    }) //end event keypress

    //tambah_buku ke tmp
    $("#tambah_buku").click(function(){
        
        var id_brg = $("#id_brg").val();
        var nama_brg     = $("#nama_brg").val();
        var satuan = $("#satuan").val();
        var jumlah_x = $("#jumlah_x").val();
        var merk = $("#merk").val();
        var harga = $("#harga").val();
        var link = $("#link").val();

        if(id_brg == "") {
            alert("Kode " + id_brg + " Masih Kosong ");
            $("#id_brg").focus();
            return false;
        }
        else if(nama_brg == ""){
            alert("Nama " + nama_brg + " Masih Kosong ");
            return false;
        }
        else if(jumlah_x == ""){
            alert("Jumlah " + jumlah_x + " Masih Kosong ");
            return false;
        }
        else if(merk == ""){
            alert("Merk " + merk + " Masih Kosong ");
            return false;
        }
        else if(harga == ""){
            alert("Harga " + harga + " Masih Kosong ");
            return false;
        }
        else if(link == ""){
            alert("Link " + link + " Masih Kosong ");
            return false;
        }
        else{
            $.ajax({
                url:"<?php echo site_url('masuks/save_tmp');?>",
                type:"POST",
                data:"id_brg="+id_brg+"&nama_brg="+nama_brg+"&satuan="+satuan+"&jumlah_x="+jumlah_x+"&merk="+merk+"&harga="+harga+"&link="+link,
                cache:false,
                success:function(hasil){
                    loadData();
                    EmptyData();
                    //alert(hasil);
                   //console.log(data);
                }
            })
        }

    }) //end tambahbuku 

    // //delete tabel tmp
    $('body').on('click', '.hapus', function(){
        
        //ambil dulu atribute kodenya
        var id_brg = $(this).attr('kode');
        $.ajax({
            url:"<?php echo site_url('masuks/hapus_tmp');?>",
            type:"POST",
            data:"id_brg="+id_brg,
            cache:false,
            success:function(hasil){
                // alert(hasil);
                loadData();
            }
        })
        

    }); //end delete table tmp

    //simpan transaksi 
    //$("#simpan").click(function(){
    $('body').on('click', '#simpan', function(){    
        
        //tampung data dari form buat dikirim 
        var id_transaksi = $("#id_transaksi").val();
        var tanggal_masuk   = $("#tanggal_masuk").val();
        // var jumlah_x  = $("#jumlah_x").val();
        // var merk          = $("#merk").val();
        var user_create          = $("#user_create").val();
        var create_date          = $("#create_date").val();

        var jumlah_tmp   = parseInt($("#jumlahTmp").val(), 10);

        //cek npm jika kosong 
        // if(merk == "") {
        //     alert("Pilih Untuk Proyek Apa?");
        //     $("#merk").focus();
        //     return false;
        // }
        // else 
        if(jumlah_tmp == 0){
            alert("Pilih Data Barang yang Masuk");
            return false;
        }
        else{
            $.ajax({
                url:"<?php echo site_url('masuks/simpan_transaksi');?>",
                type:"POST",
                data:"id_transaksi="+id_transaksi+"&tanggal_masuk="+tanggal_masuk+"&jumlah_tmp="+jumlah_tmp,
                cache:false,
                success:function(hasil){
                  //console.log(hasil);
                 
                  alert("Transaksi masuks Berhasil");
                  
                  location.reload();
                }
            })
        }
        
    })


  

});
</script>



