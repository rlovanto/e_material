<style type="text/css">
    #diprint2 {
        opacity: 0;
        margin-top: -90px;
    }

    @media print {
        #diprint {
            display: none;
        }

        #diprint2 {
            opacity: 1;
            margin-top: 0px !important;
        }

        .minus2 {
            margin-top: -50px;
            margin-bottom: 30px;
        }

        body {
            font-size: 10.5px;
        }
    }
</style>

<div class="row">
    <div class="col-lg-12"><br />

        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('laporan/news'); ?>">Laporan</a></li>
            <li class="active">Data News</li>
        </ol>

        <?php
            
            if(!empty($message)) {
                echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="row" id="diprint2">
            <div class="col-md-2" style="margin-left: 100px; margin-top: 30px;">
                <img src="<?= base_url('assets/img/profile/logo.png'); ?>" width="50px" height="50px">
            </div>
            <div class="col-md-6 minus2">
                <div class="mb-2" style="font-size: 20px; text-align: center;">Laporan News</div>
                <div style="font-size: 15px; text-align: center;">E-Learning</div>
                <!-- <div style="font-size: 20px;">Telp. (022)-7326134</div> -->
            </div>
        </div>

        <div class="panel panel-default">

            <div class="panel-heading" id="diprint">
                <form action="<?php echo site_url('laporan/news'); ?>" method="get">
                    <div class="row">
                        <div class="form-group col-md-3" id="diprint">
                            <label for="exampleInputName2">Tanggal Mulai</label>&nbsp;&nbsp;
                            <input type="date" name="tanggalMulai" class="form-control" placeholder="Tanggal Mulai"
                                style="width: 200px;">
                        </div>
                        <div class="form-group col-md-3" id="diprint">
                            <label for="exampleInputEmail2">Tanggal Selesai</label>
                            <input type="date" name="tanggalAkhir" class="form-control" placeholder="Tanggal Selesai"
                                style="width: 200px;">
                        </div>
                        <div class="form-group col-md-5" style="margin-top:25px">
                            <button type="submit" class="btn btn-primary" id="diprint">Cari Data</button>
                            <a href="<?php echo site_url('laporan/news'); ?>" class="btn btn-danger" id="diprint"
                                style="text-decoration:none; color: black;">Reset</a>
                            <button href="#" onclick="myFunction()" target="_blank" type="submit" id="diprint"
                                class="btn btn-primary diprint">Cetak data</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <td>No.</td>
                            <!-- <td>Image</td> -->
                            <td>Kode News</td>
                            <td>Judul</td>
                            <td>Isi</td>
                            <td>Sumber</td>
                            <td>Tgl Rilis</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $no = 1;
                            foreach($news->result() as $row) {
                        ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <!-- jika ada News di dalam database maka tampilkan -->
                            <!-- <td><?php if($row->image != "") { ?>
                                <img src="<?php echo base_url('assets/img/News/'.$row->image);?>" width="100px"
                                    height="100px">
                                <?php }else{ ?>
                                <img src="<?php echo base_url('assets/img/News/book-default.jpg');?>" width="100px"
                                    height="100px">
                                <?php } ?>
                            </td> -->
                            <td><?php echo $row->kd_news;?></td>
                            <td><?php echo $row->judul;?></td>
                            <td><?php echo $row->isi;?></td>
                            <td><?php echo $row->sumber;?></td>
                            <td><?php echo $row->tgl_rilis;?></td>
                        </tr>
                        <?php $no++; } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
			<div style="text-align: right;" id="diprint2">
				<p>Bandung, <?php echo date('d/m/Y') ?></p>
				<br><br><br><br>
				<p><?= $this->session->userdata('full_name'); ?></p>
			</div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-plugins/dataTables.bootstrap.min.js">
</script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-responsive/dataTables.responsive.js">
</script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>