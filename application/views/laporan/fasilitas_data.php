<style type="text/css">
    #diprint2 {
        opacity: 0;
        margin-top: -90px;
    }

    @media print {
        #diprint {
            display: none;
        }

        #diprint2 {
            opacity: 1;
            margin-top: 0px !important;
        }

        .minus2 {
            margin-top: -50px;
            margin-bottom: 30px;
        }

        body {
            font-size: 10.5px;
        }
    }
</style>
<div class="row">
    <div class="col-lg-12"><br />
        <ol class="breadcrumb">
            <li>Laporan</li>
            <li class="active">Data Fasilitas</li>
        </ol>

        <?php
            
            if(!empty($message)) {
                echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="row" id="diprint2">
            <div class="col-md-2" style="margin-left: 100px; margin-top: 30px;">
                <img src="<?= base_url('assets/img/profile/logo.png'); ?>" width="50px" height="50px">
            </div>
            <div class="col-md-6 minus2">
                <div class="mb-2" style="font-size: 20px; text-align: center;">Laporan Fasilitas</div>
                <div style="font-size: 15px; text-align: center;">E-Learning</div>
                <!-- <div style="font-size: 20px;">Telp. (022)-7326134</div> -->
            </div>
        </div>

        <div class="panel panel-default">

            <div class="panel-heading" id="diprint">
                <form action="<?php echo site_url('laporan/fasilitas'); ?>" method="get">
                <input type="text" name="cari" class="form-control" id="diprint" placeholder="Masukan Nama Fasilitas atau tahun beli.." value="<?php echo (isset($_GET['cari'])) ? $_GET['cari'] : ''; ?>" style="margin-bottom: 10px;">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" id="diprint">Cari Data</button>
                        <a href="<?php echo site_url('laporan/fasilitas'); ?>" class="btn btn-danger" id="diprint"
                            style="text-decoration:none; color: black;">Reset</a>
                            <button href="#" onclick="myFunction()" target="_blank" type="submit" id="diprint"
                        class="btn btn-primary diprint">Cetak data</button>
                    </div>
                </form>
            </div>

            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <td>No.</td>
                            <td>Image</td>
                            <td>Nama Fasilitas</td>
                            <td>Deskripsi</td>
                            <td>Tahun beli</td>
                            <td>Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                                $no = 1;
                                foreach($fasilitas->result() as $row) {
                                ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <!-- jika ada fasilitas di dalam database maka tampilkan -->
                            <td><?php if($row->image != "") { ?>
                                <img src="<?php echo base_url('assets/img/fasilitas/'.$row->image);?>" width="100px"
                                    height="100px">
                                <?php }else{ ?>
                                <img src="<?php echo base_url('assets/img/fasilitas/book-default.jpg');?>" width="100px"
                                    height="100px">
                                <?php } ?>
                            </td>
                            <td><?php echo $row->nm_fasilitas;?></td>
                            <td><?php echo $row->deskripsi;?></td>
                            <td><?php echo $row->thn_beli;?></td>
                            <td class="text-center">
                                <a href="<?php echo base_url('fasilitas/edit/'.$row->id_fsl) ?>"><input type="submit"
                                        class="btn btn-success btn-xs" name="edit" value="Edit"></a>
                                <a href="#" name="<?php echo $row->nm_fasilitas;?>" class="hapus btn btn-danger btn-xs"
                                    kode="<?php echo $row->id_fsl;?>">Hapus</a>
                            </td>
                        </tr>
                        <?php $no++; } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
			<div style="text-align: right;" id="diprint2">
				<p>Bandung, <?php echo date('d/m/Y') ?></p>
				<br><br><br><br>
				<p><?= $this->session->userdata('full_name'); ?></p>
			</div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-plugins/dataTables.bootstrap.min.js">
</script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-responsive/dataTables.responsive.js">
</script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>