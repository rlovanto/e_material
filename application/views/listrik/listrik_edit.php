<div class="row">
    <div class="col-lg-12"><br />

        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('listrik'); ?>">Listrik /Token</a></li>
            <li class="active">Edit Listrik /Token</li>
        </ol>

        <?php
            echo validation_errors();
            //buat message npm
            if(!empty($message)) {
            echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">

            <div class="panel-heading">
                Edit Listrik /Token
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <?php
                //validasi error upload
                if(!empty($error)) {
                    echo $error;
                }
            ?>
                <?php echo form_open_multipart('listrik/update', array('class' => 'form-horizontal', 'id' => 'jvalidate')) ?>
                
                <div class="form-group">
                    <p class="col-sm-2 text-left">Jenis Listrik </p>
                    <div class="col-sm-10">
                        <?php 
                            $jenis = array('Pascabayar' => 'Pascabayar', 'Token' => 'Token'); 
                            echo form_dropdown('jenis', $jenis, $edit['jenis'],"class='form-control'");    
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Id Pelanggan </p>

                    <div class="col-sm-10">
                        <input type="text" name="id_pelanggan" class="form-control" placeholder="Id Pelanggan"
                            value="<?php echo $edit['id_pelanggan']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Tempat </p>

                    <div class="col-sm-10">
                        <input type="text" name="tempat" class="form-control" placeholder="Tempat"
                            value="<?php echo $edit['tempat']; ?>">
                    </div>

                <input type="hidden" class="form-control" id="user_update" name="user_update"  value="<?= $this->session->userdata('full_name'); ?>" required>             
                <input name="update_date" type="hidden" id="update_date" value=" <?php echo date('Y-m-d'); ?> "readonly>


                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="btn-group pull-left">
                            <?php echo anchor('listrik', 'Cancel', array('class' => 'btn btn-danger btn-sm')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-group pull-right">
                            <input type="submit" name="update" value="Update" class="btn btn-success btn-sm">
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/bootstrap-datepicker.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/tinymce/tinymce.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>



<script type="text/javascript">
    tinymce.init({
        selector: 'textarea'
    });

    $(document).ready(function () {
        $("#tanggal1").datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#tanggal2").datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#tanggal").datepicker({
            format: 'yyyy-mm-dd'
        });
    })
</script>