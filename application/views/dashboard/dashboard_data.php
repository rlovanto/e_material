<div class="page-header resetIt">
    <h3>Dashboard</h3>
</div>
<div class="row">
    <div class="col-lg-12"><br />
        <?php
            if(!empty($message)) {
                echo $message;
            }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphiconfolder-open"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <font size="18"><b><?php echo $countbarang; ?></b></font>
                        </div>
                        <div><b>Jumlah Inventaris Barang</b></div>
                    </div>
                </div>
            </div>

            <?php 
                if($this->session->userdata['status'] == "Super Admin"){
            ?>
            <a href="<?php echo base_url().'barang' ?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-arrow-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
            <?php } ?>

        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphiconuser"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <font size="18"><b><?php echo $countcard; ?></b></font>
                        </div>
                        <div><b>Jumlah Kartu Gsm</b></div>
                    </div>
                </div>
            </div>
            <?php 
                if($this->session->userdata['status'] == "Super Admin"){
            ?>
            <a href="<?php echo base_url().'card' ?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-arrow-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
            <?php } ?>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphiconsort"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <font size="18"><b><?php echo $countmasuk; ?></b></font>
                        </div>
                        <div><b>Jumlah Barang Masuk</b></div>
                    </div>
                </div>
            </div>
            <?php 
                if($this->session->userdata['status'] == "Super Admin"){
            ?>
            <a href="<?php echo base_url().'masuks/all'; ?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-arrow-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
            <?php } ?>

        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphiconok"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <font size="18"><b><?php echo $countkeluar; ?></b></font>
                        </div>
                        <div><b>Jumlah Barang Keluar</b></div>
                    </div>
                </div>
            </div>
            <?php 
                if($this->session->userdata['status'] == "Super Admin"){
            ?>
            <a href="<?php echo base_url().'keluars/admin'; ?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-arrow-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
            <?php } ?>

        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px; font-weight: bold;"><i
                        class="glyphicon glyphicon-random arrow-right"></i> Data Barang</h3>
            </div>
            <div class="panel-body">
                <div class="list-group">
                    <?php 
                        $no=0;
                        foreach($barang->result() as $b){ 
                    ?>
                    <a href="#" class="list-group-item">
                        <!-- <span
                            class="badge"><?php if($b->status_buku == 1){echo "Tersedia";}else{echo "Dipinjam";}?></span> -->
                        <i class="glyphicon glyphiconuser"></i> <?php echo $b->nama_brg; ?>
                    </a>
                    <?php 
                        $no++;
                        if($no == 6) break;
                    } ?>
                </div>
                <?php 
                    if($this->session->userdata['status'] == "Super Admin"){
                ?>
                <div class="text-right">
                    <a href="<?php echo base_url().'barang' ?>">Lihat Data Barang <i
                            class="glyphicon glyphicon-arrow-right"></i></a>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px; font-weight: bold;"><i
                        class="glyphicon glyphicon-user o"></i>Kartu Gsm</h3>
            </div>
            <div class="panel-body">
                <div class="list-group">
                    <?php 
                        $no=0;
                        foreach($card->result() as $a){ 
                    ?>
                    <a href="#" class="list-group-item">
                        <span class="badge"><?php 
                            if($a->type );
                            // else echo "Pria"
                        ?></span>
                        <i class="glyphicon glyphiconuser"></i> <?php echo $a->kartu; ?>
                    </a>
                    <?php 
                        $no++;
                        if($no == 6) break;
                    } ?>
                </div>
                <?php 
                    if($this->session->userdata['status'] == "Super Admin"){
                ?>
                <div class="text-right">
                    <a href="<?php echo base_url().'card' ?>">Lihat Semua Kartu Gsm <i
                            class="glyphicon glyphicon-arrow-right"></i></a>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>

    <!-- <div class="col-lg-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title" style="font-size: 18px; font-weight: bold;"><i
                        class="glyphicon glyphicon-sort"></i> Pembelian Terakhir</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered tablehover table-striped dt-responsive">
                        <thead>
                            <tr>
                                <th>ID Transaksi</th>
                                <th>Supplier</th>
                                <th>Tgl. Masuk</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no=0;
								foreach($masuks->result() as $p){
							?>
                            <tr>
                                <td><?php echo date($p->kd_trans); ?></td>
                                <td><?php echo date($p->supplier); ?></td>
                                <td><?php echo date('d/m/Y',strtotime($p->tgl_masuk)); ?></td>
                                <td><?php echo date($p->jumlah); ?></td>
                                <td><?php echo "Rp.".number_format($p->harga)." ,-"; ?></td>
                            </tr>
                            <?php 
                                $no++; 
                                if($no == 6) break;
                            } ?>
                        </tbody>
                    </table>
                </div>
                <?php 
                    if($this->session->userdata['status'] == "Super Admin"){
                ?>
                <div class="text-right">
                    <a href="<?php echo base_url().'masuks' ?>">Lihat Semua Transaksi <i
                            class="glyphicon glyphicon-arrow-right"></i></a>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div> -->
<!-- /.row -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/morrisjs/morris.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>


<!-- test jquery -->
<script type="text/javascript">

    $(document).ready(function () {
        // alert('test jquery');
        
    });
</script>