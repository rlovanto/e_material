<div class="row">
    <div class="col-lg-12"><br />
       
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('kembali/create'); ?>">Pengembalian Barang</a></li>
            <li class="active">Create Pengembalian Barang</li>
        </ol>

        <?php
            echo validation_errors();
            //buat message npm
            if(!empty($message)) {
            echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">

            <div class="panel-heading">
                Create Pengembalian Barang
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <?php
                //validasi error upload
                if(!empty($error)) {
                    echo $error;
                }
            ?>
            <?php echo form_open_multipart('kembali/insert', array('class' => 'form-horizontal', 'id' => 'jvalidate')) ?>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Id Transaksi</p>

                    <div class="col-sm-10">
                        <input type="text" name="kd_trans" class="form-control" placeholder="Id Transaksi" value="<?= $kd_trans; ?>" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Id Pinjam </p>

                    <div class="col-sm-10">
                            <select name="reference" id="reference" class="form-control" onchange='changeValue(this.value)' required>
                                <option value="">-Pilih-</option>
                                    <?php
                                    
                                    $con = mysqli_connect("localhost", "root", "", "inventory_sensync");
                                    $query=mysqli_query($con, "select * from pinjam order by kd_trans asc"); 
                                    $result = mysqli_query($con, "select * from pinjam");  
                                    $jsArray = "var prdName = new Array();\n";
                                    while ($row = mysqli_fetch_array($result)) {  
                                    echo '<option name="kd_trans"  value="' . $row['kd_trans'] . '">' . $row['kd_trans'] . '</option>';  
                                    $jsArray .= "prdName['" . $row['kd_trans'] . "'] = {nama_brg:'" . addslashes($row['nama_brg']) . "',jumlah:'".addslashes($row['jumlah'])."',harga:'".addslashes($row['harga'])."'};\n";
                                    }
                                    ?>
                            </select>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Jumlah yg dipinjam </p>
                    <div class="col-sm-10">
                        <input type="text" name="jumlah_k" id="jumlah" placeholder="Jumlah yang di pinjam" class="form-control" value="<?php echo set_value('jumlah_k'); ?>">
                    </div>
                </div>


                <div class="form-group">
                    <p class="col-sm-2 text-left">Nama Barang</p>
                    <div class="col-sm-10">
                        <input type="text" name="nama_brg" id="nama_brg" class="form-control"readonly placeholder="Nama Barang" value="<?php echo set_value('nama_brg'); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <p class="col-sm-2 text-left">Tgl Pengembalian </p>

                    <div class="col-sm-10">
                        <input type="date" name="tgl_kembali" class="form-control" placeholder="Tanggal Pengembalian" value="<?php echo set_value('tgl_kembali'); ?>">
                    </div>
                </div>

                <input type="hidden" class="form-control" id="user_create" name="user_create" value="<?= $this->session->userdata('full_name'); ?>" required>
                <input name="create_date" type="hidden" id="create_date" value=" <?php echo date('Y-m-d'); ?> " readonly>

                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="btn-group pull-left">
                            <?php echo anchor('kembali', 'Cancel', array('class' => 'btn btn-danger btn-sm')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-group pull-right">
                            <input type="submit" name="save" value="Save" class="btn btn-success btn-sm">
                        </div>
                    </div>
                </div>

            <?php echo form_close(); ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/bootstrap-datepicker.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/tinymce/tinymce.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>



<script type="text/javascript">

tinymce.init({selector:'textarea'});

$(document).ready(function() {
    $("#tanggal1").datepicker({
        format:'yyyy-mm-dd'
    });
    
    $("#tanggal2").datepicker({
        format:'yyyy-mm-dd'
    });
    
    $("#tanggal").datepicker({
        format:'yyyy-mm-dd'
    });
})



</script>
<script type="text/javascript"> 
<?php echo $jsArray; ?>
function changeValue(id){
    document.getElementById('jumlah').value = prdName[id].jumlah;
    document.getElementById('nama_brg').value = prdName[id].nama_brg;
    // document.getElementById('jumlah').value = prdName[id].jumlah;
};
</script>

 
