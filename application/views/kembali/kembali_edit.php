<div class="row">
    <div class="col-lg-12"><br />

        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('kembali'); ?>">Pengembalian Barang</a></li>
            <li class="active">Edit Pengembalian Barang</li>
        </ol>

        <?php
            echo validation_errors();
            //buat message npm
            if(!empty($message)) {
            echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">

            <div class="panel-heading">
                Edit Pengembalian Barang
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <?php
                //validasi error upload
                if(!empty($error)) {
                    echo $error;
                }
            ?>
                <?php echo form_open_multipart('kembali/update', array('class' => 'form-horizontal', 'id' => 'jvalidate')) ?>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Id Transaksi </p>

                    <div class="col-sm-10">
                        <input type="text" name="kd_trans" class="form-control" placeholder="Id Transaksi"
                            value="<?php echo $edit['kd_trans']; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Id Pinjam </p>

                    <div class="col-sm-10">
                            <select name="reference" id="reference" class="form-control" onchange='changeValue(this.value)' required>
                                <option value="<?php echo $edit['reference']; ?>">-Pilih-</option>
                                    <?php
                                    
                                    $con = mysqli_connect("localhost", "root", "", "inventory_sensync");
                                    $query=mysqli_query($con, "select * from pinjam order by kd_trans asc"); 
                                    $result = mysqli_query($con, "select * from pinjam");  
                                    $jsArray = "var prdName = new Array();\n";
                                    while ($row = mysqli_fetch_array($result)) {  
                                    echo '<option name="kd_trans"  value="' . $row['kd_trans'] . '">' . $row['kd_trans'] . '</option>';  
                                    $jsArray .= "prdName['" . $row['kd_trans'] . "'] = {nama_brg:'" . addslashes($row['nama_brg']) . "',jumlah:'".addslashes($row['jumlah'])."',harga:'".addslashes($row['harga'])."'};\n";
                                    }
                                    ?>
                            </select>
                    </div>
                </div>
                

                <div class="form-group">
                    <p class="col-sm-2 text-left">Jumlah yg dipinjam </p>
                    <div class="col-sm-10">
                        <input type="text" name="jumlah" class="form-control" placeholder="Jumlah yg dipinjam"
                            value="<?php echo $edit['jumlah']; ?>" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Nama Barang </p>
                    <div class="col-sm-10">
                        <input type="text" name="nama_brg" class="form-control" placeholder="Nama Barang"
                            value="<?php echo $edit['nama_brg']; ?>" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Tgl Pengembalian </p>
                    <div class="col-sm-10">
                        <input type="date" name="tgl_kembali" class="form-control" placeholder="Tgl Pengembalian"
                            value="<?php echo $edit['tgl_kembali']; ?>">
                    </div>
                </div>

                <input type="hidden" class="form-control" id="user_update" name="user_update"  value="<?= $this->session->userdata('full_name'); ?>" required>             
                <input name="update_date" type="hidden" id="update_date" value=" <?php echo date('Y-m-d'); ?> "readonly>

                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="btn-group pull-left">
                            <?php echo anchor('kembali', 'Cancel', array('class' => 'btn btn-danger btn-sm')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-group pull-right">
                            <input type="submit" name="update" value="Update" class="btn btn-success btn-sm">
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/bootstrap-datepicker.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/tinymce/tinymce.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>



<script type="text/javascript">
    tinymce.init({
        selector: 'textarea'
    });

    $(document).ready(function () {
        $("#tanggal1").datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#tanggal2").datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#tanggal").datepicker({
            format: 'yyyy-mm-dd'
        });
    })
</script>
<script type="text/javascript"> 
<?php echo $jsArray; ?>
function changeValue(id){
    document.getElementById('jumlah').value = prdName[id].jumlah;
    document.getElementById('nama_brg').value = prdName[id].nama_brg;
    // document.getElementById('jumlah').value = prdName[id].jumlah;
};
</script>
