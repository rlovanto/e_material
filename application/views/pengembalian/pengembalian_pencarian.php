<?php if($pencarian->num_rows() > 0) { ?>
<br /><br />
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>No. Transaksi</td>
            <td>Id Barang</td>
            <td>Tgl. Peminjaman</td>
            <td>Jumlah Pinjam</td>
            <td></td>
        </tr>
    </thead>
    <?php foreach($pencarian->result() as $row):?>
    <tr>
        <td><?php echo $row->id_transaksi;?></td>
        <td><?php echo $row->id_brg;?></td>
        <td><?php echo $row->tanggal_pinjam;?></td>
        <td><?php echo $row->jumlah_x;?></td>
        <td><a href="#" class="tambahkan" no_transaksi="<?php echo $row->id_transaksi;?>">
            <i class="glyphicon glyphicon-plus"></i>
        </a></td>
    </tr>
    <?php endforeach;?>
</table>
<?php }else{ ?>
<br />
<strong>Id Barang Not Found</strong>

<?php } ?>
