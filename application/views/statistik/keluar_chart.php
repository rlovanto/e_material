<br>
<?php echo anchor('statistik', 'Back', array('class' => 'btn btn-warning btn-sm')); ?>

<!-- load library jquery dan highcharts -->
<script src="<?php echo base_url();?>assets/js/jquery.js"></script>
<script src="<?php echo base_url();?>assets/js/highcharts.js"></script>
<!-- end load library -->

<?php
    /* Mengambil query graph*/
    foreach($graph as $result){
        $total[] = $result->total; //ambil total
        $tanggal_keluar[] = $result->tanggal_keluar;
        $value[] = (float) $result->total; //ambil total
    }
    /* end mengambil query*/
?>

<!-- Load chart dengan menggunakan ID -->
<div id="graph"></div>
<!-- END load chart -->

<!-- Script untuk memanggil library Highcharts -->
<script type="text/javascript">
    $(function () {
        $('#graph').highcharts({
            chart: {
                type: 'line',
                margin: 75,
                options3d: {
                    enabled: false,
                    alpha: 10,
                    beta: 25,
                    depth: 70
                }
            },
            title: {
                text: 'Report Pertumbuhan Jumlah barang keluar 1 Bulan Terakhir',
                style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: <?php echo json_encode($tanggal_keluar); ?>
            },
            exporting: {
                enabled: false
            },
            yAxis: {
                title: {
                    text: 'Jumlah'
                },
            },
            tooltip: {
                formatter: function () {
                    return 'Pada tanggal <b>' + this.x + '</b> terdapat <b>' + Highcharts
                        .numberFormat(this.y, 0) + '</b> Jumlah barang keluar baru.';
                }
            },
            series: [{
                name: 'Report Pertumbuhan Jumlah barang keluar',
                data: <?php echo json_encode($value); ?> ,
                shadow : true,
                dataLabels: {
                    enabled: true,
                    color: '#045396',
                    align: 'center',
                    formatter: function () {
                        return Highcharts.numberFormat(this.y, 0);
                    },
                    y: 0,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });
</script>