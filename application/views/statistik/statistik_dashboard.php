<div class="page-header resetIt">
    <h3>Statistik Fluktuatif Data</h3>
</div>

<div class="row">
    <div class="col-lg-12"><br />
        <?php
            if(!empty($message)) {
                echo $message;
            }
        ?>
    </div>
</div>

<!-- <?php 
    if($this->session->userdata['status'] == "Super Admin" || $this->session->userdata['status'] == "Admin"){
?> -->
<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphiconfolder-open"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <font size="18"><b><?php echo $countmasuk; ?></b></font>
                        </div>
                        <div><b>Total Barang Masuk</b></div>
                    </div>
                </div>
            </div>
            <a href="<?php echo base_url().'statistik/graph_masuk' ?>">
                <div class="panel-footer">
                <span class="pull-left">Lihat Statistik Bulan Ini</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-arrow-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphiconuser"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <font size="18"><b><?php echo $countkeluar; ?></b></font>
                        </div>
                        <div><b>Total Barang Keluar</b></div>
                    </div>
                </div>
            </div>
            <a href="<?php echo base_url().'statistik/graph_keluar' ?>">
                <div class="panel-footer">
                    <span class="pull-left">Lihat Statistik Bulan Ini</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-arrow-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphiconuser"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <font size="18"><b><?php echo $countpinjam; ?></b></font>
                        </div>
                        <div><b>Total Barang Dipinjam </b></div>
                    </div>
                </div>
            </div>
            <a href="<?php echo base_url().'statistik/graph_pinjam' ?>">
                <div class="panel-footer">
                    <span class="pull-left">Lihat Statistik Bulan Ini</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-arrow-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphiconsort"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <font size="18"><b><?php echo $countkembali; ?></b></font>
                        </div>
                        <div><b>Total Barang yang Dikembalikan</b></div>
                    </div>
                </div>
            </div>
            <a href="<?php echo base_url().'statistik/graph_kembali' ?>">
                <div class="panel-footer">
                <span class="pull-left">Lihat Statistik Bulan Ini</span>
                    <span class="pull-right"><i class="glyphicon glyphicon-arrow-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>

<hr>

<hr>

</div>
<!-- <?php } ?> -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>
<!-- Morris Charts JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/morrisjs/morris.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/data/morris-data.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>