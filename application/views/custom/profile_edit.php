<div class="row">
    <div class="col-lg-12"><br />
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('dashboard/custom_profile_edit/'.$edit['id']); ?>">Profile</a></li>
            <li class="active">Edit Profile</li>
        </ol>
        <?php
            echo validation_errors();
            if(!empty($message)) {
            echo $message;
            }
        ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Edit Profile
            </div>
            <div class="panel-body">
                <?php
                if(!empty($error)) {
                    echo $error;
                } ?>

                <?php echo form_open_multipart('dashboard/custom_update_profile', array('class' => 'form-horizontal', 'id' => 'jvalidate')) ?>
                <div class="form-group">
                    <p class="col-sm-2 text-left">ID </p>
                    <div class="col-sm-10">
                        <input type="text" name="id" class="form-control" placeholder="ID" id="id" value="<?php echo $edit['id']; 
                        ?>" readonly="readonly">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Name </p>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" placeholder="Name" id="name"
                            value="<?php echo $edit['name'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Description </p>
                    <div class="col-sm-10">
                        <input type="text" name="deskripsi" class="form-control" placeholder="Deskripsi"
                            value="<?php echo $edit['deskripsi'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Visi </p>
                    <div class="col-sm-10">
                        <input type="text" name="visi" class="form-control" placeholder="Visi" id="visi"
                            value="<?php echo $edit['visi'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Tugas dan Fungsi </p>
                    <div class="col-sm-10">
                        <input type="text" name="fungsiTugas" class="form-control" placeholder="Tugas dan Fungsi"
                        id="fungsiTugas" value="<?php echo $edit['fungsiTugas']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Fasilitas </p>
                    <div class="col-sm-10">
                        <input type="text" name="fasilitas" class="form-control" placeholder="Fasilitas" id="fasilitas"
                            value="<?php echo $edit['fasilitas'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Images</p>
                    <div class="col-sm-10">
                        <?php if($edit['image'] != '') { ?>
                        <img src="<?php echo base_url('assets/img/profile/'.$edit['image']);?>" width="100px"
                            height="100px">
                        <?php }else{ ?>
                        <img src="<?php echo base_url('assets/img/profile/profile.jpg');?>" width="100px" height="100px">
                        <?php } ?> <br /><br />
                        <input type="file" name="userfile" class="form-control btn-file"
                            value="<?php echo set_value('userfile'); ?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="btn-group pull-left">
                            <?php echo anchor('dashboard/custom_profile', 'Cancel', array('class' => 'btn btn-danger btn-sm')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-group pull-right">
                            <input type="submit" name="update" value="Update" class="btn btn-success btn-sm">
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/bootstrap-datepicker.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>