<div class="row">
    <div class="col-lg-12"><br />
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('dashboard/custom_profile'); ?>">Custom Homepage</a></li>
            <li class="active">Profile</li>
        </ol>
        <?php
            if(!empty($message)) {
                echo $message;
            }
        ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <br><br>
        <?php foreach($profile->result() as $row) { ?>
        <?php 
            if($row->image != "") { ?>
        <img src="<?php echo base_url('assets/img/profile/'.$row->image);?>" width="200px" height="200px">
        <?php } else { ?>
        <img src="<?php echo base_url('assets/img/profile/profile.jpg');?>" width="200px" height="200px">
        <?php } 
        ?>
        <h5>Nama Perusahaan: </h5> <?php echo $row->name;?>
        <h5>Deskripsi: </h5><?php echo $row->deskripsi;?>
        <h5>Tugas dan Tujuan: </h5><?php echo $row->fungsiTugas;?>
        <h5>Fasilitas: </h5><?php echo $row->fasilitas;?>
        <h5>Visi:</h5><?php echo $row->visi;?>
        <br><br>
        <a href="<?php echo base_url('dashboard/custom_profile_edit/'.$row->id) ?>"><input type="submit" class="btn btn-success btn-xs"
                name="edit" value="Edit Profil"></a>
        <?php } ?> <br><br>
        <?php echo anchor('anggota/create', 'Add New Misi', array('class' => 'btn btn-primary btn-sm')); ?>
        <h5>Misi:</h5>
        <?php 
        $no = 1; 
        foreach($misi->result() as $row) { ?>
        <div class="m-1">
            <?php echo $no;?>. <?php echo $row->misi;?>
            <a href="<?php echo base_url('anggota/edit/'.$row->id) ?>"><input type="submit"
                    class="btn btn-success btn-xs" name="edit" value="Edit"></a>
            <a href="#" name="<?php echo $row->misi;?>" class="hapus btn btn-danger btn-xs"
                kode="<?php echo $row->id;?>">Hapus</a>
        </div>
        <br>
        <?php $no++; } ?>
        <br><br>
    </div>
</div>
</div>