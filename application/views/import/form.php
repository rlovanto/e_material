<html>
<head>
	<title>Form Import</title>

	<!-- Load File jquery.min.js yang ada difolder js -->
	<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>

	<script>
	$(document).ready(function(){
		// Sembunyikan alert validasi kosong
		$("#kosong").hide();
	});
	</script>
</head>
<body>
	<h3>Form Import</h3>
	<hr>
	<div class="row">
    	<div class="col-lg-12">
			<a href="<?php echo base_url("excel/format.xlsx"); ?>">Download Format</a>
			<!-- <?php echo anchor('excel/format.xlsx', 'Download Format', array('class' => 'btn btn-primary btn-sm')); ?> -->

			<br>
			<br>

			<!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
			<form method="post" action="<?php echo base_url("barang/form"); ?>" enctype="multipart/form-data">
				<!--
				-- Buat sebuah input type file
				-- class pull-left berfungsi agar file input berada di sebelah kiri
				-->
				<input type="file" name="file">
			<br/>
				<!--
				-- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
				-->
				<input type="submit" class="btn btn-primary btn-sm" name="preview" value="Preview">
			</form>
		</div>
	</div>
	<?php
	if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
		if(isset($upload_error)){ // Jika proses upload gagal
			echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
			die; // stop skrip
		}

		// Buat sebuah tag form untuk proses import data ke database
		echo "<form method='post' action='".base_url("barang/import")."'>";

		// Buat sebuah div untuk alert validasi kosong
		echo "<div style='color: red;' id='kosong'>
		Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
		</div>";

		echo "<table border='1' cellpadding='8'>
		<tr>
			<th colspan='6'>Preview Data</th>
		</tr>
		<tr>
			<th>Nama Barang</th>
			<th>Jenis</th>
			<th>Satuan</th>
			<th>Stock</th>
			<th>No Box</th>
			<th>Kategori</th>
			<th>Catatan</th>
		</tr>";

		$numrow = 1;
		$kosong = 0;

		// Lakukan perulangan dari data yang ada di excel
		// $sheet adalah variabel yang dikirim dari controller
		foreach($sheet as $row){
			// Ambil data pada excel sesuai Kolom
			$nama_brg = $row['A']; // Ambil data NIS
			$jenis = $row['B']; // Ambil data jenis
			$satuan = $row['C']; // Ambil data jenis kelamin
			$stock = $row['D']; // Ambil data stock
			$nomer_box = $row['E']; // Ambil data stock
			$kategori = $row['F']; // Ambil data stock
			$catatan = $row['G']; // Ambil data stock

			// Cek jika semua data tidak diisi
			if($nama_brg == "" && $jenis == "" && $satuan == "" && $stock == "" && $nomer_box == ""&& $kategori == ""&& $catatan == "")
				continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah jenis-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Validasi apakah semua data telah diisi
				$nama_brg_td = ( ! empty($nama_brg))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
				$jenis_td = ( ! empty($jenis))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
				$satuan_td = ( ! empty($satuan))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
				$stock_td = ( ! empty($stock))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$nomer_box_td = ( ! empty($nomer_box))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$kategori_td = ( ! empty($kategori))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$catatan_td = ( ! empty($catatan))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				// Jika salah satu data ada yang kosong
				if($nama_brg == "" or $jenis == "" or $satuan == "" or $stock == ""or $nomer_box == ""or $kategori == "" or $catatan == ""){
					$kosong++; // Tambah 1 variabel $kosong
				}

				echo "<tr>";
				echo "<td".$nama_brg_td.">".$nama_brg."</td>";
				echo "<td".$jenis_td.">".$jenis."</td>";
				echo "<td".$satuan_td.">".$satuan."</td>";
				echo "<td".$stock_td.">".$stock."</td>";
				echo "<td".$nomer_box_td.">".$nomer_box."</td>";
				echo "<td".$kategori_td.">".$kategori."</td>";
				echo "<td".$catatan_td.">".$catatan."</td>";
				echo "</tr>";
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		echo "</table>";

		// Cek apakah variabel kosong lebih dari 0
		// Jika lebih dari 0, berarti ada data yang masih kosong
		if($kosong > 0){
		?>
			<script>
			$(document).ready(function(){
				// Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
				$("#jumlah_kosong").html('<?php echo $kosong; ?>');

				$("#kosong").show(); // Munculkan alert validasi kosong
			});
			</script>
		<?php
		}else{ // Jika semua data sudah diisi
			echo "<hr>";

			// Buat sebuah tombol untuk mengimport data ke database
			echo "<button type='submit' name='import'>Import</button>";
			echo "<a href='".base_url("barang")."'>Cancel</a>";
		}

		echo "</form>";
	}
	?>
</body>
</html>
