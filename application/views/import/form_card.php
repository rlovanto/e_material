<html>
<head>
	<title>Form Import</title>

	<!-- Load File jquery.min.js yang ada difolder js -->
	<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>

	<script>
	$(document).ready(function(){
		// Sembunyikan alert validasi kosong
		$("#kosong").hide();
	});
	</script>
</head>
<body>
	<h3>Form Import</h3>
	<hr>
	<div class="row">
    	<div class="col-lg-12">
			<a href="<?php echo base_url("excel/format_card.xlsx"); ?>">Download Format</a>
			<!-- <?php echo anchor('excel/format.xlsx', 'Download Format', array('class' => 'btn btn-primary btn-sm')); ?> -->

			<br>
			<br>

			<!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
			<form method="post" action="<?php echo base_url("card/form"); ?>" enctype="multipart/form-data">
				<!--
				-- Buat sebuah input type file
				-- class pull-left berfungsi agar file input berada di sebelah kiri
				-->
				<input type="file" name="file">
			<br/>
				<!--
				-- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
				-->
				<input type="submit" class="btn btn-primary btn-sm" name="preview" value="Preview">
			</form>
		</div>
	</div>
	<?php
	if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
		if(isset($upload_error)){ // Jika proses upload gagal
			echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
			die; // stop skrip
		}

		// Buat sebuah tag form untuk proses import data ke database
		echo "<form method='post' action='".base_url("card/import")."'>";

		// Buat sebuah div untuk alert validasi kosong
		echo "<div style='color: red;' id='kosong'>
		Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
		</div>";

		echo "<table border='1' cellpadding='8'>
		<tr>
			<th colspan='6'>Preview Data</th>
		</tr>
		<tr>
			<th>Masa Aktif</th>
			<th>Type</th>
			<th>Kartu</th>
			<th>Nomor</th>
			<th>Tempat</th>
			<th>PIC</th>
		</tr>";

		$numrow = 1;
		$kosong = 0;

		// Lakukan perulangan dari data yang ada di excel
		// $sheet adalah variabel yang dikirim dari controller
		foreach($sheet as $row){
			// Ambil data pada excel sesuai Kolom
			$waktu = $row['A']; // Ambil data NIS
			$type = $row['B']; // Ambil data type
			$kartu = $row['C']; // Ambil data type kelamin
			$nomor = $row['D']; // Ambil data nomor
			$tempat = $row['E']; // Ambil data nomor
			$pic_cont = $row['F']; // Ambil data nomor

			// Cek jika semua data tidak diisi
			if($waktu == "" && $type == "" && $kartu == "" && $nomor == "" && $tempat == ""&& $pic_cont == "")
				continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah type-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Validasi apakah semua data telah diisi
				$waktu_td = ( ! empty($waktu))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
				$type_td = ( ! empty($type))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
				$kartu_td = ( ! empty($kartu))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
				$nomor_td = ( ! empty($nomor))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$tempat_td = ( ! empty($tempat))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$pic_cont_td = ( ! empty($pic_cont))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				// Jika salah satu data ada yang kosong
				if($waktu == "" or $type == "" or $kartu == "" or $nomor == ""or $tempat == ""or $pic_cont == ""){
					$kosong++; // Tambah 1 variabel $kosong
				}

				echo "<tr>";
				echo "<td".$waktu_td.">".$waktu."</td>";
				echo "<td".$type_td.">".$type."</td>";
				echo "<td".$kartu_td.">".$kartu."</td>";
				echo "<td".$nomor_td.">".$nomor."</td>";
				echo "<td".$tempat_td.">".$tempat."</td>";
				echo "<td".$pic_cont_td.">".$pic_cont."</td>";
				echo "</tr>";
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		echo "</table>";

		// Cek apakah variabel kosong lebih dari 0
		// Jika lebih dari 0, berarti ada data yang masih kosong
		if($kosong > 0){
		?>
			<script>
			$(document).ready(function(){
				// Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
				$("#jumlah_kosong").html('<?php echo $kosong; ?>');

				$("#kosong").show(); // Munculkan alert validasi kosong
			});
			</script>
		<?php
		}else{ // Jika semua data sudah diisi
			echo "<hr>";

			// Buat sebuah tombol untuk mengimport data ke database
			echo "<button type='submit' name='import'>Import</button>";
			echo "<a href='".base_url("card")."'>Cancel</a>";
		}

		echo "</form>";
	}
	?>
</body>
</html>
