<html>
<head>
	<title>Form Import</title>

	<!-- Load File jquery.min.js yang ada difolder js -->
	<script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>

	<script>
	$(document).ready(function(){
		// Sembunyikan alert validasi kosong
		$("#kosong").hide();
	});
	</script>
</head>
<body>
	<h3>Form Import</h3>
	<hr>
	<div class="row">
    	<div class="col-lg-12">
			<a href="<?php echo base_url("excel/format_isp.xlsx"); ?>">Download Format</a>
			<!-- <?php echo anchor('excel/format.xlsx', 'Download Format', array('class' => 'btn btn-primary btn-sm')); ?> -->

			<br>
			<br>

			<!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
			<form method="post" action="<?php echo base_url("isp/form"); ?>" enctype="multipart/form-data">
				<!--
				-- Buat sebuah input type file
				-- class pull-left berfungsi agar file input berada di sebelah kiri
				-->
				<input type="file" name="file">
			<br/>
				<!--
				-- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
				-->
				<input type="submit" class="btn btn-primary btn-sm" name="preview" value="Preview">
			</form>
		</div>
	</div>
	<?php
	if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
		if(isset($upload_error)){ // Jika proses upload gagal
			echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
			die; // stop skrip
		}

		// Buat sebuah tag form untuk proses import data ke database
		echo "<form method='post' action='".base_url("isp/import")."'>";

		// Buat sebuah div untuk alert validasi kosong
		echo "<div style='color: red;' id='kosong'>
		Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
		</div>";

		echo "<table border='1' cellpadding='8'>
		<tr>
			<th colspan='6'>Preview Data</th>
		</tr>
		<tr>
			<th>Id Isp</th>
			<th>Provider Isp</th>
			<th>Nomor</th>
			<th>Site</th>
			<th>Bandwith</th>
		</tr>";

		$numrow = 1;
		$kosong = 0;

		// Lakukan perulangan dari data yang ada di excel
		// $sheet adalah variabel yang dikirim dari controller
		foreach($sheet as $row){
			// Ambil data pada excel sesuai Kolom
			$id_isp = $row['A']; // Ambil data NIS
			$nama_isp = $row['B']; // Ambil data nama_isp
			$nomor = $row['C']; // Ambil data nama_isp kelamin
			$site = $row['D']; // Ambil data site
			$bandwith = $row['E']; // Ambil data site

			// Cek jika semua data tidak diisi
			if($id_isp == "" && $nama_isp == "" && $nomor == "" && $site == "" && $bandwith == "")
				continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama_isp-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Validasi apakah semua data telah diisi
				$is_isp_td = ( ! empty($id_isp))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
				$nama_isp_td = ( ! empty($nama_isp))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
				$nomor_td = ( ! empty($nomor))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
				$site_td = ( ! empty($site))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				$bandwith_td = ( ! empty($bandwith))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
				// Jika salah satu data ada yang kosong
				if($id_isp == "" or $nama_isp == "" or $site == "" or $site == ""or $bandwith == ""){
					$kosong++; // Tambah 1 variabel $kosong
				}

				echo "<tr>";
				echo "<td".$is_isp_td.">".$id_isp."</td>";
				echo "<td".$nama_isp_td.">".$nama_isp."</td>";
				echo "<td".$nomor_td.">".$nomor."</td>";
				echo "<td".$site_td.">".$site."</td>";
				echo "<td".$bandwith_td.">".$bandwith."</td>";
				echo "</tr>";
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		echo "</table>";

		// Cek apakah variabel kosong lebih dari 0
		// Jika lebih dari 0, berarti ada data yang masih kosong
		if($kosong > 0){
		?>
			<script>
			$(document).ready(function(){
				// Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
				$("#jumlah_kosong").html('<?php echo $kosong; ?>');

				$("#kosong").show(); // Munculkan alert validasi kosong
			});
			</script>
		<?php
		}else{ // Jika semua data sudah diisi
			echo "<hr>";

			// Buat sebuah tombol untuk mengimport data ke database
			echo "<button type='submit' name='import'>Import</button>";
			echo "<a href='".base_url("isp")."'>Cancel</a>";
		}

		echo "</form>";
	}
	?>
</body>
</html>
