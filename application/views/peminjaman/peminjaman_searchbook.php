<?php if($barang->num_rows() > 0) { ?>
<br /><br />
<table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Kode Barang</td>
                <td>Nama Barang</td>
                <td>Satuan</td>
                <td>Stock</td>
                <td></td>
            </tr>
        </thead>
        <?php foreach($barang->result() as $data):?>
        <tr>
            <td><?php echo $data->id_brg;?></td>
            <td><?php echo $data->nama_brg;?></td>
            <td><?php echo $data->satuan;?></td>
            <td><?php echo $data->stock;?></td>
            <td><a href="#" class="tambah" 
                kode="<?php echo $data->id_brg;?>"
                nama_brg="<?php echo $data->nama_brg;?>"
                satuan="<?php echo $data->satuan;?>"
                stock="<?php echo $data->stock;?>">
                <i class="glyphicon glyphicon-plus"></i></a></td>
        </tr>
        <?php endforeach;?>
    </table>
<?php }else{ ?>
<br />
<strong>Barang Not Found</strong>

<?php } ?>
