<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>Id Barang</td>
            <td>Nama Barang</td>
            <td>Satuan</td>
            <td>Jumlah</td>
            <td>stock</td>
            <td></td>
        </tr>
    </thead>
    <?php foreach($tmp as $tmp):?>
    <tr>
        <td><?php echo $tmp->id_brg;?></td>
        <td><?php echo $tmp->nama_brg;?></td>
        <td><?php echo $tmp->satuan;?></td>
        <td><?php echo $tmp->jumlah_x;?></td>
        <td><?php echo $tmp->stock;?></td>
        </td>
        <td><a href="#" class="hapus" kode="<?php echo $tmp->id_brg;?>"><i class="glyphicon glyphicon-trash"></i></a></td>
    </tr>
    <?php endforeach;?>
    <tr>
        <td colspan="2">Total Barang Dipinjam</td>
        <td colspan="2"><input type="text" id="jumlahTmp" readonly="readonly" value="<?php echo $jumlahTmp;?>" class="form-control"></td>
    </tr>
</table>