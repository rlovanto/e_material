<div class="row">
    <div class="col-lg-12"><br />

        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('keluar'); ?>">Barang Keluar</a></li>
            <li class="active">Data Barang Keluar</li>
        </ol>

        <?php
            
            if(!empty($message)) {
                echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <?php echo anchor('keluar/create', 'Add', array('class' => 'btn btn-primary btn-sm')); ?>
        <br /><br />
        <div class="panel panel-default">

            <div class="panel-heading">
                Data Barang Keluar
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <td>No.</td>
                            <td>ID Transaksi</td>
                            <td>Nama barang</td>
                            <td>Tgl keluar</td>
                            <td>Jumlah</td>
                            <td>Proyek</td>
                            <td>User Submit</td>
                            <td>Tanggal Submit</td>
                            <td>User Update</td>
                            <td>Tanggal Update</td>
                            <td>Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                                $no = 1;
                                foreach($keluar->result() as $row) {
                                ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $row->kd_trans;?></td>
                            <td><?php echo $row->nama_brg;?></td>
                            <td><?php echo $row->tgl_keluar;?></td>
                            <td><?php echo $row->jumlah;?></td>
                            <td><?php echo $row->proyek;?></td>
                            <td><?php echo $row->user_create;?></td>
                            <td><?php echo $row->create_date;?></td>
                            <td><?php echo $row->user_update;?></td>
                            <td><?php echo $row->update_date;?></td>
                            <td class="text-center">
                                <a href="<?php echo base_url('keluar/edit/'.$row->kd_trans) ?>"><input
                                        type="submit" class="btn btn-success btn-xs" name="edit" value="Edit"></a>
                                <a href="#" name="<?php echo $row->kd_trans;?>" class="hapus btn btn-danger btn-xs"
                                    kode="<?php echo $row->kd_trans;?>">Hapus</a>
                            </td>
                        </tr>
                        <?php $no++; } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- Modal Hapus-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="idhapus" id="idhapus">
                <p>Apakah anda yakin ingin menghapus keluar <strong class="text-konfirmasi"> </strong> ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-xs" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger btn-xs" id="konfirmasi">Hapus</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-plugins/dataTables.bootstrap.min.js">
</script>
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/datatables-responsive/dataTables.responsive.js">
</script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>

<script type="text/javascript">
    // function confirmDelete()
    // {
    //     return confirm("Apakah anda yakin ingin menghapus data anggota")
    // }

    $(function () {
        $(".hapus").click(function () {
            var kode = $(this).attr("kode");
            var name = $(this).attr("name");

            $(".text-konfirmasi").text(name)
            $("#idhapus").val(kode);
            $("#myModal").modal("show");
        });

        $("#konfirmasi").click(function () {
            var kode = $("#idhapus").val();

            $.ajax({
                url: "<?php echo site_url('keluar/delete');?>",
                type: "POST",
                data: "kd_trans=" + kode,
                cache: false,
                success: function (html) {
                    location.href =
                        "<?php echo site_url('keluar/index/delete-success');?>";
                }
            });
        });
    });
</script>