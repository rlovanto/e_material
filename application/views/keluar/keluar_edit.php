<div class="row">
    <div class="col-lg-12"><br />

        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('keluar'); ?>">Barang Keluar</a></li>
            <li class="active">Edit Barang Keluar</li>
        </ol>

        <?php
            echo validation_errors();
            //buat message npm
            if(!empty($message)) {
            echo $message;
            }
        ?>

    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">

            <div class="panel-heading">
                Edit Barang Keluar
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <?php
                //validasi error upload
                if(!empty($error)) {
                    echo $error;
                }
            ?>
                <?php echo form_open_multipart('keluar/update', array('class' => 'form-horizontal', 'id' => 'jvalidate')) ?>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Id Transaksi </p>

                    <div class="col-sm-10">
                        <input type="text" name="kd_trans" class="form-control" placeholder="Nama keluar"
                            value="<?php echo $edit['kd_trans']; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Nama Barang </p>

                    <div class="col-sm-10">
                            <select name="nama_brg" id="nama_brg" class="form-control" onchange='changeValue(this.value)' required>
                                <option value="<?php echo $edit['nama_brg']; ?>">-Pilih-</option>
                                    <?php
                                    
                                    $con = mysqli_connect("localhost", "root", "", "inventory_sensync");
                                    $query=mysqli_query($con, "select * from barangx order by nama_brg asc"); 
                                    $result = mysqli_query($con, "select * from barangx");  
                                    $jsArray = "var prdName = new Array();\n";
                                    while ($row = mysqli_fetch_array($result)) {  
                                    echo '<option name="nama_brg"  value="' . $row['nama_brg'] . '">' . $row['nama_brg'] . '</option>';  
                                    $jsArray .= "prdName['" . $row['nama_brg'] . "'] = {stock:'" . addslashes($row['stock']) . "',jumlah:'".addslashes($row['jumlah'])."',harga:'".addslashes($row['harga'])."'};\n";
                                    }
                                    ?>
                            </select>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Stock Saat Ini </p>
                    <div class="col-sm-10">
                        <input type="text" name="stock" id="stock" class="form-control"readonly>
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Tgl  Keluar Barang </p>
                    <div class="col-sm-10">
                        <input type="date" name="tgl_keluar" class="form-control" placeholder="Tgl  Keluar Barang"
                            value="<?php echo $edit['tgl_keluar']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Jumlah </p>
                    <div class="col-sm-10">
                        <input type="text" name="jumlah" class="form-control" placeholder="Tgl Selesai"
                            value="<?php echo $edit['jumlah']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <p class="col-sm-2 text-left">Proyek </p>
                    <div class="col-sm-10">
                        <?php 
                            $proyek = array('All' => 'All', 'KLHK' => 'KLHK', 'SPARING' => 'SPARING', 'Lain-Lain' => 'Lain-Lain'); 
                            echo form_dropdown('proyek', $proyek, $edit['proyek'],"class='form-control'");    
                        ?>
                    </div>
                </div>
                <input type="hidden" class="form-control" id="user_update" name="user_update"  value="<?= $this->session->userdata('full_name'); ?>" required>             
                <input name="update_date" type="hidden" id="update_date" value=" <?php echo date('Y-m-d'); ?> "readonly>

                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="btn-group pull-left">
                            <?php echo anchor('keluar', 'Cancel', array('class' => 'btn btn-danger btn-sm')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-group pull-right">
                            <input type="submit" name="update" value="Update" class="btn btn-success btn-sm">
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/bootstrap-datepicker.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/js/tinymce/tinymce.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>template/backend/sbadmin/dist/js/sb-admin-2.js"></script>



<script type="text/javascript">
    tinymce.init({
        selector: 'textarea'
    });

    $(document).ready(function () {
        $("#tanggal1").datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#tanggal2").datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#tanggal").datepicker({
            format: 'yyyy-mm-dd'
        });
    })
</script>
<script type="text/javascript"> 
<?php echo $jsArray; ?>
function changeValue(id){
    document.getElementById('stock').value = prdName[id].stock;
    document.getElementById('harga').value = prdName[id].harga;
    document.getElementById('jumlah').value = prdName[id].jumlah;
};
</script>