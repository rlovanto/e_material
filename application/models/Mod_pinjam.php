<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_pinjam extends CI_Model {

    private $table   = "pinjam";
    private $primary = "kd_trans";

    public function buat_kode()

    {
        $this->db->select('RIGHT(pinjam.kd_trans,4) as kode', FALSE);
        $this->db->order_by('kd_trans','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('pinjam');      //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
         //jika kode ternyata sudah ada.      
         $data = $query->row();      
         $kode = intval($data->kode) + 1;    
        }
        else {      
         //jika kode belum ada      
         $kode = 1;    
        }

        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
        $kodejadi = "PNJ-9921-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
        return $kodejadi; 
    }

    function getAllByUsers($id)
    {
        $this->db->order_by('pinjam.nama_brg desc');
        $this->db->join('barang', 'pinjam.nama_brg = barang.nama_brg');
        $this->db->where("pinjam.id_petugas", $id);
        return $this->db->get('pinjam');
    }

    function searchPinjam($cari, $limit, $offset)
    {
        $this->db->like($this->primary,$cari);
        $this->db->or_like("kd_trans",$cari);
        $this->db->limit($limit, $offset);
        return $this->db->get($this->table);
    }

    function totalRows($table)
	{
		return $this->db->count_all_results($table);
    }

    function getDataPinjam($limit, $offset)
    {
        // return $this->db->get_where('post', array('category_id' => $category_id));
        $this->db->select('*');
        $this->db->from('pinjam a');
        // $this->db->where('a.npm', $npm);
        $this->db->limit($limit, $offset);
        $this->db->order_by('a.kd_trans desc');
        return $this->db->get();
    }
    
    function getAll()
    {
        $this->db->order_by('pinjam.kd_trans desc');
        return $this->db->get('pinjam');
    }

    function insertPinjam($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    function cekPinjam($kode)
    {
        $this->db->where("kd_trans", $kode);
        return $this->db->get("pinjam");
    }

    function updatePinjam($kd_trans, $data)
    {
        $this->db->where('kd_trans', $kd_trans);
		$this->db->update('pinjam', $data);
    }

    function getGambar($kd_trans)
    {
        $this->db->select('image');
        $this->db->from('pinjam');
        $this->db->where('kd_trans', $kd_trans);
        return $this->db->get();
    }

    function deletePinjam($kode, $table)
    {
        $this->db->where('kd_trans', $kode);
        $this->db->delete($table);
    }

    function PinjamSearch($kd_trans)
    {
        $this->db->like($this->primary,$kd_trans);
        $this->db->or_like("kd_trans",$kd_trans);
        $this->db->limit(10);
        return $this->db->get($this->table);
    }




}

/* End of file ModelName.php */
