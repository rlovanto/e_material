<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_pasca extends CI_Model {
    private $table   = "pasca";
    private $primary = "nomor";
    
     #import data
     function view(){
		return $this->db->get('pasca')->result(); // Tampilkan semua data yang ada di tabel siswa
	}
    // Fungsi untuk melakukan proses upload file
    function upload_file($filename){
        $this->load->library('upload'); // Load librari upload
        
        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size']	= '2048';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    // Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
    function insert_multiple($data){
        $this->db->insert_batch('pasca', $data);
    }

    function searchPasca($cari, $limit, $offset)
    {
        $this->db->like($this->primary,$cari);
        $this->db->or_like("provider",$cari);
        $this->db->or_like("nomor",$cari);
        $this->db->limit($limit, $offset);
        return $this->db->get($this->table);
    }

    function totalRows($table)
	{
		return $this->db->count_all_results($table);
    }

    
    function getAll()
    {
        $this->db->order_by('pasca.nomor desc');
        return $this->db->get('pasca');
    }

    function insertPasca($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    function cekPasca($kode)
    {
        $this->db->where("nomor", $kode);
        return $this->db->get("pasca");
    }

    function updatePasca($nomor, $data)
    {
        $this->db->where('nomor', $nomor);
		$this->db->update('pasca', $data);
    }

    function deletePasca($kode, $table)
    {
        $this->db->where('nomor', $kode);
        $this->db->delete($table);
    }

    function PascaSearch($nomor)
    {
        $this->db->like($this->primary,$nomor);
        $this->db->or_like("nomor",$nomor);
        $this->db->limit(10);
        return $this->db->get($this->table);
    }

}

