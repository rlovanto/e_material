<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_masuks extends CI_Model 
{

    private $table = "masuks";
    private $tmp_msk   = "tmp_msk";
    
    function AutoNumbering()
    {
        $today = date('Ymd');

        $data = $this->db->query("SELECT MAX(id_transaksi) AS last FROM $this->table ")->row_array();

        $lastNoFaktur = $data['last'];
        
        $lastNoUrut   = substr($lastNoFaktur,8,3);
        
        $nextNoUrut   =  ((int) $lastNoUrut) + 1; 
        
        $nextNoTransaksi = $today.sprintf('%03s',$nextNoUrut);
        
        return $nextNoTransaksi;
    }

    function getAll()
    {
        $this->db->order_by('barang_masuk.id_transaksi desc');
        return $this->db->get('barang_masuk');
    }

    function getTmp()
    {
        return $this->db->get("tmp_msk");
    }
    
    function cekTmp($kode)
    {
        $this->db->where("id_brg",$kode);
        return $this->db->get("tmp_msk");
    }

    function InsertTmp($data)
    {
        //$this->db->insert("masuks",$info);
        $this->db->insert($this->tmp_msk, $data);    
    }

    function InsertTransaksi($data)
    {
        $this->db->insert($this->table, $data);
    }

    function jumlahTmp()
    {
        return $this->db->count_all("tmp_msk");
    }

    function deleteTmp($id_brg)
    {
        $this->db->where("id_brg",$id_brg);
        $this->db->delete($this->tmp_msk);
    }

    function deletePinjam($kode, $table)
    {
        $this->db->where('id_transaksi', $kode);
        $this->db->delete($table);
    }
    function getTransaksi()
    {
        return $this->db->get($this->table);
    }
    
    function cekTransaksi($kode)
    {
        $this->db->where("id_transaksi", $kode);
        return $this->db->get("masuks");
    }
    function updateTransaksi($id_transaksi, $data)
    {
        $this->db->where('id_transaksi', $id_transaksi);
		$this->db->update('masuks', $data);
    }
    function graph(){
        $query = $this->db->query("SELECT * from barang_masuk");
         
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
    function totalRows($table)
	{
        // $this->db->where("status", 'N');
		return $this->db->count_all_results($table);
    }

    function getAlls()
    {
        $this->db->order_by('masuks.id_transaksi desc');
        $this->db->join('pengembalian', 'masuks.id_transaksi = pengembalian.id_transaksi');
        return $this->db->get('masuks');
    }
    function getAllByUsers($id)
    {
        $this->db->order_by('masuks.id_brg desc');
        $this->db->join('barang', 'masuks.id_brg = barang.id_brg');
        $this->db->where("masuks.id_petugas", $id);
        return $this->db->get('masuks');
    }
}

/* End of file Mod_peminjaman.php */
