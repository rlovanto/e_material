<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_peminjaman extends CI_Model 
{

    private $table = "transaksi";
    private $tmp   = "tmp";
    
    function AutoNumbering()
    {
        $today = date('Ymd');

        $data = $this->db->query("SELECT MAX(id_transaksi) AS last FROM $this->table ")->row_array();

        $lastNoFaktur = $data['last'];
        
        $lastNoUrut   = substr($lastNoFaktur,8,3);
        
        $nextNoUrut   =  ((int) $lastNoUrut) + 1;
        
        $nextNoTransaksi = $today.sprintf('%03s',$nextNoUrut);
        
        return $nextNoTransaksi;
    }
    function graph(){
        $query = $this->db->query("SELECT * from peminjaman");
         
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
    function getAll()
    {
        $this->db->order_by('peminjaman.id_transaksi desc');
        return $this->db->get('peminjaman');
    }

    function getTmp()
    {
        return $this->db->get("tmp");
    }
    
    function cekTmp($kode)
    {
        $this->db->where("id_brg",$kode);
        return $this->db->get("tmp");
    }

    function InsertTmp($data)
    {
        //$this->db->insert("transaksi",$info);
        $this->db->insert($this->tmp, $data);    
    }

    function InsertTransaksi($data)
    {
        $this->db->insert($this->table, $data);
    }

    function jumlahTmp()
    {
        return $this->db->count_all("tmp");
    }

    function deleteTmp($id_brg)
    {
        $this->db->where("id_brg",$id_brg);
        $this->db->delete($this->tmp);
    }

    function deletePinjam($kode, $table)
    {
        $this->db->where('id_transaksi', $kode);
        $this->db->delete($table);
    }
    function getTransaksi()
    {
        return $this->db->get($this->table);
    }

    function totalRows($table)
	{
        $this->db->where("status", 'Dipinjam');
		return $this->db->count_all_results($table);
    }
    function totalRowsi($table)
	{
        // $this->db->where("status", 'N');
		return $this->db->count_all_results($table);
    }

    function getAlls()
    {
        $this->db->order_by('transaksi.id_transaksi desc');
        $this->db->join('pengembalian', 'transaksi.id_transaksi = pengembalian.id_transaksi');
        return $this->db->get('transaksi');
    }

    function getAllByUsers($id)
    {
        $this->db->order_by('transaksi.id_brg desc');
        $this->db->join('barang', 'transaksi.id_brg = barang.id_brg');
        $this->db->where("transaksi.id_petugas", $id);
        return $this->db->get('transaksi');
    }
}

/* End of file Mod_peminjaman.php */
