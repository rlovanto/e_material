<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_kategori extends CI_Model {
    private $table   = "kategori";
    private $primary = "kd_kategori";
    


    #import data
    function view(){
		return $this->db->get('kategori')->result(); // Tampilkan semua data yang ada di tabel siswa
	}
    // Fungsi untuk melakukan proses upload file
    function upload_file($filename){
        $this->load->library('upload'); // Load librari upload
        
        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size']	= '2048';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }
// Kode otomatis per date.
    function AutoNumbering()
    {
        $today = date('Ymd');
        $data = $this->db->query("SELECT MAX(kd_kategori) AS last FROM $this->table ")->row_array();
        $lastNoFaktur = $data['last'];
        $lastNoUrut   = substr($lastNoFaktur,8,3);
        $nextNoUrut   =  ((int) $lastNoUrut) + 1;
        $nextNoTransaksi = $today.sprintf('%03s',$nextNoUrut);
        return $nextNoTransaksi;
    }

    // Kode otomatis.
    public function buat_kode()
    {
        $this->db->select('RIGHT(kategori.kd_kategori,4) as kode', FALSE);
        $this->db->order_by('kd_kategori','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('kategori');  

        //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){          
            $data = $query->row();      
            $kode = intval($data->kode) + 1;    
        }
        // belum ada data
        else {        
            $kode = 1;    
        }

        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
        $kodejadi = "Kategori-9921-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
        return $kodejadi; 
    }
    // Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
    function insert_multiple($data){
        $this->db->insert_batch('kategori', $data);
    }

    function searchKategori($cari, $limit, $offset)
    {
        $this->db->like($this->primary,$cari);
        $this->db->or_like("kategori",$cari);
        $this->db->or_like("kd_kategori",$cari);
        $this->db->limit($limit, $offset);
        return $this->db->get($this->table);
    }

    function totalRows($table)
	{
		return $this->db->count_all_results($table);
    }

    
    function getAll()
    {
        $this->db->order_by('kategori.kd_kategori desc');
        return $this->db->get('kategori');
    }

    function insertKategori($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    function cekKategori($kode)
    {
        $this->db->where("kd_kategori", $kode);
        return $this->db->get("kategori");
    }

    function updateKategori($kd_kategori, $data)
    {
        $this->db->where('kd_kategori', $kd_kategori);
		$this->db->update('kategori', $data);
    }

    function deleteKategori($kode, $table)
    {
        $this->db->where('kd_kategori', $kode);
        $this->db->delete($table);
    }

    function KategoriSearch($kd_kategori)
    {
        $this->db->like($this->primary,$kd_kategori);
        $this->db->or_like("kd_kategori",$kd_kategori);
        $this->db->limit(10);
        return $this->db->get($this->table);
    }

}

