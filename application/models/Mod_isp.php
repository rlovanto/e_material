<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_isp extends CI_Model {
    private $table   = "isp";
    private $primary = "id_isp";
    
    #import data
    function view(){
        return $this->db->get('isp')->result(); // Tampilkan semua data yang ada di tabel siswa
    }
    // Fungsi untuk melakukan proses upload file
    function upload_file($filename){
        $this->load->library('upload'); // Load librari upload
        
        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size']	= '2048';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    // Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
    function insert_multiple($data){
        $this->db->insert_batch('isp', $data);
    }


    function searchIsp($cari, $limit, $offset)
    {
        $this->db->like($this->primary,$cari);
        $this->db->or_like("nomor",$cari);
        $this->db->or_like("id_isp",$cari);
        $this->db->limit($limit, $offset);
        return $this->db->get($this->table);
    }

    function totalRows($table)
	{
		return $this->db->count_all_results($table);
    }
 
    function getAll()
    {
        $this->db->order_by('isp.id_isp desc');
        return $this->db->get('isp');
    }

    function insertIsp($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    function cekIsp($kode)
    {
        $this->db->where("id_isp", $kode);
        return $this->db->get("isp");
    }

    function updateIsp($id_isp, $data)
    {
        $this->db->where('id_isp', $id_isp);
		$this->db->update('isp', $data);
    }

    function deleteIsp($kode, $table)
    {
        $this->db->where('id_isp', $kode);
        $this->db->delete($table);
    }

    function IspSearch($id_isp)
    {
        $this->db->like($this->primary,$id_isp);
        $this->db->or_like("id_isp",$id_isp);
        $this->db->limit(10);
        return $this->db->get($this->table);
    }

}

