<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_barangx extends CI_Model {

    private $table   = "barangx";
    private $primary = "nama_brg";
	#import data
    function view(){
		return $this->db->get('barangx')->result(); // Tampilkan semua data yang ada di tabel siswa
	}
    // Fungsi untuk melakukan proses upload file
    function upload_file($filename){
        $this->load->library('upload'); // Load librari upload
        
        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size']	= '2048';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    // Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
    function insert_multiple($data){
        $this->db->insert_batch('barangx', $data);
    }

    function searchBarang($cari, $limit, $offset)
    {
        $this->db->like($this->primary,$cari);
        $this->db->or_like("nama_brg",$cari);
        $this->db->limit($limit, $offset);
        return $this->db->get($this->table);
    }
    

    function totalRows($table)
	{
		return $this->db->count_all_results($table);
    }

    function getDataBarang($limit, $offset)
    {
        // return $this->db->get_where('post', array('category_id' => $category_id));
        $this->db->select('*');
        $this->db->from('barangx a');
        // $this->db->where('a.npm', $npm);
        $this->db->limit($limit, $offset);
        $this->db->order_by('a.nama_brg desc');
        return $this->db->get();
    }
    
    function getAll()
    {
        $this->db->order_by('barangx.nama_brg desc');
        return $this->db->get('barangx');
    }

    function insertBarang($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    function cekBarang($kode)
    {
        $this->db->where("nama_brg", $kode);
        return $this->db->get("barangx");
    }

    function updateBarang($nama_brg, $data)
    {
        $this->db->where('nama_brg', $nama_brg);
		$this->db->update('barangx', $data);
    }

    function getGambar($nama_brg)
    {
        $this->db->select('image');
        $this->db->from('barangx');
        $this->db->where('nama_brg', $nama_brg);
        return $this->db->get();
    }

    function deleteBarang($kode, $table)
    {
        $this->db->where('nama_brg', $kode);
        $this->db->delete($table);
    }

    // function BarangSearch($nama_brg)
    // {
    //     $this->db->like($this->primary,$nama_brg);
    //     $this->db->or_like("nama_brg",$nama_brg);
    //     $this->db->limit(10);
    //     return $this->db->get($this->table);
    // }
    function BarangSearch($nama_brg)
    {
        $this->db->like($this->primary,$nama_brg);
        $this->db->or_like("nama_brg",$nama_brg);
        $this->db->limit(10);
        return $this->db->get($this->table);
    }




}

/* End of file ModelName.php */
