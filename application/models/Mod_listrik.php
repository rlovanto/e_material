<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_listrik extends CI_Model {
    private $table   = "listrik";
    private $primary = "id_pelanggan";
    
    #import data
    function view(){
		return $this->db->get('listrik')->result(); // Tampilkan semua data yang ada di tabel siswa
	}
    // Fungsi untuk melakukan proses upload file
    function upload_file($filename){
        $this->load->library('upload'); // Load librari upload
        
        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size']	= '2048';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    // Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
    function insert_multiple($data){
        $this->db->insert_batch('listrik', $data);
    }

    function searchListrik($cari, $limit, $offset)
    {
        $this->db->like($this->primary,$cari);
        $this->db->or_like("jenis",$cari);
        $this->db->or_like("id_pelanggan",$cari);
        $this->db->limit($limit, $offset);
        return $this->db->get($this->table);
    }

    function totalRows($table)
	{
		return $this->db->count_all_results($table);
    }

    
    function getAll()
    {
        $this->db->order_by('listrik.id_pelanggan desc');
        return $this->db->get('listrik');
    }

    function insertListrik($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    function cekListrik($kode)
    {
        $this->db->where("id_pelanggan", $kode);
        return $this->db->get("listrik");
    }

    function updateListrik($id_pelanggan, $data)
    {
        $this->db->where('id_pelanggan', $id_pelanggan);
		$this->db->update('listrik', $data);
    }

    function deleteListrik($kode, $table)
    {
        $this->db->where('id_pelanggan', $kode);
        $this->db->delete($table);
    }

    function ListrikSearch($id_pelanggan)
    {
        $this->db->like($this->primary,$id_pelanggan);
        $this->db->or_like("id_pelanggan",$id_pelanggan);
        $this->db->limit(10);
        return $this->db->get($this->table);
    }

}

