<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_pengembalian extends CI_Model {

    private $table_transaksi    = 'transaksi';
    private $table_pengembalian = 'pengembalian';
    // private $table_anggota      = 'anggota';  
    private $table_barang         = 'barang';    

    public function SearchBrg($id_brg)
    {
        $data = $this->db->query("SELECT * FROM $this->table_transaksi WHERE id_transaksi 
                                  NOT IN(SELECT id_transaksi FROM $this->table_pengembalian)
                                  AND id_brg LIKE '%$id_brg%' GROUP BY id_brg");
        return $data;
    }

    public function SearchTransaksi($no_transaksi)
    {
        // $query = $this->db->query("SELECT a.*, b.nama FROM $this->table_transaksi a, $this->table_anggota 
                                    // b WHERE a.id_transaksi = '$no_transaksi' AND a.id_transaksi 
        //                            NOT IN(SELECT c.id_transaksi FROM $this->table_pengembalian c)
        //                            AND a.npm = b.npm");
        $query = $this->db->query("SELECT a.*, b.nama_brg FROM $this->table_transaksi a, $this->table_barang  b 
                                    WHERE a.id_transaksi = '$no_transaksi' AND a.id_transaksi 
                                    NOT IN(SELECT c.id_transaksi FROM $this->table_pengembalian c)
                                    AND a.id_brg = b.id_brg");
        return $query;
    }

    public function showBrg($no_transaksi)
    {
        // $query = $this->db->query("SELECT a.*, b.judul,b.pengarang 
        //                            FROM $this->table_transaksi a, $this->table_buku b 
        //                            WHERE a.id_transaksi = '$no_transaksi' AND a.id_transaksi 
        //                            NOT IN(SELECT c.id_transaksi FROM $this->table_pengembalian c)
        //                            AND a.kode_buku = b.kode_buku");
        $query = $this->db->query("SELECT a.*, b.nama_brg,b.satuan 
                                    FROM $this->table_transaksi a, $this->table_barang b 
                                    WHERE a.id_transaksi = '$no_transaksi' AND a.id_transaksi 
                                    NOT IN(SELECT c.id_transaksi FROM $this->table_pengembalian c)
                                    AND a.id_brg = b.id_brg");
        return $query;
    }

    public function insertPengembalian($data)
    {
        $this->db->insert($this->table_pengembalian, $data);
    }

    public function UpdateStatus($no_transaksi, $data)
    {
        $this->db->where("id_transaksi", $no_transaksi);
        $this->db->update($this->table_transaksi, $data);
        
    }

    function totalRows($table)
	{
		return $this->db->count_all_results($table);
    }
}

/* End of file Mod_pengembalian.php */
