<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_keluars extends CI_Model 
{

    private $table = "keluars";
    private $tmp_klr   = "tmp_klr";
    
    function AutoNumber()
    {
        $today = date('Ymd');

        $data = $this->db->query("SELECT MAX(id_transaksi) AS last FROM $this->table ")->row_array();

        $lastNoFaktur = $data['last'];
        
        $lastNoUrut   = substr($lastNoFaktur,8,3);
        
        $nextNoUrut   =    ((int) $lastNoUrut) + 1; 
     
        $nextNoTransaksi = $today.sprintf('%03s',$nextNoUrut);
        
        return $nextNoTransaksi;
    }

    function getAll()
    {
        $this->db->order_by('keluar_brg.id_transaksi desc');
        return $this->db->get('keluar_brg');
    }

    function getTmp()
    {
        return $this->db->get("tmp_klr");
    }
    
    function cekTmp($kode)
    {
        $this->db->where("id_brg",$kode);
        return $this->db->get("tmp_klr");
    }

    function InsertTmp($data)
    {
        //$this->db->insert("keluars",$info);
        $this->db->insert($this->tmp_klr, $data);    
    }

    function InsertTransaksi($data)
    {
        $this->db->insert($this->table, $data);
    }

    function jumlahTmp()
    {
        return $this->db->count_all("tmp_klr");
    }

    function deleteTmp($id_brg)
    {
        $this->db->where("id_brg",$id_brg);
        $this->db->delete($this->tmp_klr);
    }

    function deleteKeluar($kode, $table)
    {
        $this->db->where('id_transaksi', $kode);
        $this->db->delete($table);
    }
    function getTransaksi()
    {
        return $this->db->get($this->table);
    }

    function totalRows($table)
	{
        // $this->db->where("status", 'N');
		return $this->db->count_all_results($table);
    }

    function getAlls()
    {
        $this->db->order_by('keluars.id_transaksi desc');
        $this->db->join('pengembalian', 'keluars.id_transaksi = pengembalian.id_transaksi');
        return $this->db->get('keluars');
    }
    function getAllByUsers($id)
    {
        $this->db->order_by('keluars.id_brg desc');
        $this->db->join('barang', 'keluars.id_brg = barang.id_brg');
        $this->db->where("keluars.id_petugas", $id);
        return $this->db->get('keluars');
    }
    function graph(){
        $query = $this->db->query("SELECT * from keluar_brg");
         
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }


}

/* End of file Mod_peminjaman.php */
