<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_statistik extends CI_Model {
    // Mengambil data pengunjung.
    function graphic_peminjaman(){
        $query = $this->db->query("SELECT tanggal_pinjam, COUNT(*) as total FROM peminjaman WHERE tanggal_pinjam BETWEEN '".date('Y-m-d', strtotime('-1 month'))."' AND '".date('Y-m-d')."' GROUP BY tanggal_pinjam");
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    // Mengambil data buku.
    function graphic_pengemblian(){
        $query = $this->db->query("SELECT tgl_pengembalian, COUNT(*) as total FROM pengembalian WHERE tgl_pengembalian BETWEEN '".date('Y-m-d', strtotime('-1 month'))."' AND '".date('Y-m-d')."' GROUP BY tgl_pengembalian");
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    // Mengambil data barang_masuk.
    function graphic_barangmasuk(){
        $query = $this->db->query("SELECT tanggal_masuk, COUNT(*) as total FROM barang_masuk WHERE tanggal_masuk BETWEEN '".date('Y-m-d', strtotime('-1 month'))."' AND '".date('Y-m-d')."' GROUP BY tanggal_masuk");
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
    
    // Mengambil data ebook.
    function graphic_barangkeluar(){
        $query = $this->db->query("SELECT tanggal_keluar, COUNT(*) as total FROM keluar_brg WHERE tanggal_keluar BETWEEN '".date('Y-m-d', strtotime('-1 month'))."' AND '".date('Y-m-d')."' GROUP BY tanggal_keluar");
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

}