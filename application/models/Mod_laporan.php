<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_laporan extends CI_Model {

    function getAllBarang($keyword)
    {
        if ($keyword != NULL) {
			$this->db->like('nama_brg', $keyword)->or_like('jenis', $keyword)->or_like('satuan', $keyword)->or_like('nomer_box', $keyword)->or_like('kategori', $keyword);
            $this->db->order_by('barang.nama_brg desc');
			return $this->db->get('barang');
		} else {
            $this->db->order_by('barang.nama_brg desc');
			return $this->db->get('barang');
		}
    }
    function getAllCard($keyword)
    {
        if ($keyword != NULL) {
			$this->db->like('type', $keyword)->or_like('kartu', $keyword)->or_like('nomor', $keyword)->or_like('tempat', $keyword);
            $this->db->order_by('card.type desc');
			return $this->db->get('card');
		} else {
            $this->db->order_by('card.type desc');
			return $this->db->get('card');
		}
    }
    function getAllPasca($keyword)
    {
        if ($keyword != NULL) {
			$this->db->like('type', $keyword)->or_like('provider', $keyword)->or_like('nomor', $keyword)->or_like('tempat', $keyword);
            $this->db->order_by('pasca.type desc');
			return $this->db->get('pasca');
		} else {
            $this->db->order_by('pasca.type desc');
			return $this->db->get('pasca');
		}
    }

    function getAllMasuk($tanggalMulai, $tanggalSelesai)
    {
        if ($tanggalMulai != NULL && $tanggalSelesai) {
			$query = "SELECT * from masuk where tgl_masuk between '$tanggalMulai' and '$tanggalSelesai'";
		    return $this->db->query($query);
		} else {
            $this->db->order_by('masuk.tgl_masuk desc');
			return $this->db->get('masuk');
		}
    }

    function getAllKeluar($tanggalMulai, $tanggalSelesai)
    {
        if ($tanggalMulai != NULL && $tanggalSelesai) {
			$query = "SELECT * from keluar where tgl_keluar between '$tanggalMulai' and '$tanggalSelesai'";
		    return $this->db->query($query);
		} else {
            $this->db->order_by('keluar.tgl_keluar desc');
			return $this->db->get('keluar');
		}
    }
    function getAllPinjam($tanggalMulai, $tanggalSelesai)
    {
        if ($tanggalMulai != NULL && $tanggalSelesai) {
			$query = "SELECT * from pinjam where tgl_pinjam between '$tanggalMulai' and '$tanggalSelesai'";
		    return $this->db->query($query);
		} else {
            $this->db->order_by('pinjam.tgl_pinjam desc');
			return $this->db->get('pinjam');
		}
    }
    function getAllKembali($tanggalMulai, $tanggalSelesai)
    {
        if ($tanggalMulai != NULL && $tanggalSelesai) {
			$query = "SELECT * from kembali where tgl_kembali between '$tanggalMulai' and '$tanggalSelesai'";
		    return $this->db->query($query);
		} else {
            $this->db->order_by('kembali.tgl_kembali desc');
			return $this->db->get('kembali');
		}
    }

    function getAllUsers($keyword)
    {
        if ($keyword != NULL) {
			$this->db->like('username', $keyword)->or_like('full_name', $keyword);
            $this->db->order_by('petugas.id_petugas desc');
            return $this->db->get('petugas');
		} else {
            $this->db->order_by('petugas.id_petugas desc');
            return $this->db->get('petugas');
		}
    }

    function getAllBuku($keyword)
    {
        if ($keyword != NULL) {
			$this->db->like('kode_buku', $keyword)->or_like('judul', $keyword);
            $this->db->order_by('buku.kode_buku desc');
			return $this->db->get('buku');
		} else {
            $this->db->order_by('buku.kode_buku desc');
			return $this->db->get('buku');
		}
    }

    function getAllKegiatan($tanggalMulai, $tanggalSelesai)
    {
        if ($tanggalMulai != NULL && $tanggalSelesai) {
			$query = "SELECT * from kegiatan where tgl_mulai between '$tanggalMulai' and '$tanggalSelesai'";
		    return $this->db->query($query);
		} else {
            $this->db->order_by('kegiatan.nm_kegiatan desc');
			return $this->db->get('kegiatan');
		}
    }

    function getAllPengumuman($tanggalMulai, $tanggalSelesai)
    {
        if ($tanggalMulai != NULL && $tanggalSelesai) {
			$query = "SELECT * from f_announcement where tgl between '$tanggalMulai' and '$tanggalSelesai'";
		    return $this->db->query($query);
		} else {
            $this->db->order_by('f_announcement.id_announcement desc');
			return $this->db->get('f_announcement');
		}
    }

    function getAllMajalah($tanggalMulai, $tanggalSelesai)
    {
        if ($tanggalMulai != NULL && $tanggalSelesai) {
			$query = "SELECT * from majalah where tgl_terbit between '$tanggalMulai' and '$tanggalSelesai'";
		    return $this->db->query($query);
		} else {
            $this->db->order_by('majalah.kd_mjl desc');
			return $this->db->get('majalah');
		}
    }

    function getAllKliping($tanggalMulai, $tanggalSelesai)
    {
        if ($tanggalMulai != NULL && $tanggalSelesai) {
			$query = "SELECT * from kliping where tgl between '$tanggalMulai' and '$tanggalSelesai'";
		    return $this->db->query($query);
		} else {
            $this->db->order_by('kliping.kd_klp desc');
			return $this->db->get('kliping');
		}
    }

    function getAllEbook($keyword)
    {
        if ($keyword != NULL) {
			$this->db->like('kode_eb', $keyword)->or_like('keterangan', $keyword);
            $this->db->order_by('ebook.kode_eb desc');
			return $this->db->get('ebook');
		} else {
            $this->db->order_by('ebook.kode_eb desc');
			return $this->db->get('ebook');
		}
    }

    function getAllFlipbook($tanggalMulai, $tanggalSelesai)
    {
        if ($tanggalMulai != NULL && $tanggalSelesai) {
			$query = "SELECT * from flipbook where tgl_sunting between '$tanggalMulai' and '$tanggalSelesai'";
		    return $this->db->query($query);
		} else {
            $this->db->order_by('flipbook.kode_fb desc');
			return $this->db->get('flipbook');
		}
    }

    function getAllMaps($keyword)
    {
        if ($keyword != NULL) {
			$this->db->like('nm_perpus', $keyword);
            $this->db->order_by('maps.id desc');
			return $this->db->get('maps');
		} else {
            $this->db->order_by('maps.id desc');
			return $this->db->get('maps');
		}
    }

    function getAllFasilitas($keyword)
    {
        if ($keyword != NULL) {
			$this->db->like('nm_fasilitas', $keyword)->or_like('thn_beli', $keyword);
            $this->db->order_by('fasilitas.id_fsl desc');
            return $this->db->get('fasilitas');
		} else {
            $this->db->order_by('fasilitas.id_fsl desc');
            return $this->db->get('fasilitas');
		}
    }

    function getAllLayanan($keyword)
    {
        if ($keyword != NULL) {
			$this->db->like('nama', $keyword)->or_like('deskripsi', $keyword);
            $this->db->order_by('layanan.kd_lyn desc');
            return $this->db->get('layanan');
		} else {
            $this->db->order_by('layanan.kd_lyn desc');
            return $this->db->get('layanan');
		}
    }

    function getAllNews($tanggalMulai, $tanggalSelesai)
    {
        if ($tanggalMulai != NULL && $tanggalSelesai) {
			$query = "SELECT * from news where tgl_rilis between '$tanggalMulai' and '$tanggalSelesai'";
		    return $this->db->query($query);
		} else {
            $this->db->order_by('news.kd_news desc');
			return $this->db->get('news');
		}
    }

    function getAllArtikel($tanggalMulai, $tanggalSelesai)
    {
        if ($tanggalMulai != NULL && $tanggalSelesai) {
			$query = "SELECT * from artikel where tgl_tulis between '$tanggalMulai' and '$tanggalSelesai'";
		    return $this->db->query($query);
		} else {
            $this->db->order_by('artikel.kd_art desc');
			return $this->db->get('artikel');
		}
    }

    public function searchPinjaman($tanggal1, $tanggal2)
    {
        // $this->db->select('*');
        // $this->db->from('transaksi');
        // $this->db->where('tanggal_pinjam <','$tanggal1');
        // $this->db->where('tanggal_kembali >','$tanggal2');

        // return $this->db->get();
        return $this->db->query("SELECT a.*,  
                                 CASE 
                                    WHEN a.status = 'N' THEN 'Masih Dipinjam'
                                 ELSE 'Sudah Dikembalikan' 
                                 END AS status_pinjam
                                 FROM transaksi a WHERE a.tanggal_pinjam  BETWEEN '$tanggal1' AND '$tanggal2' GROUP BY a.id_transaksi");
    }   
    
    public function detailPinjaman($id_transaksi)
    {
        // $this->db->select("*");
        // $this->db->from("transaksi a");
        // $this->db->where("a.id_transaksi", $id_transaksi);
        // $this->db->join("buku b", "a.kode_buku = b.kode_buku");
        // return $this->db->get();
        return $this->db->query("SELECT a.*,b.kode_buku,b.judul, b.pengarang, 
                                 CASE 
                                    WHEN a.status = 'N' THEN 'Masih di pinjam'
                                 ELSE 'Sudah Dikembalikan' 
                                 END AS status_pinjam
                                 FROM transaksi a, buku b 
                                 WHERE a.id_transaksi = '$id_transaksi' 
                                 AND a.kode_buku = b.kode_buku");
    }

    public function searchPengembalian($tanggal1, $tanggal2)
    {
        return $this->db->query("SELECT transaksi.id_transaksi, anggota.nama, pengembalian.*, buku.judul , petugas.full_name
        FROM pengembalian
        INNER JOIN transaksi
        ON transaksi.id_transaksi = pengembalian.id_transaksi
        INNER JOIN buku
        ON buku.kode_buku = transaksi.kode_buku
        INNER JOIN anggota
        ON anggota.npm = transaksi.nis
        INNER JOIN petugas
        ON petugas.id_petugas = pengembalian.id_petugas
        WHERE pengembalian.tgl_pengembalian BETWEEN '$tanggal1' AND '$tanggal2'
        GROUP BY pengembalian.id_transaksi");
    }

    public function detailPengembalian($id_transaksi)
    {
        return $this->db->query("SELECT a.*, b.status, c.kode_buku, c.judul, c.pengarang, 
                                CASE 
                                    WHEN b.status = 'N' THEN 'Masih di pinjam'
                                ELSE 'Sudah Dikembalikan' 
                                END AS status_pinjam
                                FROM pengembalian a, transaksi b, buku c
                                WHERE a.id_transaksi = '$id_transaksi'
                                AND a.id_transaksi = b.id_transaksi
                                AND b.kode_buku = c.kode_buku");
    }

    
}

/* End of file Mod_laporan.php */
