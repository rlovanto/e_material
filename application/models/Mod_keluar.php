<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_keluar extends CI_Model {

    private $table   = "keluar";
    private $primary = "kd_trans";

    function graph(){
        $query = $this->db->query("SELECT * from keluar_brg");
         
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function buat_kode()

    {
        $this->db->select('RIGHT(keluar.kd_trans,4) as kode', FALSE);
        $this->db->order_by('kd_trans','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('keluar');      //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
         //jika kode ternyata sudah ada.      
         $data = $query->row();      
         $kode = intval($data->kode) + 1;    
        }
        else {      
         //jika kode belum ada      
         $kode = 1;    
        }

        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
        $kodejadi = "OUT-9921-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
        return $kodejadi; 
    }

    function getAllByUsers($id)
    {
        $this->db->order_by('keluar.nama_brg desc');
        $this->db->join('barang', 'keluar.nama_brg = barang.nama_brg');
        $this->db->where("keluar.id_petugas", $id);
        return $this->db->get('keluar');
    }

    function searchKeluar($cari, $limit, $offset)
    {
        $this->db->like($this->primary,$cari);
        $this->db->or_like("kd_trans",$cari);
        $this->db->limit($limit, $offset);
        return $this->db->get($this->table);
    }

    function totalRows($table)
	{
		return $this->db->count_all_results($table);
    }

    function getDataKeluar($limit, $offset)
    {
        // return $this->db->get_where('post', array('category_id' => $category_id));
        $this->db->select('*');
        $this->db->from('keluar a');
        // $this->db->where('a.npm', $npm);
        $this->db->limit($limit, $offset);
        $this->db->order_by('a.kd_trans desc');
        return $this->db->get();
    }
    
    function getAll()
    {
        $this->db->order_by('keluar.kd_trans desc');
        return $this->db->get('keluar');
    }

    function insertKeluar($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    function cekKeluar($kode)
    {
        $this->db->where("kd_trans", $kode);
        return $this->db->get("keluar");
    }

    function updateKeluar($kd_trans, $data)
    {
        $this->db->where('kd_trans', $kd_trans);
		$this->db->update('keluar', $data);
    }

    function getGambar($kd_trans)
    {
        $this->db->select('image');
        $this->db->from('keluar');
        $this->db->where('kd_trans', $kd_trans);
        return $this->db->get();
    }

    function deleteKeluar($kode, $table)
    {
        $this->db->where('kd_trans', $kode);
        $this->db->delete($table);
    }

    function KeluarSearch($kd_trans)
    {
        $this->db->like($this->primary,$kd_trans);
        $this->db->or_like("kd_trans",$kd_trans);
        $this->db->limit(10);
        return $this->db->get($this->table);
    }




}

/* End of file ModelName.php */
