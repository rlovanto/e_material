<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_custom extends CI_Model {

    function getCustomProfile()
    {
        return $this->db->get('f_profile');
    }

    function getOneCustomProfile($id)
    {
        $this->db->where("id", $id);
        return $this->db->get("f_profile");
    }

    function getAllProfile()
    {
        $this->db->order_by('f_profile.name desc');
        return $this->db->get('f_profile');
    }

    function getProfileGambar($id)
    {
        $this->db->select('image');
        $this->db->from('f_profile');
        $this->db->where('id', $id);
        return $this->db->get();
    }

    function updateProfile($id, $data)
    {
        $this->db->where('id', $id);
		$this->db->update('f_profile', $data);
    }

    function getCustomMisi()
    {
        return $this->db->get('f_misi');
    }

    function getAllMisi()
    {
        $this->db->order_by('f_misi.id asc');
        return $this->db->get('f_misi');
    }
}