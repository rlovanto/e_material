<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_anggota extends CI_Model {

    function getAnggota()
    {
        return $this->db->get('anggota');
    }

    function getAll()
    {
        $this->db->order_by('anggota.npm desc');
        return $this->db->get('anggota');
    }

    function insertAnggota($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    function cekAnggota($kode)
    {
        $this->db->where("npm", $kode);
        return $this->db->get("anggota");
    }

    function updateAnggota($npm, $data)
    {
        $this->db->where('npm', $npm);
		$this->db->update('anggota', $data);
    }

    function getDataAnggota($limit, $offset)
    {
        // return $this->db->get_where('post', array('category_id' => $category_id));
        $this->db->select('*');
        $this->db->from('anggota a');
        // $this->db->where('a.npm', $npm);
        $this->db->limit($limit, $offset);
        $this->db->order_by('a.npm desc');
        return $this->db->get();
    }

    function getGambar($npm)
    {
        $this->db->select('image');
        $this->db->from('anggota');
        $this->db->where('npm', $npm);
        return $this->db->get();
    }

    function totalRows($table)
	{
		return $this->db->count_all_results($table);
    }

    function getTotalRows()
    {
        return $this->db->get('anggota')->num_rows();
    }

    
    function searchAnggota($cari, $limit, $offset)
    {
        $this->db->like("npm",$cari);
        $this->db->or_like("nama",$cari);
        $this->db->limit($limit, $offset);
        return $this->db->get('anggota');
    }

    function deleteAnggota($kode, $table)
    {
        $this->db->where('npm', $kode);
        $this->db->delete($table);
    }

}

/* End of file ModelName.php */
